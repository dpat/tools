#!/bin/bash

: <<COMMENTBLOCK
This code calls the docker fMRIPrep tool to prepprocess fMRI and structural data in the BIDS data structure. This script requires that you are in the directory where your subject subdirectories are stored in the BIDS data structure.
COMMENTBLOCK


# Exit and spit out help message if number of arguments is too small
if [ $# -lt 1 ]
  then
    echo "======================================================"
    echo "This assumes you are running docker"
    echo "and have downloaded fmriprep: docker pull nipreps/fmriprep"
    echo "e.g. Run subject sub-304: $0 304"
    echo "e.g. Run multiple subjects: $0 304 305 306"
    echo "On the mac, ensure Docker has more than the default 2 GB of RAM:"
    echo "Docker->Preferences->Advanced"
    echo ""
    echo "run this from the project directory above data so that derivatives, data, and fmriprep_work are all siblings"
    echo "======================================================"
    exit 1
fi


# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${PWD}/derivatives ]; then
mkdir ${PWD}/derivatives
fi

# Just to be safe, create the work directory if it does not exist.
# Apparently Docker does not care if this is under the BIDS input dir.
if [ ! -d ${PWD}/fmriprep_work ]; then
mkdir ${PWD}/fmriprep_work
fi

# Including "" around PWD ensures it will handle spaces in the path names.
# This limited version *should* run quickly but no freesurfer or surface formats.
# docker run --rm -it  -v /Users/dpat/license.txt:/opt/freesurfer/license.txt:ro -v "${PWD}"/Nifti:/data:ro -v "${PWD}"/derivatives:/out nipreps/fmriprep:latest /data /out participant --participant_label ${*} -w "${PWD}"/fmriprep_work --stop-on-first-crash --use-aroma  --ignore slicetiming  --fs-no-reconall

# output spaces: cifti data in native space can overlay on the T1w GIFTIs that are produced
# fsaverage can overlay on standard meshes
# output spaces fsaverage and MNI152NLin6Asym:res-2 are preferred by CONN

docker run --rm -it  -v /Users/dpat/license.txt:/opt/freesurfer/license.txt:ro -v "${PWD}"/data:/data:ro -v "${PWD}"/derivatives:/out -v "${PWD}"/fmriprep_work:/work nipreps/fmriprep:latest /data /out participant --participant_label ${*} -w /work --stop-on-first-crash --use-aroma --cifti-output --ignore slicetiming --output-spaces fsnative fsaverage MNI152NLin6Asym:res-2