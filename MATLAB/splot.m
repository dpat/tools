function splot( y,x,file)
% Creates the scatterplot I want with a best fit line:
% example: splot('laterality', 'Age','B_compare_rl_arc')
% This command will assume a dataset with a "laterality" column and an
% "Age" column.  An output fig file will be created in the current directory 
% with the dataset name and both variable names: 
% e.g., B_compare_rl_arc_Age_laterality.fig and .jpg

set(0,'defaulttextinterpreter','none')
% concatenate strings
xvar=char(strcat((file),'.',(x)))
yvar=char(strcat((file),'.',(y)))
% look in the base workspace for the variables I have just named.
basex=evalin('base',xvar);
basey=evalin('base',yvar);
% Create the scatterplot with relatively large filled black markers.
f=scatter(basex, basey, 'MarkerEdgeColor','k','MarkerFaceColor','k','LineWidth',3)
% Create large x and y labels
xlabel(x, 'FontSize', 20);
ylabel(y, 'FontSize', 20);
% add a best fit line
h=lsline;
% set line color to red
set(h(1), 'color','r');
% set line width to be thicker
set(h(1), 'LineWidth', 3.0);
% Create the fig file name with the names of the contributing parts
name=char(strcat((file),'_',(x),'_',(y)));
jpeg=char(strcat((file),'_',(x),'_',(y),'.jpg'));
% save the fig file with the names of the contributing parts
saveas(f, name);
saveas(f, jpeg);
end
