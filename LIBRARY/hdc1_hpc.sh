#!/bin/bash

: <<COMMENTBLOCK
This code calls the Apptainer heudiconv tool to do the first step in converting DICOMS into the BIDS.
It requires that you are in the parent directory of both the Dicom and Nifti directories. 
It assumes the Dicom directory contains subjects, and under each subject is a sequence folder containing the actual dicoms.
See https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/heudiconv.html.
Aidan Dolby edited the file to run with apptainer 12/8/2023.
COMMENTBLOCK

#==============================================================================

function NoSess ()
{
: <<COMMENTBLOCK

   Function:   NoSess
   Purpose:    Run stage 1 of heudiconv for the case with no sessions
   Input:      3 arguments
   Output:     Two files in the code directory: dicominfo*.tsv and heuristic_template.py

COMMENTBLOCK

# Ensure there is a code subdir in the bids dir
if [ ! -d ${bidsdir}/code ]; then 
  mkdir -p ${bidsdir}/code/ 
fi

# If there are two arguments then there is no session
if [ $# -eq 3 ]; then
  echo ""
  echo "================================================"
  echo ""
  echo "A hidden .heudiconv directory will be created: ${bidsdir}/.heudiconv/sub-${subject}/info"
  echo "The dicominfo.tsv file will be copied to ${bidsdir}/code/dicominfo_sub-${subject}.tsv"
  echo "A template translator file will be created in ${bidsdir}/code/heuristic_template.py"
  echo ""
  # Remove any pesky .DS_Store files in the dicomdir
  find ${dicomdir} -type f -name .DS_Store -exec rm {} \;
  echo "nesting check: This should list a real DICOM file (*.dcm or *.IMA).  If it lists anything else, there is a problem with your dicom directory:"
  filepath=$(find ${dicomdir}/${subject} -type f | head -n 1)
  echo "${filepath}"   
  echo ""
  echo "================================================"
  echo ""
  
  filepathdepth=$( echo ${filepath} | awk -F"/" 'NF > max {max = NF} END {print max-3}' )

  thresh=1
  while [  ${thresh} -le ${filepathdepth} ]; do
    nest="${nest}/*"
    ((thresh+=1))
  done

  echo ""
  echo "================================================"
  echo ""
  apptainer run --cleanenv --bind ${PWD}:/base ${APTLOC} -d /base/${dicomdir}/{subject}${nest}/*.* -o /base/${bidsdir}/ -f convertall -s ${subject} -c none --minmeta --overwrite
  
  echo "This is the Apptainer command being run"
  echo "apptainer run --cleanenv --bind ${PWD}:/base ${APTLOC} -d /base/${dicomdir}/{subject}${nest}/*.* -o /base/${bidsdir}/ -f convertall -s ${subject} -c none --minmeta --overwrite"
  # Copy the dicominfo*.tsv file out of the hidden directory and give it a unique name
  cp ${bidsdir}/.heudiconv/${subject}/info/dicominfo*.tsv ${bidsdir}/code/dicominfo_sub-${subject}.tsv
  # else if there are 4 arguments, there is a session
  else
  HelpMessage
  exit 1
fi

echo "======================================================================"
echo ""
echo "########################################"
echo "INSTRUCTIONS"
echo "########################################"
echo ""
echo "See ${bidsdir}/code/heuristic_template.py for a template translator script, which you should rename and modify"
echo "See ${bidsdir}/code/dicominfo_sub-${subject}.tsv for a spreadsheet of available criteria"
echo ""
echo "See https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/heudiconv.html#exploring-criteria for more criteria examples" 
echo ""
echo "======================================================================"

# Create a template with common choices
NoSessTemplate

}

#==============================================================================

: <<COMMENTBLOCK

   Function:   NoSessTemplate
   Purpose:    function_purpose
   Input:      input_files
   Output:     output

COMMENTBLOCK

function NoSessTemplate ()
{
# Create a template for non-session data with common choices
cat << EOF > ${bidsdir}/code/heuristic_template.py
import os

def create_key(template, outtype=('nii.gz',), annotation_classes=None):
    if template is None or not template:
        raise ValueError('Template must be a valid format string')
    return template, outtype, annotation_classes


def infotodict(seqinfo):
    # Section 1: Define BIDS output names

    ##########################################################

    # The "data" key creates a counter with sequential numbers you can use for naming sequences.
    # This is especially valuable if you run the same sequence multiple times at the scanner.
    # But, you should try to give a unique protocol name to each sequence you run so you don't need this!
    # data = create_key('run{item:03d}')
    # Examples of using the counter to label files with sequential numbers
    # func_train = create_key('sub-{subject}/func/sub-{subject}_task-train_run-{item:03d}_bold')
    # This sequence produces several echoes.  Here you need the counter to label the echos.
    # t2_3dax_mag = create_key('sub-{subject}/anat/sub-{subject}_acq-3dax_echo-{item:03d}_part-mag_MEGRE')
    
    ##########################################################

    # The following are common sequences you might run.  
    # These names are for subjects WITHOUT sessions.
    # You should remove any sequences you aren't using.
    # If you have other sequences not demonstrated here, you may need to look up the specification:
    # https://bids-specification.readthedocs.io/en/stable/04-modality-specific-files/01-magnetic-resonance-imaging-data.html
    # Maintain the indentation of the lines!  Python is very picky about this.    

    anat_t1w = create_key('sub-{subject}/anat/sub-{subject}_T1w')
    anat_flair = create_key('sub-{subject}/anat/sub-{subject}_FLAIR')
    anat_t2w = create_key('sub-{subject}/anat/sub-{subject}_T2w')
    func_fmri_run1 = create_key('sub-{subject}/func/sub-{subject}_task-rest_run-01_bold')
    fmap_mag = create_key('sub-{subject}/fmap/sub-{subject}_magnitude')
    fmap_phase = create_key('sub-{subject}/fmap/sub-{subject}_phasediff')
    dwi_ap = create_key('sub-{subject}/dwi/sub-{subject}_dir-AP_dwi')
    fmap_dwi_pa =  create_key('sub-{subject}/fmap/sub-{subject}_dir-PA_epi')
    perf_asl = create_key('sub-{subject}/perf/sub-{subject}_asl')  
    
    # Example of naming WITH sessions: Add session to the path and file name
    # The 'ses-' prefix will be added automatically
    # t1w = create_key('sub-{subject}/{session}/anat/sub-{subject}_{session}_T1w')
    
    # Section 1b: Add keys to dictionary
    ##########################################################

    # Any key you defined above must appear in this dictionary if it is to be exported to BIDS.
    # Remove or add items to the dictionary as needed.

    info = {anat_t1w: [], anat_flair: [], anat_t2w: [], func_fmri_run1: [], fmap_mag: [], fmap_phase: [], dwi_ap: [], fmap_dwi_pa: [], perf_asl: [] }
    last_run = len(seqinfo)
        
    # Section 2: Example Criteria for selecting DICOMS
    ##########################################################

    # Define test criteria to identify each DICOM sequence:
    # The goal is to define criteria that will be consistent across subjects
    # seqinfo (s) refers to information in dicominfo*.tsv file. 
    # Consult that file for available criteria.
    # Each sequence to export must have been defined in Section 1 and included in Section 1b above.
    # The following for-loop illustrates the use of multiple criteria.  You must adapt your script
    # to use the names and values in your dicominfo*.tsv.
    # Maintain the indentation of the lines!  Python is very picky about this.    

    for idx, s in enumerate(seqinfo):
        # T1w: the string 'MPRAGE' must appear somewhere in the protocol_name
        if ('MPRAGE' in s.protocol_name):
            info[anat_t1w].append(s.series_id)
        # FLAIR: The string 'FLAIR' must appear somewhere in the protocol_name
        if ('FLAIR' in s.protocol_name):
            info[anat_flair].append(s.series_id)
        # T2w hippocampus image: the protocol_name must equal 'HighResHippocampus'
        if ('HighResHippocampus' == s.protocol_name):
            info[anat_t2w].append(s.series_id)
        # The protocol_name must equal 'Axial 3D ME T2 GRE (MSV21)' and the entry 'M' must appear in the image_type tuple'
        # This entry is commented out, but useful for illustration of using criteria from the image_type tuple.
        # if ('Axial 3D ME T2 GRE (MSV21)' ==  s.protocol_name) and ('M' in s.image_type):
        #    info[t2_3dax_mag].append(s.series_id)
        # FMRI: The protocol name must equal 'RS-FMRI-MB' AND the sequence must NOT be motion corrected or any derived.
        if ('RS-FMRI-MB' == s.protocol_name) and (not s.is_motion_corrected) and (not s.is_derived):
            info[func_fmri_run1].append(s.series_id)
        # MAG: Dimension 3 must equal 64, and the string 'fmap' must appear somewhere in the protocol_name
        if ('fmap' in s.protocol_name) and (s.dim3 == 64):
            info[fmap_mag].append(s.series_id)
        # PHASE: Dimension 3 must equal 32, and the string 'fmap' must appear somewhere in the protocol_name
        if ('fmap' in s.protocol_name) and (s.dim3 == 32):
            info[fmap_phase].append(s.series_id)
        # DWI: The protocol name must include the string 'DTI' and dimensions 3 and 4 must meet the given size criteria. 
        if ('DTI' in s.protocol_name) and (s.dim3 == 66) and (s.dim4 == 74):
            info[dwi_ap].append(s.series_id)
        # DWI RPE B0 images for distortion correction: The protocol name must include the string 'DTI_REVERSE'  
        if ('DTI_REVERSE' in s.protocol_name):
            info[fmap_dwi_pa].append(s.series_id)
        # ASL: The protocol name must be 'AXIAL 3D PASL plus M0' and the sequence must not not be derived.
        # This ensures I get the raw dataset and not the derived-reconstructed one.
        if ('AXIAL 3D PASL plus M0' == s.series_description) and (not s.is_derived):
            info[perf_asl].append(s.series_id)
    return info

EOF
}

#==============================================================================

function Sess ()
{
: <<COMMENTBLOCK

   Function:   Sess
   Purpose:    Run heudiconv stage 1 for subject with session
   Input:      4 arguments
   Output:     Two files in the code directory: dicominfo*.tsv and heuristic_template.py

COMMENTBLOCK

# Ensure there is a code subdir in the bids dir
if [ ! -d ${bidsdir}/code ]; then 
  mkdir -p ${bidsdir}/code/ 
fi



# If there are two arguments then there is no session
if [ $# -eq 4 ]; then
  echo ""
  echo "================================================"
  echo ""
  echo "A hidden .heudiconv directory will be created: ${bidsdir}/.heudiconv/sub-${subject}/info"
  echo "The dicominfo.tsv file will be copied to ${bidsdir}/code/dicominfo_sub-${subject}_ses-${session}.tsv"
  echo "A template translator file will be created in ${bidsdir}/code/heuristic_template.py"
  echo ""
  # Remove any pesky .DS_Store files in the dicomdir
  find ${dicomdir} -type f -name .DS_Store -exec rm {} \;
  echo "nesting check: This should list a real DICOM file (*.dcm or *.IMA). If it lists anything else, there is a problem with your dicom directory:"
  filepath=$(find ${dicomdir}/${subject}/${session} -type f | head -n 1)
  echo "${filepath}"   
  echo ""
  echo "================================================"
  echo ""
  # Because there is a session directory, we need to subtract 4 from max
  filepathdepth=$( echo ${filepath} | awk -F"/" 'NF > max {max = NF} END {print max-4}' )

  thresh=1
  while [  ${thresh} -le ${filepathdepth} ]; do
    nest="${nest}/*"
    ((thresh+=1))
  done
  # --cleanenv --no-home
  apptainer run --cleanenv --bind ${PWD}:/base ${APTLOC} -d /base/${dicomdir}/{subject}/{session}${nest}/*.* -o /base/${bidsdir}/ -f convertall -s ${subject} -ss ${session} -c none --overwrite
  
   
  echo "This is the Singularity command being run"
  echo "apptainer run --cleanenv --bind ${PWD}:/base ${APTLOC} -d /base/${dicomdir}/{subject}/{session}${nest}/*.* -o /base/${bidsdir}/ -f convertall -s ${subject} -ss ${session} -c none --minmeta --overwrite"
  
  # Copy the dicominfo*.tsv file out of the hidden directory and give it a unique name
  cp ${bidsdir}/.heudiconv/${subject}/info/dicominfo*.tsv ${bidsdir}/code/dicominfo_sub-${subject}_ses-${session}.tsv
# For anything else, print the help message and stop.
else
  HelpMessage
  exit 1
fi

echo "======================================================================"
echo ""
echo "########################################"
echo "INSTRUCTIONS"
echo "########################################"
echo ""
echo "See ${bidsdir}/code/heuristic_template_sess.py for a template translator script, which you should rename and modify"
echo "See ${bidsdir}/code/dicominfo_sub-${subject}_ses-${session}.tsv for a spreadsheet of available criteria"
echo ""
echo "See https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/heudiconv.html#exploring-criteria for more criteria examples" 
echo ""
echo "======================================================================"

# Create a template with common choices
SessTemplate

}

#==============================================================================

: <<COMMENTBLOCK

   Function:   SessTemplate
   Purpose:    function_purpose
   Input:      input_files
   Output:     output

COMMENTBLOCK

function SessTemplate ()
{
# Create a template for non-session data with common choices
cat << EOF > ${bidsdir}/code/heuristic_template_sess.py
import os

def create_key(template, outtype=('nii.gz',), annotation_classes=None):
    if template is None or not template:
        raise ValueError('Template must be a valid format string')
    return template, outtype, annotation_classes


def infotodict(seqinfo):
    # Section 1: Define BIDS output names

    ##########################################################

    # The "data" key creates a counter with sequential numbers you can use for naming sequences.
    # This is especially valuable if you run the same sequence multiple times at the scanner.
    # But, you should try to give a unique protocol name to each sequence you run so you don't need this!
    # data = create_key('run{item:03d}')
    # Examples of using the counter to label files with sequential numbers
    # func_train = create_key('sub-{subject}/{session}/func/sub-{subject}_{session}_task-train_run-{item:03d}_bold')
    # This sequence produces several echoes.  Use the counter to label the echos.
    # t2_3dax_mag = create_key('sub-{subject}/{session}/anat/sub-{subject}_{session}_acq-3dax_echo-{item:03d}_part-mag_MEGRE')

    ##########################################################

    # The following are common sequences you might run.  These names are for subjects WITH sessions.
    # You should remove any sequences you aren't using.
    # If you have other sequences not demonstrated here, you may need to look up the specification:
    # https://bids-specification.readthedocs.io/en/stable/04-modality-specific-files/01-magnetic-resonance-imaging-data.html
    # Maintain the indentation of the lines!  Python is very picky about this.    

    anat_t1w = create_key('sub-{subject}/{session}/anat/sub-{subject}_{session}_T1w')
    anat_flair = create_key('sub-{subject}/{session}/anat/sub-{subject}_{session}_FLAIR')
    anat_t2w = create_key('sub-{subject}/{session}/anat/sub-{subject}_{session}_T2w')
    func_fmri_run1 = create_key('sub-{subject}/{session}/func/sub-{subject}_{session}_task-rest_run-01_bold')
    fmap_mag = create_key('sub-{subject}/{session}/fmap/sub-{subject}_{session}_magnitude')
    fmap_phase = create_key('sub-{subject}/{session}/fmap/sub-{subject}_{session}_phasediff')
    dwi_ap = create_key('sub-{subject}/{session}/dwi/sub-{subject}_{session}_dir-AP_dwi')
    fmap_dwi_pa =  create_key('sub-{subject}/{session}/fmap/sub-{subject}_{session}_dir-PA_epi')
    perf_asl = create_key('sub-{subject}/{session}/perf/sub-{subject}_{session}_asl')  
   
    # Example of naming WITHOUT sessions: Remove session from the path and the filename:
    # t1w = create_key('sub-{subject}/anat/sub-{subject}_T1w')
    
    # Section 1b: Add keys to dictionary
    ##########################################################

    # Any key you defined above must appear in this dictionary if it is to be exported to BIDS.
    # Remove or add items to the dictionary as needed.

    info = {anat_t1w: [], anat_flair: [], anat_t2w: [], func_fmri_run1: [], fmap_mag: [], fmap_phase: [], dwi_ap: [], fmap_dwi_pa: [], perf_asl: [] }
    last_run = len(seqinfo)
        
    # Section 2: Example Criteria for selecting DICOMS
    ##########################################################

    # Define test criteria to identify each DICOM sequence:
    # The goal is to define criteria that will be consistent across subjects
    # seqinfo (s) refers to information in dicominfo*.tsv file. 
    # Consult that file for available criteria.
    # Each sequence to export must have been defined in Section 1 and included in Section 1b above.
    # The following for-loop illustrates the use of multiple criteria.  You must adapt your script
    # to use the names and values in your dicominfo*.tsv.
    # Maintain the indentation of the lines!  Python is very picky about this.    

    for idx, s in enumerate(seqinfo):
        # T1w: the string 'mprage' must appear somewhere in the protocol_name
        if ('MPRAGE' in s.protocol_name):
            info[anat_t1w].append(s.series_id)
        # FLAIR: The string 'FLAIR' must appear somewhere in the protocol_name
        if ('FLAIR' in s.protocol_name):
            info[anat_flair].append(s.series_id)
        # T2w hippocampus image: the protocol_name must equal 'HighResHippocampus'
        if ('HighResHippocampus' == s.protocol_name):
            info[anat_t2w].append(s.series_id)
        # The protocol_name must equal 'Axial 3D ME T2 GRE (MSV21)' and the entry 'M' must appear in the image_type tuple'
        # This entry is commented out, but useful for illustration of using criteria from the image_type tuple.
        # if ('Axial 3D ME T2 GRE (MSV21)' ==  s.protocol_name) and ('M' in s.image_type):
        #    info[t2_3dax_mag].append(s.series_id)
        # FMRI: The protocol name must equal 'RS-FMRI-MB' and the sequence must NOT be motion corrected or any derived.
        if ('RS-FMRI-MB' == s.protocol_name) and (not s.is_motion_corrected) and (not s.is_derived):
            info[func_fmri_run1].append(s.series_id)
        # MAG: Dimension 3 must equal 64, and the string 'field_mapping' must appear somewhere in the protocol_name
        if ('fmap' in s.protocol_name) and  (s.dim3 == 64):
            info[fmap_mag].append(s.series_id)
        # PHASE: Dimension 3 must equal 32, and the string 'field_mapping' must appear somewhere in the protocol_name
        if ('fmap' in s.protocol_name) and (s.dim3 == 32):
            info[fmap_phase].append(s.series_id)
        # DWI: The protocol name must include the string 'DTI' and dimensions 3 and 4 must meet the given size criteria. 
        if ('DTI' in s.protocol_name) and (s.dim3 == 66) and (s.dim4 == 74):
            info[dwi_ap].append(s.series_id)
        # DWI RPE B0 images for distortion correction: The protocol name must include the string 'DTI_REVERSE'  
        if ('DTI_REVERSE' in s.protocol_name):
            info[fmap_dwi_pa].append(s.series_id)
        # ASL: The protocol name must be 'AXIAL 3D PASL plus M0' and the sequence must not be derived.
        # This ensures I get the raw dataset and not the derived-reconstructed one.
        if ('AXIAL 3D PASL plus M0' == s.series_description) and (not s.is_derived):
            info[perf_asl].append(s.series_id)
    return info

EOF
}

#==============================================================================
: <<COMMENTBLOCK

   Function:   HelpMessage
   Purpose:    function_purpose
   Input:      input_files
   Output:     output

COMMENTBLOCK

function HelpMessage ()
{
cat <<- "EOF"
======================================================
Three arguments are required. An optional 4th argument can be added to encode the session if there is one
argument 1: Name of Dicom folder (e.g. Dicom)
argument 2: Name of BIDS folder for NIfTI images (e.g. data)
argument 3: Name of subject in DICOM folder to convert (e.g., 304)

e.g., hdc1.sh Dicom data 304

OPTIONAL SESSION ARGUMENT
argument 4: Optional session argument, (e.g., itbs)  
  
e.g., hdc1.sh Dicom data 304 itbs

You must be running from the parent directory to both the Dicom and BIDS folders
If you have a session argument, this assumes your DICOMS are nested under subject and then session.

======================================================
EOF
exit 1
}

#########################  END FUNCTION DEFINITIONS  ##########################
###############################################################################

# Location of Apptainer container
APTLOC=${SIF}/heudiconv_v1.0.0.sif #set location of singluarity file

while getopts h options 2> /dev/null; do        # Setup -h flag. redirect errors
  case $options in                              # In case someone invokes -h
  h) HelpMessage;;                              # Run the help message function
  \?) echo "only h is a valid flag" 1>&2        # If bad options are passed, print message
  esac
done

shift $(( $OPTIND -1))                          # Index option count, so h isn't treated as arg
optnum=$(( $OPTIND -1))                        	# optnums hold # of options passed in

if [ ${optnum} -lt 1 ] && [ $# -lt 1 ]; then    # If no options or arg
    HelpMessage                                 # then display HelpMessage
fi

if [ ${optnum} -lt 1 ] && [ $# -eq 4 ]; then    # If no options but 4 args
  # Define the variables
  dicomdir=${1}
  bidsdir=${2}
  subject=${3}
  session=${4}  

  Sess ${dicomdir} ${bidsdir} ${subject} ${session} # Default to running Main with session

elif [ ${optnum} -lt 1 ] && [ $# -eq 3 ]; then    # If no options but 3 args
  # Define the variables
  dicomdir=${1}
  bidsdir=${2}
  subject=${3} 
  NoSess ${dicomdir} ${bidsdir} ${subject}   # Running Main with non-session
else
  $1                                       # Run test function Clean or CalcNesting
fi
