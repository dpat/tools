 function clust= toMatT( input_image, output_mat )

% INPUT FILE should be UNZIPPED 3D mask
% See hp_wrap which provides the input_image and output_mat
% Output is a 2D vector (reshaped from 3D)
% Based on ijn1.m
% Dianne Patterson, Created: Aug 15, 2013
% Calls tools in the nifti_io toolbox from http://www.rotman-baycrest.on.ca/~jimmy/NIFTI/

% This assumes Tstats from afni, which have had 100 added to them and 
% are in a 5D array.  Only the first 4D array contains the T-stats, so we
% dump the 2nd one in toMatT.  Finally, having squeezed the results to get
% rid of 0's we are free to subtract 100 from the remaining values to get
% our original T-values back.

ext='.nii';
img=load_nii([input_image ext]);
clear clust X vals
X=double(img.img); %extract the raw image from the structure, make it a double to be more compatible with other matlab functions
clear img
X1=X(:,:,:,:,1);
X1=flipdim(X1,1); % do the left-right flip to make sure coordinates are consistent with toMatXYZ.
clear X
[lin]=find(X1(:,:,:,1));  %identify the linear index of any nonzero value in the first of the 4d volumes.
X1size=size(X1); % Get the matrix size
[row,col,page]=ind2sub(X1size,lin); % extract the row, column and page from the linear index
% This corrects for the 0-1 difference between fsl and matlab
coords=[row-1,col-1,page-1];
coords=coords';
Rlen=length(row);% Get the matrix size
vals=zeros(size(X1,4),Rlen); % create an empty matrix of the correct size
for i=1:Rlen  % sadly, use a for loop
     vals(:,i)=squeeze(X1(row(i),col(i),page(i),:)); % get each timeseries, squeeze it and put it in the matrix

    i=i+1; % increment i
end; % end the for loop
outname=[output_mat];
clust=vals-100; % subtract 100 from each value in the squeezed matrix, and set the output name (now we should have t-values back)
save (outname, 'clust');
clear 
 end 


