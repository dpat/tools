% run this from the directory containg the subdirs ax, cor and sag
% geo_wrap calls nifti2geojson iteratively so that lots of files can be converted.
% you may wish to alter this script to run nifti2geojson for your own purposes
% axials
cd ax;
for i=[24:12:288]        % start with slice 24 and process every 12th slice until you reach slice 288
	slice=num2str(i);  % create a string to hold the slice number and use in naming input_image to process
	filename=char(strcat('ax_',(slice)));  % construct a name for each axial slice
     nifti2geojson(filename, 'a','../labels_all.txt',0.5,1);
end

% coronals
cd ..;
cd cor;
for i=[48:12:396]
	slice=num2str(i);  % create a string to hold the slice number and use in naming input_image to process
	filename=char(strcat('cor_',(slice)));  % construct a name for each axial slice
     nifti2geojson(filename, 'c','../labels_all.txt',0.5,1);
end

% sagittals
cd ..;
cd sag;

for i=[48:12:312]
	slice=num2str(i);  % create a string to hold the slice number and use in naming input_image to process
	filename=char(strcat('sag_',(slice)));  % construct a name for each axial slice
     nifti2geojson(filename, 's','../labels_all.txt',0.5,1);
end

% Because the 0 slice is completely blank, we can't use it, so it should be deleted
delete sag_180_L_0mm.geojson

% Because the 0 slice is completely blank, we can't use it, but this should be an equivalent non-blank slice
nifti2geojson('sag_181', 's','../labels_all.txt',0.5,1);

