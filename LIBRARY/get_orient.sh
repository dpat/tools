#!/bin/bash

: <<COMMENTBLOCK
Look inside the NIFTI header to determine whether the axes are Left to right or Right to Left.
Right to left should be the standard.  Although any proper program should read the orientations from the header and display correctly, it is sometimes the case that a program assumes Right-to-left, and thus displays incorrectly. To change the orientation to the standard, apply reset_orient.sh
COMMENTBLOCK

img=$1
orientq=$(fslhd ${img} | grep -m 1 qform_xorient | awk '{print $1, $2}')
orients=$(fslhd ${img} | grep -m 1 sform_xorient | awk '{print $1, $2}')
echo "${img}:"
echo "${orientq}"
echo "${orients}"
echo "===================="
