function img2adj(input_image, dim_x, dim_y, dim_z)
% INPUT FILE should be UNZIPPED 4D mask
%
% This function takes a 4d time series image and correlates each time
% series to every other timeseries in the image.  The resulting workspace
% is saved and should be reloaded when one wants to create a vec_metric by
% running BCT tools which create an output vector.
% Arguments are: 4dinput_file, and x,y,z dimensions of output file voxels.
% *.nii extension assumed/added
% example: img2adj('thal_l', 3.75, 3.75, 5)
% Dianne Patterson, Created: Dec 29 2011
% Calls tools in the nifti_io toolbox from http://www.rotman-baycrest.on.ca/~jimmy/NIFTI/

tic
ext='.nii';
img=load_nii([input_image ext]);
X=single(img.img); % extract the raw image from the structure, make it a double to be more compatible with other matlab functions
clear img; % clean up to clear memory
[lin]=find(X(:,:,:,1));  % identify the linear index of any nonzero value in the first of the 4d volumes.
x1=size(X,1); % I need is the size of x, so later I can delete X
x2=size(X,2);
x3=size(X,3);
x4=size(X,4);
[row,col,page]=ind2sub([x1,x2,x3,x4],lin); % extract the row, column and page from the linear index
newsize=x1* x2* x3; % multiply the first 3 dims to get the size for reshape
vals=reshape(X,newsize,x4); % reshape into a 2D matrix
clear X newsize; % cleanup to increase efficiency
missing0=(vals)==0; % identify zeros as missing values
vals(missing0)=NaN; % set all missing values to NaNs
vals(any(isnan(vals),2),:)=[]; % remove rows with missing values
clear missing0
[r,p]=corrcoef(vals'); % get the correlation coefficients of timeseries to timeseries (r-value)
% MAKE MATLAB IGNORE IRRELEVANT VALUES (like one)
missing1=(r)==1; % create a matrix identifying all 1 values 
r(missing1)=0; %swap in 0s for 1s
clear ext lin missing1 vals % This variable is very large, but has served its purpose. Get rid of it.
save ([input_image '_matlab']); % save workspace
toc