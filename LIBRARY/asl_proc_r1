#!/bin/bash

# Run from the basedir or cd to it: This should be the subject directory containing the ${subj}_T1w.anat
# An fmap directory containing the magnitude image, brain extracted magnitude and fmap_rads
# A perf directory containing the *asl_m0scan.nii.gz and *asl_cl.nii.gz
# basedir=/Volumes/Main/Working/Tool_Testing/basil/data/outputs

if [ $# -lt 3 ]; then
  echo "Run from the parent directory, e.g., /Volumes/Main/Working/Tool_Testing/basil/data/outputs/ if sub-001 is in that directory"
  echo "Specify arguments: subj, out, and run"
  echo "e.g., 3 arguments:" 
  echo "asl_proc_r1 sub-001 basil_r1_1 run-01"
  echo "WARNING: This script assumes certain values for oxford_asl which may not be appropriate for your data! Use it as an example only"
  exit 1
fi

if [ $# -eq 3 ]; then
  # Pass in ${subj}, e.g sub-001
  subj=$1
  # Pass in the output dir name
  out=$2
  # Pass in the run, e.g, run-01 
  run=$3
  
  input_asl=${subj}/perf/${run}/${subj}_${run}_asl_cl.nii.gz
  input_m0=${subj}/perf/${run}/${subj}_${run}_asl_m0scan.nii.gz
 # output directory will be created if it does not exist
  outdir=${subj}/perf/${run}/${out}
  echo "==========================="
  echo "Subject is ${subj}"
  echo "run is ${run}"
  echo "output directory is ${outdir}"
fi

echo "==========================="

# Single ti version run-01, standard-buxton model
# Be careful about moving the flags and arguments around in the command-line (don't do it)!

oxford_asl -i ${input_asl} --iaf ct --ibf rpt --bolus 0.8 --rpts 10 --tis 2.75 \
--fslanat ${subj}/${subj}_T1w.anat -c ${input_m0} --cmethod single --tr 4 --cgain 1 --region-analysis \
--tissref csf --t1csf 4.3 --t2csf 750 --t2bl 150 --te 0 --echospacing 2.08e-05 \
--pedir -y --fmap ${subj}/fmap/fmap_rads.nii.gz --fmapmag ${subj}/fmap/${subj}_magnitude1.nii.gz \
--fmapmagbrain ${subj}/fmap/${subj}_magnitude_brain.nii.gz \
-o ${outdir} --bat 0.7 --t1 1.3 --t1b 1.65 --alpha 0.98 --spatial --fixbolus --mc --pvcorr --artoff


echo "==========================="
echo "Processing is done"
echo "output directory is ${outdir}"
echo "=========================="