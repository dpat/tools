#!/bin/bash

: <<COMMENTBLOCK
Find the difference and percent difference between 2 images
COMMENTBLOCK

if [ $# -lt 2 ]
then
  echo "Usage: $0 <filename1> <filename2>"
  echo "Example $0 im1 im2"
  echo "imdiff calculates the difference between 2 greyscale volumes"
  echo "the diff image is the raw subtraction"
  echo "the diffper image is the percent difference."
  echo "these can be viewe with one of the render colormaps in fsleyes"
  echo "because the render colormaps have a negative and positive range"
  echo "if im1 is larger then differences are positive"
  echo "if im2 is larger then differences are negative"
  echo "a text file imdiff.txt is created and populated with mean and sd"
  echo "of the diffper image AND mean and SD of the squared error image."
  exit 1
fi

im1=$1 # Define the first argument as im1
im2=$2 # Define the second argument as im2
stem1=`basename -s .nii.gz ${im1}`
stem2=`basename -s .nii.gz ${im2}`

# Get the raw difference between images by subtracting
fslmaths ${im1} -sub ${im2} diff_${stem1}_${stem2}
# Convert the raw differences to percents
fslmaths diff_${stem1}_${stem2} -mul 100 diffper_${stem1}_${stem2}
# Square the differences (to get rid of negatives).  This gives us squared errors
fslmaths diff_${stem1}_${stem2} -mul diff_${stem1}_${stem2} se_${stem1}_${stem2}

# Get the mean and standard deviation of non-zero voxels in the diffper image
mean_diffper=`fslstats diffper_${stem1}_${stem2} -M`
sd_diffper=`fslstats diffper_${stem1}_${stem2} -S`

# This should be mean absolute error
mean_se=`fslstats se_${stem1}_${stem2} -M`
sd_se=`fslstats se_${stem1}_${stem2} -S`
# echo im1 im2 mean and sd of diffper to the text file.
# create the header row if diffper_${stem1}_${stem2}.txt does not exist.
if [ ! -e imdiff.txt ]; then
	touch imdiff.txt
	echo "im1 im2 mean_diffper sd_diffper mean_se sd_se" > imdiff.txt
fi

echo "${stem1} ${stem2} ${mean_diffper} ${sd_diffper} ${mean_se} ${sd_se}" >>imdiff.txt
