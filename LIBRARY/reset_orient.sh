#!/bin/bash

: <<COMMENTBLOCK
Alter the orientation info in the header. Everything that *should* be on the left should remain on the left after this manipulation. We are just changing information inside the NIFTI header to be in the standard Right-to-Left orientation. Although any proper program should read the orientations from the header and display correctly, it is sometimes the case that a program assumes Right-to-Left, and thus displays incorrectly. To simply view the orientation, use get_orient.sh
COMMENTBLOCK

img=$1

# Alter the header information
fslswapdim ${img} -x y z swap_${img}
fslorient -swaporient swap_${img}
mv swap_${img} ${img}

# Report the header information now that it has been altered.
orientq=$(fslhd ${img} | grep -m 1 qform_xorient | awk '{print $1, $2}')
orients=$(fslhd ${img} | grep -m 1 sform_xorient | awk '{print $1, $2}')
echo "${img}:"
echo "${orientq}"
echo "${orients}"
echo "===================="
