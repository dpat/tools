#!/bin/bash

#============================================================================
: <<COMMENTBLOCK

Function: AddCortices
Purpose: 	Add cortex images together to make the atlas
Input: 		Assumes a subdir Cortices exists containing label masks for
					each cortex created by MkCortices_L and MkCortices_R
Output: 	Makes the concatenated label atlas.
COMMENTBLOCK

AddCortices ()
{
outimg=HCPMMP1_cortex_atlas
for label_img in Cortices/*.nii.gz; do
	if [ ! -e  ${outimg}.nii.gz ]; then
 		imcp ${label_img} ${outimg}
 		# subtract the image from itself to get an empty image.
 		fslmaths ${outimg} -sub ${outimg} ${outimg} -odt short
	fi
		fslmaths ${outimg} -add ${label_img} ${outimg}
		echo "adding ${label_img} to ${outimg}"
done

}

#============================================================================
: <<COMMENTBLOCK

Function: MkCortices_L
Purpose: 	Add images together to make each cortex (Glasser "region") mask
Input: 		Assumes a subdir ROISxId containing roi indexed binary masks for each region
Output: 	Makes the concatenated "region" masks

COMMENTBLOCK

MkCortices_L ()
{
for c_indx in {1..22}; do
  cortex=cortex_${c_indx}
	list=L_list_${c_indx}
	# echo the variable name and its contents
	echo "${cortex} contains ${!cortex}"
	echo "${list} contains ${!list}"
	outimg=${!cortex}_L
	for roi in ${!list}; do
		area=ROISxID/roi_${roi}.nii.gz
		if [ ! -e  ${outimg}.nii.gz ]; then
		 		imcp ${area} ${outimg}
		 		# subtract the image from itself to get an empty image.
		 		fslmaths ${outimg} -sub ${outimg} ${outimg} -odt short
		fi
		fslmaths ${outimg} -add ${area} ${outimg}
	done
	fslmaths ${outimg} -mul ${c_indx} ${outimg} -odt short
	label_value=`fslstats ${outimg} -M`
	echo "${outimg} ${label_value}"
	echo "=================================="
	immv ${outimg} Cortices
done


}

#============================================================================
: <<COMMENTBLOCK

Function: MkCortices_R
Purpose: 	Add images together to make each cortex (Glasser "region") mask
Input: 		Assumes a subdir ROISxId containing roi indexed binary masks for each region
Output: 	Makes the concatenated "region" masks. Each mask has an appropriate labels
					value (section +100 for the right)

COMMENTBLOCK

MkCortices_R ()
{
for c_indx in {1..22}; do
  cortex=cortex_${c_indx}
	list=R_list_${c_indx}
	# echo the variable name and its contents
	echo "${cortex} contains ${!cortex}"
	echo "${list} contains ${!list}"
	outimg=${!cortex}_R
	for roi in ${!list}; do
		area=ROISxID/roi_${roi}.nii.gz
		if [ ! -e  ${outimg}.nii.gz ]; then
		 		imcp ${area} ${outimg}
		 		# subtract the image from itself to get an empty image.
		 		fslmaths ${outimg} -sub ${outimg} ${outimg} -odt short
		fi
		fslmaths ${outimg} -add ${area} ${outimg}
	done
	# Create the value we need to multiply by
	value=`echo 100+${c_indx} | bc`
	fslmaths ${outimg} -mul ${value} ${outimg} -odt short
	label_value=`fslstats ${outimg} -M`
	echo "${outimg} ${label_value}"
	echo "=================================="
	immv ${outimg} Cortices
done

}

#============================================================================
: <<COMMENTBLOCK

Function: MkXMLlabels
Purpose: 	For the cortices, create the labels for the atlas XMLfile
Input: 		Assumes a subdir Cortices exists containing label masks for
					each cortex created by MkCortices_L and MkCortices_R
Output: 	Makes the XML labels for atlas.
COMMENTBLOCK

MkXMLlabels ()
{

for label_img in Cortices/*_L.nii.gz Cortices/*_R.nii.gz; do
	# get the label basename
	stem=`basename -s .nii.gz ${label_img}`
	max=`fslstats ${label_img} -M`
	# Identify the extension .*
	ext=`echo ${max} | cut -d'.' -f2`
	# Chop the extension off the end of the number, to make sure it is an integer
	max=`basename -s .${ext} ${max}`
	volmm=`fslstats ${label_img} -V | awk '{print $2}'`
	cogx=`fslstats ${label_img} -C | awk '{print $1}'`
	cogy=`fslstats ${label_img} -C | awk '{print $2}'`
	cogz=`fslstats ${label_img} -C | awk '{print $3}'`
	echo "<label index=\"${max}\" x=\"${cogx}\" y=\"${cogy}\" z=\"${cogz}\">${stem}</label>" >> HCP-MMP1_cortex_labels.txt
	echo "${stem}, ${max}, ${cogx}, ${cogy}, ${cogz}, ${volmm}"
	echo "${stem}, ${max}, ${cogx}, ${cogy}, ${cogz}, ${volmm}" >> HCP-MMP1_cortex_info.csv
done
}

#============================================================================
# Define the lists of regions to combine for each cortex.
L_list_1="1"
L_list_2="4 5 6"
L_list_3="3 13 16 17 19 152"
L_list_4="7 18 22 153 154 160 163"
L_list_5="2 20 21 23 138 156 157 158 159"
L_list_6="8 9 51 52 53"
L_list_7="36 37 38 39 40 41 43 44 55"
L_list_8="10 11 12 54 56 78 96"
L_list_9="99 100 101 102 113"
L_list_10="24 103 104 105 124 173 174"
L_list_11="107 123 125 128 129 130 175 176"
L_list_12="106 108 109 110 111 112 114 115 167 168 169 178"
L_list_13="118 119 120 122 126 127 135 155"
L_list_14="131 132 133 134 136 137 172 177"
L_list_15="25 28 139 140 141"
L_list_16="29 42 45 46 47 48 49 50 95 117"
L_list_17="116 143 144 145 146 147 148 149 150 151"
L_list_18="14 15 27 30 31 32 33 34 35 121 142 161 162"
L_list_19="57 58 59 60 61 62 63 64 65 69 88 164 165 166 179 180"
L_list_20="66 72 89 90 91 92 93 94 170"
L_list_21="74 75 76 77 79 80 81 82 171"
L_list_22="26 67 68 70 71 73 83 84 85 86 87 97 98"
R_list_1="201"
R_list_2="204 205 206"
R_list_3="203 213 216 217 219 352"
R_list_4="207 218 222 353 354 360 363"
R_list_5="202 220 221 223 338 356 357 358 359"
R_list_6="208 209 251 252 253"
R_list_7="236 237 238 239 240 241 243 244 255"
R_list_8="210 211 212 254 256 278 296"
R_list_9="299 300 301 302 313"
R_list_10="224 303 304 305 324 373 374"
R_list_11="307 323 325 328 329 330 375 376"
R_list_12="306 308 309 310 311 312 314 315 367 368 369 378"
R_list_13="318 319 320 322 326 327 335 355"
R_list_14="331 332 333 334 336 337 372 377"
R_list_15="225 228 339 340 341"
R_list_16="229 242 245 246 247 248 249 250 295 317"
R_list_17="316 343 344 345 346 347 348 349 350 351"
R_list_18="214 215 227 230 231 232 233 234 235 321 342 361 362"
R_list_19="257 258 259 260 261 262 263 264 265 269 288 364 365 366 379 380"
R_list_20="266 272 289 290 291 292 293 294 370"
R_list_21="274 275 276 277 279 280 281 282 371"
R_list_22="226 267 268 270 271 273 283 284 285 286 287 297 298"

# List of the 22 cortices
cortex_1="Primary_Visual"
cortex_2="Early_Visual"
cortex_3="Dorsal_Stream_Visual"
cortex_4="Ventral_Stream_Visual"
cortex_5="MT+_Complex_and_Neighboring_Visual_Areas"
cortex_6="Somatosensory_and_Motor"
cortex_7="Paracentral_Lobular_and_Mid_Cingulate"
cortex_8="Premotor"
cortex_9="Posterior_Opercular"
cortex_10="Early_Auditory"
cortex_11="Auditory_Association"
cortex_12="Insular_and_Frontal_Opercular"
cortex_13="Medial_Temporal"
cortex_14="Lateral_Temporal"
cortex_15="Temporo-Parieto-Occipital_Junction"
cortex_16="Superior_Parietal"
cortex_17="Inferior_Parietal"
cortex_18="Posterior_Cingulate"
cortex_19="Anterior_Cingulate_and_Medial_Prefrontal"
cortex_20="Orbital_and_Polar_Frontal"
cortex_21="Inferior_Frontal"
cortex_22="Dorsolateral_Prefrontal"

#MkCortices_L
#MkCortices_R
AddCortices
#MkXMLlabels
