#!/bin/bash

: <<COMMENTBLOCK
This code calls the docker fMRIPrep tool to prepprocess fMRI and structural data in the BIDS data structure. This script requires that you are in the directory where your subject subdirectories are stored in the BIDS data structure.
COMMENTBLOCK


# Exit and spit out help message if number of arguments is too small
if [ $# -lt 1 ]
  then
    echo "======================================================"
    echo "This assumes you are running docker"
    echo "and have downloaded fmriprep: docker pull pennbbl/qsiprep"
    echo "e.g. Run subject sub-304: $0 304"
    echo "e.g. Run multiple subjects: $0 304 305 306"
    echo "On the mac, ensure Docker has more than the default 2 GB of RAM:"
    echo "Docker->Preferences->Advanced"
    echo ""
    echo "run this from the project directory above data so that derivatives, data, and qsiprep_work are all siblings"
    echo "This assumes you have already run qsiprep"
    echo "======================================================"
    exit 1
fi

# Including "" around PWD ensures it will handle spaces in the path names.

# data (where your bids data resides)
#	qsiprep-output (The subdirectory created by the previous preprocessing step, by default: `derivatives/qsiprep`)
#	work (This should be outside of the *data* and *derivatives* directories and its presence will allow the creation of the reports)
#	out (Where the qsirecon directory will be created)
# This will look for a FreeSurfer directory created by fmriprep in derivatives/sourcedata/freesurfer

docker run --rm -it \
 -v /Users/dpat/license.txt:/opt/freesurfer/license.txt:ro \
 -v "${PWD}"/data:/data:ro \
 -v "${PWD}"/derivatives/qsiprep:/qsiprep-output:ro \
 -v "${PWD}"/qsiprep_work:/work \
 -v "${PWD}"/derivatives:/out \
 -v /Users/dpat/qsiprep:/home/qsiprep \
 pennbbl/qsiprep /data /out participant \
 --recon-input /qsiprep-output \
 --recon-spec mrtrix_multishell_msmt_noACT \
 --recon_only --output-resolution 1.3 -w /work -v -v \
 --fs-license-file /opt/freesurfer/license.txt \
 --freesurfer_input /out/sourcedata/freesurfer 
