#!/bin/bash

# From https://gist.github.com/CMCDragonkai/a8f5a03ca89f15b8ede256ec47dfa5c1

exec {times}> times {typescript}> typescript < "${1-/dev/stdin}"
while read -r; do [[ $REPLY = '  "stdout": [' ]] && break; done # skip to this line
LANG=C
printf "Script started on %(%c)T\n" -1 >&"$typescript" # dummy
while read -r open; [[ $open = '[' ]]; do
  read -r elapsed; read -r string; read -r close
  eval printf %b%n "$string" characters >&"$typescript" # put count in $characters
  printf "%s %s\n" "${elapsed%,}" "$characters" >&"$times"
done