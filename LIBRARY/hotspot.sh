#!/bin/bash

: <<COMMENTBLOCK

###############################################################################
AUTHOR:         Dianne Patterson University of Arizona
								Aneta Kielar, University of Arizona
DATE CREATED:   12/21/2007
DATE MODIFIED:  5/23/2020
###############################################################################
COMMENTBLOCK

#==============================================================================
: <<COMMENTBLOCK

Function: AddImages
Purpose: 	add a bunch of images together.
Input: 		name for output image
					a regular expression capturing the images to use as input
					e.g., AddImages Left_Learn l_*_learnable
					(do not add initial or final * or .nii.gz)
					Typically the input images will be binary
Output: 	an image with all the input image combined.
					Typically this will be a hotspot image resulting from adding a
					bunch of binary masks together.
Notes: 		See ExtractImages to go in the other direction and fslroi2 to
					extract from a 4d image
					See fsladd as a possible alternative

COMMENTBLOCK

AddImages ()
{

	if [ $# -lt 2 ]                             # There should be one argument.
	then
		echo "Usage: $0  <outimg> <instem>"
		echo "Example: $0  all_images run"        # Else return a usage message.
		exit 1
	fi

	outimg=$1                                # The name of the output image
	instem=$2   					  # some string shared by all the files you want to include in the mask

	for img  in  *${instem}*.nii.gz; do
		# cheap trick to make an empty mask the right size
		if [ ! -e  ${outimg}.nii.gz ]; then
			imcp ${img} ${outimg}
			echo "creating mask from ${img}"
			# subtract the image from itself to get an empty image.
			fslmaths ${outimg} -sub ${outimg} ${outimg}
		fi

		echo "${img}"
		fslmaths ${outimg} -add ${img} ${outimg}
	done

}

#==============================================================================
: <<COMMENTBLOCK

Function: MkCombo
Purpose: 	make it easier to use AddImages in the hotspot directory
					which has a limited set of names and image types.
					Run it in the directory where the files are (so you could create
					a subset of subject in a directory)
Input: 		type: A,B,tract;
					tract: i.e., the actual tract to use, such as arc_l, unc_r etc.
					This assumes the naming that results from running bip_stats.
					It assumes the images of interest are in a single directory together.
Output: 	combo_${type}_${tract}


COMMENTBLOCK

MkCombo ()
{

echo "$type"
echo "$tract"
outimg="combo_${type}_${tract}"

if [ "$type" = "A" ] || [ "$type" = "B" ]; then
	instem="roi_${type}_${tract}_*_final_stand_bin"
	echo "instem is ${instem}"
elif [ "$type" = "tract" ]; then
	instem="${type}_${tract}_*_final_stand_bin"
	echo "instem is ${instem}"
fi

AddImages ${outimg} ${instem}

}
#============================================================================
: <<COMMENTBLOCK

Function: Populate
Purpose: 	Find standard space binary mask images for each tract in tract_list.
Input: 		Assumes bids data structure.  Assumes you are in the relevant subject
					directory when you run the function.  Works well with fe scripts.
					If you use an fe script, you should run from the main BIDS dir (not derivatives)
					Assumes the tract_list, defined in Main, is correct.
Output: 	Copies of the binary masks are placed in ${output}/hotspots

COMMENTBLOCK

Populate ()
{

echo "subject is ${subj}"
echo "bids_dir is ${bids_dir}"
echo "output_dir is ${output_dir}"

if [ ! -d ${output_dir}/hotspots ]; then
	# Create the hotspot directory
	echo "${output_dir}/hotspots does not exist, creating"
	mkdir ${output_dir}/hotspots
elif [ -d ${output_dir}/hotspots ]; then
	echo ================================
  echo "hotspots already exists, good"
fi

# Copy the binary mask tracts and endpoints into the hotspots directory
for tract in ${tract_list}; do
  tract_dir=${output_dir}/${subj}/dwi/${tract}_bip
  echo "tract dir is ${tract_dir}"
  cp ${tract_dir}/*final_stand_bin.nii.gz ${output_dir}/hotspots
done

}
#============================================================================
function Help
{
echo "Help for hotspot.sh"
echo "====================================="
echo "Arguments are AddImages, Populate or MkCombo"
echo "example $0 AddImages"
echo "example: $0 Populate"
echo "example: $0 MkCombo"
echo "====================================="
echo "AddImages adds all the images in a directory"
echo "provide the output name first and the input stem second"
echo "====================================="
echo "Run MkCombo from inside ${output_dir}/hotspots"
echo "MkCombo does not care about subject numbers,"
echo "it combines all images of the type and tract specified."
echo "-------------"
echo "Types are A or B or tract"
echo "Tract is any member of the tract_list hardcoded near the end of $0:"
echo "This is the current tract_list:"
echo "$tract_list"
echo "example: $0 MkCombo A arc_l"
echo "======================================"
echo "Populate runs well from the main BIDS dir using a foreach script"
echo "Populate creates the ${output_dir}/hotspots dir and fills it with images."
echo "====================================="

exit 1

}
#============================================================================
# Run this from each subject's bids directory (or use fe_ak_ppa etc)
subj=`basename ${PWD}`
bids_dir=`dirname ${PWD}`
output_dir=${bids_dir}/derivatives/bip
# Define the list of tracts to look for.
tract_list="arc_l arc_r aslant_l aslant_r ilf_l ilf_r slf2_l slf2_r unc_l unc_r"
#dothis=$1

if [ $# -lt 1 ]; then
	Help
else
	dothis=$1
fi
if [ "$dothis" = Populate ]; then
	echo "subject is ${subj}"
	echo "If you are not in a subject directory, this will bomb"
	Populate
fi

if [ "$dothis" = MkCombo ]; then
		if [ $# -lt 2 ]                      # There should be two arguments.
		then
			echo "Usage: $0  <type> <tract>"
			echo "MkCombo requires a type and tract"
			echo "types are A or B or tract"
			echo "tract is any member of the tract_list:"
			echo "$tract_list"
			echo "Example: $0 A arc_l"        # Else return a usage message.
			exit 1
		fi
	type=$2
	tract=$3
	MkCombo ${type} ${tract}
fi

if [ "$dothis" = AddImages ]; then
		if [ $# -lt 2 ]                      # There should be two arguments.
		then
			echo "Usage: $0  <output> <input_stem>"
			echo "Example: $0 my_hotspot wlesion*"        
			exit 1
		fi
	outimg=$1
	instem=$2
fi



