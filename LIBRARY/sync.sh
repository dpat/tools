#!/bin/bash

# Designed to facilitate syncing operations
# Written by Dianne Patterson, University of Arizona 10/13/2022

#==============================================================================
: <<COMMENTBLOCK

Function:   pull
Purpose:    pull data from specified source directory on some remote machine to a local destination
Input:      
            
Output:    

COMMENTBLOCK

pull ()
{
 echo "syncing ${machine}:${source} to ${destdir} on this machine"

echo "continue? yes/no"
read answer

if [ "$answer" = "yes" ]; then
  rsync -rvtulgopzEK -e ssh ${machine}:${source} ${destdir}
else
    echo "backing out"
    exit 1
fi 
}



#==============================================================================
: <<COMMENTBLOCK

Function:   push
Purpose:    push data from specified source directory to machine and destination
Input:      
Output:     

COMMENTBLOCK

push ()
{
 echo "syncing ${source} to ${destdir} on ${machine}"

echo "continue? yes/no"
read answer

if [ "$answer" = "yes" ]; then
     rsync -rvtulgopzEK -e ssh ${source} ${machine}:${destdir}
else
    echo "backing out"
    exit 1
fi 
}

#########################  END FUNCTION DEFINITIONS  ##########################
###############################################################################


if [ $# -lt 4 ]
then
    echo "Usage: $0 <direction> <source> <machine> <destdir>"
    echo "can be used with files or directories"
    echo "Push stuff FROM the local machine TO the specified remote machine:"
    echo "Example: $0 push Surface_Formats feckless /Volumes/Express_1/Working/Teaching" 
    echo "Local is whatever you are logged in to when you start the process."  
    echo ""
    echo "Pull stuff from a remote machine (feckless) TO here"
    echo "$0 pull /Volumes/Express_1/Working/Tool_Testing/neuroimaging/fmriprep/CAM/derivatives feckless ." 
    echo ""  
    exit 1
fi

direction=$1 # (push or pull)
source=$2 # existing data that is being transferred
machine=$3 # remote machine to interact with
destdir=$4 # directory where data will end up.

if [[ ${direction} = "pull" ]]; then
  echo "pulling" 
  pull ${machine} ${source} ${destdir}
elif [[ ${direction} = "push" ]]; then
  echo "pushing"
  push ${machine} ${source} ${destdir}
fi