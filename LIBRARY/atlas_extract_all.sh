#!/bin/bash
# Written by Dianne Patterson, University of Arizona

#==============================================================================
: <<COMMENTBLOCK

Function:  DivideRLvols
Purpose:   To divide the atlas volumes into left and right rois
Input:     vol_* files
Output:    roi*_L and roi*_R

COMMENTBLOCK

function DivideRLvols
{

fslmaths "vol_"${name}  -mul ${mask2mm_L}  -bin "roi_"${name}_L
fslmaths "vol_"${name}  -mul ${mask2mm_R}  -bin  "roi_"${name}_R

}

#==============================================================================
: <<COMMENTBLOCK

Function:   GetAtlasVols
Purpose:    Create a set of volumes from the atlas, assumes right and left are already
			      encoded in the atlas (or we don't want to split left from right)
Input:      Atlas file and xml file names
Output:     A big set of named roi binary image files

COMMENTBLOCK

function GetAtlasVols
{

 atlas=$1
 xml=$2

# Get max and clean it up so it is an integer for bash
max=`fslstats ${atlas} -R | awk '{printf $2 "\n"}'`
# Identify the extension .*
ext=`echo ${max} | cut -d'.' -f2`
# Chop the extension off the end of the number
max=`basename -s .${ext} ${max}`

 # Atlas indices start at 0
 # Mask values start at 1
 index=0
 thresh=1
 # Iterate through atlas indices creating individual named volumes
 while [  ${thresh} -le ${max}  ] && [  ${index} -lt ${max}  ]; do
   RenameAtlasVols ${xml}
   fslmaths ${atlas} -thr ${thresh} -uthr ${thresh} -bin "roi_"${name}
   let thresh+=1
   let index+=1
done

}

#==============================================================================
: <<COMMENTBLOCK

Function:   GetAtlasVolsRL
Purpose:    Create a set of volumes from the atlas
Input:      Atlas file and xml file names
Output:     Binary masks named vol* (include l and r in one image)
			      Binary masks named roi* (left version and right version)

COMMENTBLOCK

function GetAtlasVolsRL
{

 atlas=$1
 xml=$2

# Get max and clean it up so it is an integer for bash
max=`fslstats ${atlas} -R | awk '{printf $2 "\n"}'`
# Identify the extension .*
ext=`echo ${max} | cut -d'.' -f2`
# Chop the extension off the end of the number
max=`basename -s .${ext} ${max}`

 # Atlas indices start at 0, but the values in the masks
 # start at 1
 index=0
 thresh=1
 # Iterate through atlas indices creating individual named volumes
 while [  ${thresh} -le ${max}  ] && [  ${index} -lt ${max}  ]; do
   RenameAtlasVols ${xml}
   fslmaths ${atlas} -thr ${thresh} -uthr ${thresh} -bin "vol_"${name}
   DivideRLvols
   # imrm "vol_"${name}
   let thresh+=1
   let index+=1
done

}

#==============================================================================
: <<COMMENTBLOCK

Function:   MkCb
Purpose:    Export a set of binary masks from the Cerebellum atlas
Input:      Atlas must exist
Output:     A directory Cb containing binary masks of the Cerebellum atlas
		        regions based on the Fnirt atlas

COMMENTBLOCK

function MkCb
{

Cb=${ATLASES}/Cerebellum/Cerebellum-MNIflirt-maxprob-thr25-2mm
#Cb=${ATLASES}/Cerebellum/Cerebellum-MNIfnirt-maxprob-thr50-2mm
CbXML=${ATLASES}/Cerebellum_MNIflirt.xml

 mkdir Cb
 cd Cb
 GetAtlasVolsRL ${Cb} ${CbXML}
  rm vol*

 # Remove images that contain no voxels, (the Cb atlas has
 # some images that are already tagged right and left and some that are not).
for roi  in roi*.nii.gz; do
  vol=`fslstats ${roi} -V | awk '{print $1}'`
  if [ ${vol} -lt 1 ]; then
      rm ${roi}
  fi
done

# Rename files so we don't include redundant Right and Left labels.
for roi  in roi*.nii.gz; do
  name=`echo ${roi} | sed 's/_Left//g' | sed 's/_Right//g'`
  mv ${roi} ${name}
done

}

 #==============================================================================
: <<COMMENTBLOCK

Function:   MkCort
Purpose:    Create left and right binary mask for each
			      region defined in the HarvardOxford-Cortical atlas
Input:      None, assumes FSL dir exists
Output:     Cort dir containing cortical rois

COMMENTBLOCK

function MkCort
{

cort=${ATLASES}/HarvardOxford/HarvardOxford-cort-maxprob-thr25-2mm
#cort=${ATLASES}/HarvardOxford/HarvardOxford-cort-maxprob-thr50-2mm

cortXML=${ATLASES}/HarvardOxford-Cortical.xml

mkdir Cort
cd Cort
GetAtlasVolsRL ${cort} ${cortXML}
rm vol*.gz
cd ..

}

#==============================================================================
: <<COMMENTBLOCK

Function:   MkMNI
Purpose:    Create left and right binary mask for each
      			region defined in the MNI atlas
Input:      None, assumes FSL dir exists
Output:     MNI dir containing cortical rois

COMMENTBLOCK

function MkMNI
{

mni=${ATLASES}/MNI/MNI-maxprob-thr25-2mm
mniXML=${ATLASES}/MNI.xml

mkdir MNI
cd MNI
GetAtlasVolsRL ${mni} ${mniXML}
rm vol*.gz
cd ..

}

#==============================================================================
: <<COMMENTBLOCK

Function:   MkSubcort
Purpose:    Create left and right binary masks for each region defined in
			      HarvardOxford-Subcortical atlas
Input:      None
Output:     Subcort directory containing appropriately named subcortical rois

COMMENTBLOCK

function MkSubcort
{

subcort=${ATLASES}/HarvardOxford/HarvardOxford-sub-maxprob-thr25-2mm
#subcort=${ATLASES}/HarvardOxford/HarvardOxford-sub-maxprob-thr50-2mm
subcortXML=${ATLASES}/HarvardOxford-Subcortical.xml

mkdir Subcort
cd Subcort
GetAtlasVols ${subcort} ${subcortXML}
cd ..

}

#==============================================================================
: <<COMMENTBLOCK

Function:   RenameAtlasVols
Purpose:    Rename regions to short standard names.  Currently handles
      			HarvardOxford Cortical and Subcortical atlases
Input:      This functions gets called by getAtlasVolsRL or getAtlasVols
Output:     As each name is encountered in the calling function, renaming
            is applied as appropriate.

COMMENTBLOCK

function RenameAtlasVols
{

 touch ROInames.txt
 fullname=`cat ${xml} | grep "index=\"${index}\"" | awk -F\> '{ print $2 }' | awk -F\< '{ print $1 }'| sed 's/,/ /g' | sed 's/ /_/g' | sed 's/__/_/g' `

 name=`cat ${xml} | grep "index=\"${index}\"" | awk -F\> '{ print $2 }' | awk -F\< '{ print $1 }'| sed 's/,/ /g' | \
 awk -F\( '{ print $1 }' | sed 's/ /_/g' | sed 's/__/_/g' | sed 's/[_ ]$//g' | sed "s/\'//g" | sed 's/__ /_/g' | sed 's/-/_/g' | \
 sed 's/anterior_division/ant/g' | sed 's/posterior_division/post/g' | \
 sed 's/superior_division/sup/g' | sed 's/inferior_division/inf/g' | \
 sed 's/Lateral/Lat/g' | sed 's/Medial/Med/g' | sed 's/Central/Cen/g' | \
 sed 's/Superior_Frontal_Gyrus/SFG/g' | sed 's/Middle_Frontal_Gyrus/MFG/g' | sed 's/Inferior_Frontal_Gyrus/IFG/g' | \
 sed 's/Superior_Temporal_Gyrus/STG/g' | sed 's/Middle_Temporal_Gyrus/MTG/g' | sed 's/Inferior_Temporal_Gyrus/ITG/g' | \
 sed 's/Angular_Gyrus/AnG/g' | sed 's/Supramarginal_Gyrus/SMG/g' | \
 sed 's/Parahippocampal_Gyrus/PhG/g' | sed 's/Insular_Cortex/Ins/g' | \
 sed 's/Juxtapositional_Lobule/SMA/g' | sed 's/Frontal_Pole/FRP/g' | \
 sed 's/IFG_pars_triangularis/BA45/g' | sed 's/IFG_pars_opercularis/BA44/g' | \
 sed 's/Precentral_Gyrus/PrG/g' | sed 's/Temporal_Pole/tmp/g' | \
 sed 's/temporooccipital_part/tempocc/g' | sed 's/Postcentral_Gyrus/PoG/g' | \
 sed 's/Superior_Parietal_Lobule/SPL/g' | sed 's/Occipital_Cortex/OL/g' | \
 sed 's/Subcallosal_Cortex/SCA/g' | sed 's/Cingulate_Gyrus/CgG/g' | \
 sed 's/Precuneous_Cortex/PCu/g' | sed 's/Cuneal_Cortex/Cun/g' | \
 sed 's/Frontal_Orbital_Cortex/OFC/g' | sed 's/Fusiform_Cortex/FuG/g' | sed 's/Fusiform_Gyrus/FuG/g' | \
 sed 's/Operculum_Cortex/Op/g' | sed 's/Opercular_Cortex/Op/g' | \
 sed 's/Planum_Polare/PP/g' | sed 's/Planum_Temporale/PT/g' | sed 's/Heschls_Gyrus/HG/g' |\
 sed 's/Supracalcarine_Cortex/SCLC/g' | sed 's/Occipital_Pole/OCP/g' | sed 's/Occipital/OL/g' | \
 sed 's/Lingual_Gyrus/LiG/g' | sed 's/Ventrical/Ventricle/g' | \
 sed 's/Paracingulate_Gyrus/PAC/g' | sed 's/Intracalcarine_Cortex/CALC/g' | \
 sed 's/Temporal/TL/g' | sed 's/Frontal/FL/g' | sed 's/Parietal/PL/g' | \
 sed 's/Cortex/GM/g' | sed 's/White_Matter/WM/g' | sed 's/SMA_GM/SMA/g' | \
 sed 's/Left_Thalamus/Th_L/g' | sed 's/Right_Thalamus/Th_R/g' | \
 sed 's/Left_Caudate/Cd_L/g' | sed 's/Right_Caudate/Cd_R/g' | \
 sed 's/Left_Putamen/Pu_L/g' | sed 's/Right_Putamen/Pu_R/g' | \
 sed 's/Left_Pallidum/GP_L/g' | sed 's/Right_Pallidum/GP_R/g' | \
 sed 's/Left_Amygdala/Amg_L/g' | sed 's/Right_Amygdala/Amg_R/g' | \
 sed 's/Left_Hippocampus/Hi_L/g' | sed 's/Right_Hippocampus/Hi_R/g' | \
 sed 's/Left_Accumbens/Acb_L/g' | sed 's/Right_Accumbens/Acb_R/g' `

 echo "index: ${index}, thresh: ${thresh}, fullname: ${fullname}, name: ${name}"
 echo "${thresh}, ${name}, ${fullname}" >>ROInames.txt

}

 #==============================================================================
: <<COMMENTBLOCK

   Function:   HelpMessage
   Purpose:    function_purpose
   Input:      input_files
   Output:     output

COMMENTBLOCK

 function HelpMessage
 {

echo "Usage: $0 "
echo "this extracts and binarizes all individual rois in an atlas"
echo "Run with no arguments to create cortical, subcortical and cerebellar atlases"
echo "$0 mkCort  will generate just the cortical atlas"
echo "$0 mkSubcort  will generate just the subcortical atlas"
echo "$0 mkCb  will generate just the cerebellar atlas"
echo "other atlases are not currently implemented"
echo ""

 }

#==============================================================================
: <<COMMENTBLOCK

Function:   Main
Purpose:   Create binary masks for HarvardOxford Cortical and Subcortical atlases
		       Also create binary masks for Fnirt Cerebellum atlas
Input:     None
Output:    Three directories, Cort, Subcort and Cb containing binary masks from each atlas

COMMENTBLOCK

function Main
{
GetAtlasVols
# Cortical
#MkCort

# Subcortical
# MkSubcort

 # Cerebellum
 #MkCb

# Lobes
# MkMNI

}

#########################  END FUNCTION DEFINITIONS  ##########################
###############################################################################
source plante2_profile.sh
# Load shared functions
. prep_functions.sh

while getopts h options 2> /dev/null; do        # Setup -h flag. redirect errors
    case $options in                            # In case someone invokes -h
    h) HelpMessage;;                            # Run the help message function
    \?) echo "only h is a valid flag" 1>&2      # If bad options are passed, print message
    esac
done

shift $(( $OPTIND -1))                          # Index option count, so h isn't treated as arg
optnum=$(( $OPTIND -1))                        	# optnums hold # of options passed in

if [ ${optnum} -lt 1 -a $# -lt 1 ]              # If no options or args
    then
        dothis=Main                             # Default to running Main
    else
        dothis=$1                               # Run mkCb, mkCort, or mkSubcort separaely
fi

$dothis
