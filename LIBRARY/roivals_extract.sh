#!/bin/bash

: <<COMMENTBLOCK
Extract roi values from a standard 2mm MNI image
COMMENTBLOCK

if [ $# -lt 2 ]; then                           # if less than one arg
  echo "Purpose: Make a list of regions with activation"
  echo "   		   at a given statistical-threshold"
  echo "Input:   1) Standard space stats image"
  echo "         2) A stat_threshold should be passed in to limit the output"
  echo "            Keep in mind that values could be negative"
  echo "         e.g., roivals_extract.sh MNI_2mm_stats 1.216"
  echo "Output:  Mean & max vals for each region that meets the criterion: ROIValues.csv"
  echo "This is the non-dockerized version."
  echo "See also https://bitbucket.org/dpat/roixtractor/src/master/"
  exit 1
fi

img=`remove_ext $1`
stat_thresh=$2

ROI_DIR=/usr/local/tools/REF/IMAGES/WEAVE_ROIS
Atlas_List=${ROI_DIR}/HO_CB_list.csv
OUTIMG=out

if [ -f ROIValues.csv ]; then
  "ROIValues.csv exists, removing and replacing"
  rm ROIValues.csv
fi

if [ -f HO_CB_${img}.csv ]; then
  "HO_CB_${img}.csv exists, removing and replacing"
  rm HO_CB_${img}.csv
fi

for roi in ${ROI_DIR}/roi*.nii.gz; do
  # Extract a reasonable name for the roi
  roi_base=$(basename -s .nii.gz ${roi} | sed 's/roi_//g')
  # Calculate the mean value in each region from Atlas/roi*
  # Redirect standard error to /dev/null to prevent complaints
  # about an empty image when fslstats finds a mean of 0
  MeanStat=$(fslstats ${img} -k ${roi} -M 2>/dev/null)
  MaxStat=$(fslstats ${img} -k ${roi} -R | awk '{print $2}')
  # MeanStat is probably a float, so use bc to determine whether
  # it is above or below the threshold. This particular
  # use of bc returns a boolean: 1=yes, 0=no
  good=$(echo "${MeanStat}  >= ${stat_thresh}" | bc)
  # If the roi meets the criterion threshold, add it to ROIValues.csv
  if [[ ${good}  -eq 1  ]]; then
    echo "${roi_base},${MeanStat},${MaxStat},${img}" >> ROIValues.csv
    echo "${roi_base} is good with mean value: ${MeanStat}"
    if [ ! -e  ${OUTIMG}.nii.gz ]; then
   		imcp ${roi} ${OUTIMG}
   		# subtract the outimage from itself to get an empty image.
   		fslmaths ${OUTIMG} -sub ${OUTIMG} ${OUTIMG} -odt short
  	fi
    # Create an roi with the correct internal value
    fslmaths ${roi} -mul ${MeanStat} ${PWD}/${roi_base}_val
    # Add these rois with values to the outimg
    fslmaths ${OUTIMG} -add ${roi_base}_val ${OUTIMG}
		echo "adding ${roi_base} to ${OUTIMG}"
    imrm ${roi_base}_val
  fi
done

echo regionName_Unique,Image,Mean,Max,LR,Lobe > HO_CB_${img}.csv
join -t, ROIValues.csv ${Atlas_List} | sed 's/ //g' >> HO_CB_${img}.csv
