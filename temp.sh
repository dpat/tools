#!/bin/bash

#declare -a list_array=("sub-075" "sub-078" "sub-079")

declare -a list_array=("sub-075" "sub-078" "sub-079" "sub-083" "sub-084" "sub-086" "sub-089" "sub-094" "sub-104" "sub-105" "sub-106" "sub-109")

# Extract the B0 images from the PA files
# for subj in ${list_array[@]}; do
#   echo ${subj}
#   cd ${subj}/fmap
#   img=${subj}_dir-PA_epi.nii.gz
#   fslroi ${img} B0_1 0 1
#   dim4=`fslinfo ${img} | grep -m 1 dim4 | awk '{print $2}'`
#   count=`echo "${dim4} -1" | bc`
#   fslroi ${img} B0_2 ${count} 1
#   mv ${img} orig_${img}
#   fslmerge -t ${img} B0_1 B0_2
#   rm B0*
#   cd ..
# done

# Check the files
for subj in ${list_array[@]}; do
  echo ${subj}
  cd ${subj}/fmap
  img=${subj}_dir-PA_epi.nii.gz
rm orig_${subj}_dir-PA_epi.nii.gz
  cd ..
done
