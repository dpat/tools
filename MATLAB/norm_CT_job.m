%-----------------------------------------------------------------------
% Job saved on 04-Mar-2019 14:55:15 by cfg_util (rev $Rev: 6942 $)
% spm SPM - SPM12 (7219)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.tools.MRI.CTnorm.images = {'/Volumes/Main/Working/Beeson_Data_Review/patients47/sub-0316/anat_CT_axial.nii,1'};
matlabbatch{1}.spm.tools.MRI.CTnorm.ctles = {'/Volumes/Main/Working/Beeson_Data_Review/patients47/sub-0316/lesion_CT.nii,1'};
matlabbatch{1}.spm.tools.MRI.CTnorm.brainmaskct = 1;
matlabbatch{1}.spm.tools.MRI.CTnorm.bb = [-90 -126 -72
                                          90 90 108];
matlabbatch{1}.spm.tools.MRI.CTnorm.vox = [1 1 1];
matlabbatch{1}.spm.tools.MRI.CTnorm.DelIntermediate = 0;
matlabbatch{1}.spm.tools.MRI.CTnorm.AutoSetOrigin = 1;
