#!/bin/bash

: <<COMMENTBLOCK
This code opens fsleyes with the designated images.  Once you nudge and save the image, it applies flirt to commit the change to the image. 03/28/2019.  Someday this extra step (applying flirt) won't be necessary, the nudge and save should be enough.
Many thanks to Mark Jenkinson who explained how to use the sqform.
COMMENTBLOCK

# Exit if number of arguments is too small
if [ $# -lt 2 ]
    then
        echo "======================================================"
        echo "Two arguments are required. An optional 3rd argument can be added if there is an additional mask to view"
        echo "argument 1: name of NIFTI image to overlay masks on (so, an anatomical image)"
        echo "argument 2: name of mask file that will be nudged and rewritten."
        echo "This should be a binary mask."
        echo "optional argument 3: an additional mask image, if desired for viewing."
        echo "This should also be a binary mask."
        echo "e.g., $0 anat_T1w LV_L"
        echo "e.g., $0 anat_T1w LV_L lesion"
        echo "To nudge the mask, select it in the Overlay list."
        echo "The mask should be at the top of the list. Then choose Tools->Nudge."
        echo "apply, close.  From the overlay list, select the floppy disk icon next to your"
        echo "mask, and choose overwrite."
        echo "We'll save a *_back image."
        echo "======================================================"
        exit 1
fi

# get the input stem
anat=`remove_ext ${1}`
mask=`remove_ext ${2}`
mask2=${3}

if [ ! -e ${mask}_back.nii.gz ]; then
  echo "creating ${mask}_back.nii.gz"
  imcp ${mask} ${mask}_back
fi

# If there are 2 arguments
if [ $# -eq 2 ]; then
  echo "You should nudge the Red mask (top of the list)."
  fsleyes ${anat} ${mask} -cm Red
  echo "Applying your transformations to the mask, be patient"
  flirt -in ${mask} -ref ${anat} -applyxfm -usesqform -interp nearestneighbour -out ${mask}
# else if there are more than 2 arguments, the third argument is a mask displayed in Blue
elif [ $# -gt 2 ]; then
  echo "You should nudge the Red mask (top of the list)."
  echo "The Blue mask is just for your viewing pleasure"
  fsleyes ${anat} ${mask2} -cm Blue ${mask} -cm Red
  echo "Applying your transformations, be patient"
  flirt -in ${mask} -ref ${anat} -applyxfm -usesqform -interp nearestneighbour -out ${mask}
# For anything else, spit out the help message and stop.
else
  echo "======================================================"
  echo "Two arguments are required. An optional 3rd argument can be added if there is an additional mask to view"
  echo "argument 1: name of NIFTI image to overlay masks on (so, an anatomical image)"
  echo "argument 2: name of mask file that will be nudged and rewritten."
  echo "This should be a binary mask."
  echo "optional argument 3: an additional mask image, if desired for viewing."
  echo "This should also be a binary mask."
  echo "e.g., $0 anat_T1w LV_L"
  echo "e.g., $0 anat_T1w LV_L lesion"
  echo "To nudge the mask, select it in the Overlay list. Then choose Tools->Nudge."
  echo "apply, close.  From the overlay list, select the floppy disk icon next to your"
  echo "mask, and choose overwrite."
  echo "When you save your nudged file, choose to overwrite (we'll save a *_back image)"
  echo "======================================================"
  exit 1
fi

fslmaths ${mask} ${mask} -odt short
