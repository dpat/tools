Additional FSL atlases

To use these atlases, download the zip file by clicking on the atlas title.  Place the unzipped folder and xml file in the fsl atlases directory (e.g., /usr/local/fsl/data/atlases). The next time you start fsleyes, the atlas should be available in the atlas panel.
==============================================================
BIP https://osf.io/cbk3u/

The BIP language atlas  is based on my own iterative parcellation work. The regions and tracts are described here:
https://bitbucket.org/dpat/bipbids/src/master/REF/IMAGES/Readme_tracts.txt

This is the accompanying paper:
Patterson, D. K., Van Petten, C., Beeson, P., Rapcsak, S. Z., & Plante, E. (2014). Bidirectional iterative parcellation of diffusion weighted imaging data: separating cortical regions connected by the arcuate fasciculus and extreme capsule. NeuroImage, 102 Pt 2, 704–716. http://doi.org/10.1016/j.neuroimage.2014.08.032

The software for running BIP is available as a containerized BIDS app.  See the description here: https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/bids_containers.html#bip
and the bitbucket site here: https://bitbucket.org/dpat/bipbids/src/master/
================================================================
HCP-MMP1 and HCP-MMP1_cortices https://osf.io/azup8/

The HCP-MMP1 atlas is based on the asymmetrical version (MMP_in_MNI_corr.nii.gz) of the MNI projection of the HCP-MMP1 available here: https://figshare.com/articles/HCP-MMP1_0_projected_on_MNI2009a_GM_volumetric_in_NIfTI_format/3501911. Also see: https://neurovault.org/collections/1549/ as another source of the image).

Glasser divides 180 regions in each hemisphere into 22 separate cortices (He calls them "areas" and "regions" respectively).

The HCP-MMP1 atlas has 180 regions in each hemisphere.  On the left, these are numbered 1-180.  On the right they are numbered 201-380 so that 201 is the right homologue of 1; 262 is the right homologue of 62, etc.)  Note that MRtrix3 renumbers the values on the right to go from 181 to 360 to avoid the discontinuity (i.e., unused values between 181 and 199).

Each of the 180 regions occupies one of 22 cortices which are displayed in a separate atlas: HCP-MMP1_cortices. These are numbered 1-22 on the left and 101-122 on the right, in keeping with the original strategy.

Detailed information about the regions and cortices are described in the Glasser 2016 Supplemental file: "Supplementary Neuroanatomical Results For A Multi-modal Parcellation of Human Cerebral Cortex".  I have made the the following helpful files available:
1) The final table of 180 regions from that supplemental file available as an Excel sheet: Glasser_2016_Table.xlsx.  https://bitbucket.org/dpat/tools/src/master/REF/ATLASES/Glasser_2016_Table.xlsx
2) HCP-MMP1_UniqueRegionList.csv

https://bitbucket.org/dpat/tools/raw/master/REF/ATLASES/HCP-MMP1_UniqueRegionList.csv
providing information from the final table in addition to a center of gravity in voxel coordinates and a volume in cubic mm for each of the 360 regions (180 in each hemisphere).
3) A text file naming the 22 cortices and how they are grouped as per descriptions in the supplemental material.  https://bitbucket.org/dpat/tools/raw/master/REF/ATLASES/HCP-MMP1_cortices.txt
4) HCP-MMP1_cortex_UniqueRegionList.csv providing the center of gravity in voxel coordinates and the volume in cubic mm for each of the 44 cortices (22 in each hemisphere).
https://bitbucket.org/dpat/tools/raw/master/REF/ATLASES/HCP-MMP1_cortex_UniqueRegionList.csv

This is the accompanying paper:
Glasser, M. F., Coalson, T. S., Robinson, E. C., Hacker, C. D., Harwell, J., Yacoub, E., et al. (2016). A multi-modal parcellation of human cerebral cortex. Nature, 1–11. http://doi.org/10.1038/nature18933
================================================================
GreciusFunc https://osf.io/x76r5/

This is a 2mm probabalistic atlas with 13 functional regions defined. It includes both cortex and cerebellum.

The data for each roi were downloaded from http://findlab.stanford.edu/functional_ROIs.html

This is the accompanying paper:

Shirer, W. R., Ryali, S., Rykhlevskaia, E., Menon, V., & Greicius, M. D. (2012). Decoding Subject-Driven Cognitive States with Whole-Brain Connectivity Patterns. Cerebral Cortex. http://doi.org/10.1093/cercor/bhr099
=================================================================
YeoBuckner7 and YeoBuckner17 https://osf.io/5ayw4/

These are 2mm probabalistic atlases with 7 and 17 functional regions defined respectively. These atlases were converted from Freesurfer space to FSL standard space with the cortex and cerebellum combined.
There are two atlases:
1) YeoBuckner7: The 7 functional networks
2) YeoBuckner17: Finer divisions of the 7 networks into a total of 17 networks
(which are not always proper subsets of the first 7)

These are the accompanying papers:
Yeo, B. T. T., Krienen, F. M., Sepulcre, J., Sabuncu, M. R., Lashkari, D., Hollinshead, M., et al. (2011). The organization of the human cerebral cortex estimated by intrinsic functional connectivity. Journal of Neurophysiology, 106(3), 1125–1165. http://doi.org/10.1152/jn.00338.2011

Buckner, R. L., Krienen, F. M., Castellanos, A., Diaz, J. C., & Yeo, B. T. T. (2011). The organization of the human cerebellum estimated by intrinsic functional connectivity. Journal of Neurophysiology, 106(5), 2322–2345. http://doi.org/10.1152/jn.00339.2011
