#!/usr/bin/env python3
import sys
import argparse
import os
import subprocess
from glob import glob

def run(command, env={}):
    merged_env = os.environ
    merged_env.update(env)
    process = subprocess.Popen(command, stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT, shell=True,
                               env=merged_env)
    while True:
        line = process.stdout.readline()
        line = str(line, 'utf-8')[:-1]
        print(line)
        if line == '' and process.poll() != None:
            break
    if process.returncode != 0:
        raise Exception("Non zero return code: %d"%process.returncode)

parser = argparse.ArgumentParser(description='BIDS App entrypoint script.')
parser.add_argument('bids_dir', help='The directory with the input dataset '
                    'formatted according to the BIDS standard.')
parser.add_argument('output_dir', help='The directory where the output files '
                    'should be stored. If you are running group level analysis '
                    'this folder should be prepopulated with the results of the'
                    'participant level analysis.')
parser.add_argument('--participant_label', help='The label(s) of the participant(s)'
                    'that should be analyzed. The label '
                   'corresponds to sub-<participant_label> from the BIDS spec '
                   '(so it does not include "sub-"). If this parameter is not '
                   'provided all subjects should be analyzed. Multiple '
                   'participants can be specified with a space separated list.',
                   nargs="+")
parser.add_argument('--session_label', help='The label of the session that should'
                    'be analyzed. The label '
                    'corresponds to ses-<session_label> from the BIDS spec '
                    '(so it does not include "ses-"). If this parameter is not '
                    'provided all sessions should be analyzed. Multiple '
                    'sessions can be specified with a space separated list.',
                    nargs="+")
parser.add_argument('--cmd', help='The bash command to be run')
args = parser.parse_args()

subjects_to_analyze = []
# only for a subset of subjects
if args.participant_label:
    subjects_to_analyze = args.participant_label
# for all subjects
else:
    subject_dirs = glob(os.path.join(args.bids_dir, "sub-*"))
    subjects_to_analyze = [subject_dir.split("-")[-1] for subject_dir in subject_dirs]

# Here I want to pass in a command from the commandline
# But how do I get flexibility?
for subject_label in subjects_to_analyze:
    cmd = "echo %s %s %s %s "%(subject_label,args.bids_dir,args.output_dir,args.cmd)
    cmd1 = "echo %s; ls sub-%s"%(subject_label,subject_label)
    cmd2 = "ls %s"%(args.bids_dir)
    cmd3 = "ls %s"%(args.output_dir)
    #cmd4 = "%s/sub-%s" %(args.bids_dir,subject_label)
    #cmd5 = "%s" %(args.cmd)
    print(cmd)
    run(cmd)
    print(cmd1)
    run(cmd1)
    print(cmd3)
    run(cmd3)
