
% Assumes a 1 0 vector called imgVec.
% Purpuse: take in a vector of 1 0 values (from an image mask).
% Create an adjacency matrix from the vector.  This assumes
% that any cell with a 1 is correlated to any other cell with a 1.
% This is appropriate for importing the network defined by an IC.
% See the script ICnet_prep.sh which does the initial processing for this.

len=length(imgVec); % get vector length
adjMat=zeros(len, len); % create matrix of 0's

% Fill in the off-diagonal cells of the adjacency matrix
% Because i and j are different, we'll never put anything in the diagonal.
for i= 1:len-1
    for j=i+1:len
        adjMat(i,j)=imgVec(i) .* imgVec(j);
    end    
i=i+1;
end

% To fill in just the upper triangle with the symmetrical matrix
adjMat=adjMat+adjMat';

clear ans i j len;

