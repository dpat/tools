#!/bin/bash

# Author: Dianne Patterson, Ph.D.

if [ $# -lt 1 ]
then
    echo "Usage: $0 <input file>"
    echo "Example: $0 combo_mask"
    echo "This reports clusters and their sizes"
    echo "An optional 2nd argument defaults to 26 and represents the number of neighbours"
    echo "to include in clustering.  Other options are 6 or 18 (6 is the harshest)"
    echo "e.g., Example: $0 combo_mask 6"
    echo "This command provides information useful for running cluster_clean.sh"
    echo ""
    exit 1
fi

if [ $# -eq 1 ]; then
  number=26
elif [ $# -eq 2 ]; then
  number=$2
fi

input=$1

echo "Clusters with a connectivity of ${number}"
fsl-cluster -i $input -t 1 --connectivity=${number}
echo ------------------------------------------------------------------------------
echo
