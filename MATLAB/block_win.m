function block_win(num_sess, hrf_vec, win_width, slide_incr)
% This function is similar to task_load.m, in that it creates a windowed
% result (e.g., 167 timepoints per session instead of 176 timepoints per 
% session for russian).  The function writes "block_windowed.mat" which
% is the psuedo block labels for a vector representing all the concatenated
% sessions.  Thus, for example, for the Russian, the 8 blocks in session 1 
% are labeled 1-8, whereas the 8 blocks in session 2 are labeled 16-24.
% Blocks are contiguous with no gaps. Each block in the windowed result is
% the same size as in the original experiment, and the same as the other
% blocks EXCEPT that the number of windows is smaller than the number of
% timepoints, and so the end is truncated.  This forces the blocks to align
% with the hi-lo function for the task.  There are alternative way to
% calculate the blocks, look in the Deprecated folder under Misc.

% example: block_win(4, hrf_vec, 9,1)
% for 4 sessions, the model hrf of the task from SPM.mat, a window size (9)
% and slider increment (1).

% You must first load the hrf_vec into the workspace

len_vec=length(hrf_vec);
[pks, locs]=findpeaks(hrf_vec);
% get the number of blocks in the session
num_blocks=length(pks);
% get the best block length
block_size=mode(diff(locs));
% create a vector of zeros to hold the block values
block_vec=zeros(length(hrf_vec), 1);
% Fill block_vec from the beginning with block numbers
    for block=1:num_blocks;  
       block_end=block_size*block;
       block_start=(block_end-block_size)+1;
       %fill block_vec with appropriate values up to the 
       % end of the correct vector size 
       block_vec(block_start:len_vec)=block;
    end

% Fill in the appropriate block numbers for consecutive sessions 
% and create the concatenated output all_blocks
    for sess=1:num_sess;
      if ~exist ('all_blocks', 'var')
          all_blocks=block_vec; % this is what will happen for session 1
          % if t exists, concatenate block_vec onto t
          elseif exist ('all_blocks', 'var')
           % multiplier is 1 less than session number   
           mul=sess-1;
           % create a fresh block_vec to multiply and concatenate
           block_vec2=block_vec+(num_blocks*mul);
          all_blocks=[all_blocks;block_vec2];
      end
    end
clear sess;

% Calculate the correct number of windows for each session.
% Then fill in the window vector with the correct values from all_block
% until we truncate the values at the end of the vector (because we run out
% of windows)
 for sess=1:num_sess    
     finish_sess=len_vec*sess;
     start_sess=((finish_sess-len_vec)+1);
     block_sess=all_blocks(start_sess:finish_sess);
     numstps = ((length(block_sess)-win_width)/slide_incr);
     for i = 1:numstps
        % fill in each block value from the original vector until we cut
        % off the end
        t(i)=block_sess(i); 
     end  
     
     % concatenate the sessions into one big vector
      if ~exist ('block_windowed', 'var')
         block_windowed=[t];
         elseif exist ('block_windowed', 'var')
         block_windowed=[block_windowed, t]; 
      end
 end
% Transpose the output
 block_windowed=block_windowed';

 
% save just the variable I need
save('block_windowed.mat', 'block_windowed')
