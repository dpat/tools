#!/bin/bash

: <<COMMENTBLOCK
get the sform and qform from the image header and diff them.
sform is listed first. See sqdiff_wrap.sh as well.
COMMENTBLOCK

# If there is an argument
if [ $# -eq 1 ]; then
img=${1}
sform=$(fslorient -getsform ${img})
qform=$(fslorient -getqform ${img})

echo "diff results for $(basename ${PWD}) ${img}:"
diff <(echo "$sform") <(echo "$qform")
echo "======================="

else # if there are no arguments
  echo "Run sqdiff.sh on all NIFTI images in this directory?"
  echo "yes to continue"
  echo "hit any other key to abort and view help."
  read answer
  if [ "$answer" = "yes" ]; then
    echo "applying sqdiff to all image files in the directory"
    for img in *.nii *.nii.gz; do
      if [ -e  ${img} ]; then
        sform=$(fslorient -getsform ${img})
        qform=$(fslorient -getqform ${img})

        echo "diff results for $(basename ${PWD}) ${img}:"
        diff <(echo "$sform") <(echo "$qform")
        echo "======================="
      fi
    done
  else
    echo "HELP on sqdiff.sh"
    echo "===================="
    echo "With one image file as the argument,"
    echo "e.g., sqdiff.sh mask"
    echo "sqdiff.sh identifies any differences between the qform to the sform for that image."
    echo "--------------------"
    echo "With no arguments, sqdiff.sh asks if you want to review all the images in the directory."
    echo "IF you answer yes,"
    echo "then sqdiff.sh runs on all NIFTI images in the current directory."
    echo "Otherwise, hit any key to see this help message."
    echo ""
  fi
fi
