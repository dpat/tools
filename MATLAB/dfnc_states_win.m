function dfnc_states_win(postprocess_file)
% example: 
% dfnc_states_win('LearnSlo2ERBM_dfnc_post_process.mat')
% state data in contained in the DFNC subdir in *_dfnc_post_process.mat
% This function extracts the state for every window into a single table
% with scans as columns.

load(postprocess_file);

% permute the dimensions of the states into a more approachable format.
% (a column of timepoints for each subject and each session.)
states=permute(clusterInfo.states,[3,2,1]);
numsess=size(states, 2); % dimension 2 is the number of sessions
numsub=size(states,3);   % dimension 3 is the number of subjects
numwins=size(states,1);  % dimension 1 is the number of windowed timepoints

    for sub=1:numsub
        for count=1:numwins
             scan=(states(count,:,sub));
             t1=table(sub, count, scan);
             if ~exist ('t', 'var')
                 t=t1;
                 % if t exists, then put the new data in another table and
                 % concatenate it onto t
                 elseif exist ('t', 'var')
                 t=[t;t1];
             end  
        end 
    end  
    
% write the concatenated table for subjects and sessions.
writetable(t,'state_stats_win.csv');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
