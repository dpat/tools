#!/bin/bash

###############################################################################
#########################  DEFINE FUNCTIONS  ##################################
: <<COMMENTBLOCK

Function:   GetSize
Purpose:    Get the percent size of lesion compared to brain for
						native space and various algorithms

COMMENTBLOCK

GetSize ()
{

lm_mni=${sub}_lesion
bm_mni=${sub}_brain_mask
echo "lesion mask=${lm_mni}"
echo "brain mask=${bm_mni}"

# Get size in mm^2 of each mask of interest
nat_les=`fslstats lesionmask -V | awk '{printf $2}'`
nat_bm_ob=`fslstats ${sub}_brain_mask -V | awk '{printf $2}'`
# This brainmask is the fsl_anat_alt.sh result:
# and should be used for evaluating the fsl and ant3 output
nat_bm=`fslstats fsl/${sub}.anat/T1_biascorr_brain_mask -V | awk '{printf $2}'`

# # Calculate percentages (lesion is x percent of optibet brain mask)
nat_per_ob=`echo "scale=6; (${nat_les}/${nat_bm_ob})*100" | bc`

# # Calculate percentages (lesion is x percent of fsl_anat_alt.sh brain mask)
nat_per=`echo "scale=6; (${nat_les}/${nat_bm})*100" | bc`

echo " nat_les_ob=${nat_les}; nat_bm_ob=${nat_bm_ob}; nat_per_ob=${nat_per_ob}, nat_per=${nat_per} "

dir_list="ant1 ant2 ant3 fsl spm_brain spm_head spm_old spm_old_nosw spm_tpm_brain spm_tpm_head"

for dir in ${dir_list}; do
echo ${dir}
bm=`fslstats ${dir}/${sub}_MNI_brain_mask -V | awk '{printf $2}'`
echo "${dir}_bm =${bm}"
les=`fslstats ${dir}/${sub}_MNI_lesion -V | awk '{printf $2}'`
echo "${dir}_les =${les}"
per=`echo "scale=6; (${les}/${bm})*100" | bc`
diff_ob=`echo "scale=6; (${nat_per_ob}-${per})" | bc`
diff=`echo "scale=6; (${nat_per}-${per})" | bc`
echo "${nat_per} - ${per} = ${diff}"

if [ ! -e ../lesion_change.csv ]; then
 	touch ../lesion_change.csv
  echo subject, method, nat_per_ob, nat_per, diff_ob, diff >> ../lesion_change.csv
fi

echo ${sub}, ${dir}, ${nat_per_ob}, ${nat_per}, ${diff_ob}, ${diff} >> ../lesion_change.csv

done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   GetSize2
Purpose:    Get the percent size of lesion compared to brain for
						native space and various algorithms (Do this in reverse of GetSize, and add more
						variables to the output)

COMMENTBLOCK

GetSize2 ()
{

lm_mni=${sub}_lesion
bm_mni=${sub}_brain_mask
echo "lesion mask=${lm_mni}"
echo "brain mask=${bm_mni}"

# Get size in mm^2 of each mask of interest
nat_les=`fslstats lesionmask -V | awk '{printf $2}'`
nat_bm_ob=`fslstats ${sub}_brain_mask -V | awk '{printf $2}'`
# This brainmask is the fsl_anat_alt.sh result:
# and should be used for evaluating the fsl and ant3 output
nat_bm=`fslstats fsl/${sub}.anat/T1_biascorr_brain_mask -V | awk '{printf $2}'`

# # Calculate percentages (lesion is x percent of optibet brain mask)
nat_per_ob=`echo "scale=6; (${nat_les}/${nat_bm_ob})*100" | bc`

# # Calculate percentages (lesion is x percent of fsl_anat_alt.sh brain mask)
nat_per=`echo "scale=6; (${nat_les}/${nat_bm})*100" | bc`

echo " nat_les_ob=${nat_les}; nat_bm_ob=${nat_bm_ob}; nat_per_ob=${nat_per_ob}, nat_per=${nat_per} "

dir_list="ant1 ant2 ant3 fsl spm_brain spm_head spm_old spm_old_nosw spm_tpm_brain spm_tpm_head"

for dir in ${dir_list}; do
echo ${dir}
bm=`fslstats ${dir}/${sub}_MNI_brain_mask -V | awk '{printf $2}'`
echo "${dir}_bm =${bm}"
les=`fslstats ${dir}/${sub}_MNI_lesion -V | awk '{printf $2}'`
echo "${dir}_les =${les}"
per=`echo "scale=6; (${les}/${bm})*100" | bc`
diff_ob=`echo "scale=6; (${per} - ${nat_per_ob})" | bc`
diff=`echo "scale=6; (${per} - ${nat_per})" | bc`
echo "${per} - ${nat_per} = ${diff}"

if [ ! -e ../lesion_change2.csv ]; then
 	touch ../lesion_change2.csv
  echo subject, method, nat_per_ob, nat_per, per, diff_ob, diff >> ../lesion_change2.csv
fi

echo ${sub}, ${dir}, ${nat_per_ob}, ${nat_per}, ${per}, ${diff_ob}, ${diff} >> ../lesion_change2.csv

done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   GetSize3
Purpose:    Get the percent size of lesion compared to brain for
						native space and various algorithms (same as GetSize2 except now we will only use the final FSL brain mask for fsl and ant3.  There are fewer columns as there are no longer explicit optibet columns)

COMMENTBLOCK

GetSize3 ()
{

lm_mni=${sub}_lesion
bm_mni=${sub}_brain_mask
echo "lesion mask=${lm_mni}"
echo "brain mask=${bm_mni}"

# Get size in mm^2 of each mask of interest
nat_les=`fslstats lesionmask -V | awk '{printf $2}'`
nat_bm_ob=`fslstats ${sub}_brain_mask -V | awk '{printf $2}'`
# This brainmask is the fsl_anat_alt.sh result:
# and should be used for evaluating the fsl and ant3 output
nat_bm=`fslstats fsl/${sub}.anat/T1_biascorr_brain_mask -V | awk '{printf $2}'`

# # Calculate percentages (lesion is x percent of optibet brain mask)
nat_per_ob=`echo "scale=6; (${nat_les}/${nat_bm_ob})*100" | bc`

# # Calculate percentages (lesion is x percent of fsl_anat_alt.sh brain mask)
nat_per=`echo "scale=6; (${nat_les}/${nat_bm})*100" | bc`

echo " nat_les_ob=${nat_les}; nat_bm_ob=${nat_bm_ob}; nat_per_ob=${nat_per_ob}, nat_per=${nat_per} "

dir_list1="ant1 ant2 spm_brain spm_head spm_old spm_old_nosw spm_tpm_brain spm_tpm_head"
dir_list2="ant3 fsl"

for dir in ${dir_list1}; do
echo ${dir}
bm=`fslstats ${dir}/${sub}_MNI_brain_mask -V | awk '{printf $2}'`
echo "${dir}_bm =${bm}"
les=`fslstats ${dir}/${sub}_MNI_lesion -V | awk '{printf $2}'`
echo "${dir}_les =${les}"
per=`echo "scale=6; (${les}/${bm})*100" | bc`
diff_ob=`echo "scale=6; (${per} - ${nat_per_ob})" | bc`
#diff=`echo "scale=6; (${per} - ${nat_per})" | bc`
echo "${per} - ${nat_per} = ${diff}"

if [ ! -e ../lesion_change3.csv ]; then
 	touch ../lesion_change3.csv
  echo subject,method,nat_per,per,diff >> ../lesion_change3.csv
fi

echo ${sub},${dir},${nat_per_ob},${per},${diff_ob} >> ../lesion_change3.csv

done


for dir in ${dir_list2}; do
echo ${dir}
bm=`fslstats ${dir}/${sub}_MNI_brain_mask -V | awk '{printf $2}'`
echo "${dir}_bm =${bm}"
les=`fslstats ${dir}/${sub}_MNI_lesion -V | awk '{printf $2}'`
echo "${dir}_les =${les}"
per=`echo "scale=6; (${les}/${bm})*100" | bc`
#diff_ob=`echo "scale=6; (${per} - ${nat_per_ob})" | bc`
diff=`echo "scale=6; (${per} - ${nat_per})" | bc`
echo "${per} - ${nat_per} = ${diff}"

if [ ! -e ../lesion_change3.csv ]; then
 	touch ../lesion_change3.csv
  echo subject, method, nat_per, per, diff >> ../lesion_change3.csv
fi

echo ${sub},${dir},${nat_per},${per},${diff} >> ../lesion_change3.csv

done
}

#==============================================================================


: <<COMMENTBLOCK

Function:   JaccardBrain
Purpose:    Compare the similarity of each subject*method brain mask to the MNI standard

COMMENTBLOCK

JaccardBrain ()
{
# Brain mask template
template_mask=/usr/local/fsl/data/standard/MNI152_T1_2mm_brain_mask.nii.gz

dir_list="ant1 ant2 ant3 fsl spm_brain spm_head spm_old spm_old_nosw spm_tpm_brain spm_tpm_head"

dir_list1="ant1 ant2 ant3 fsl"
dir_list2="spm_brain spm_head spm_old spm_old_nosw spm_tpm_brain spm_tpm_head"
#
for dir in ${dir_list1}; do
echo ${dir}
bm1=${dir}/${sub}_MNI_brain_mask
jaccard.sh ${template_mask} ${bm1}
done

for dir in ${dir_list2}; do
echo ${dir}
bm2=${dir}/${sub}_MNI_brain_mask_r
jaccard.sh ${template_mask} ${bm2}
done
}

#########################  END FUNCTION DEFINITIONS  ##########################
###############################################################################

# This needs to be run from DataC8 with a for loop so that it enters each subject directory
# and then runs stats. It will create and then add to a csv file in the DataC8 directory.


sub=`basename ${PWD}`
# Call GetSize
# GetSize
# Call JaccardBrain
#JaccardBrain

# Call GetSize2
#GetSize2

# Call GetSize3
GetSize3
