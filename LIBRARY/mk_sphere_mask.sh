#!/bin/bash

if [ $# -lt 5 ]
then
    echo "Usage: $0 <input file> <x_pos> <y_pos> <z_pos> <radius_mm>"
    echo "An image of the same size as the input will be created"
    echo "containing a binary spherical mask"
    echo "Example: $0 perfusion 63 63 15 10"
    echo "creates perfusion_sphere_mask_x63_y63_z15_10mm.nii.gz"
    echo "perfusion_sphere_mask_x63_y63_z15 is centered at coordinates" 
    echo "x63, y63, z15 and of radius 10 mm"
    echo "__________________________________"
    echo "See also mk_box_mask.sh"
    exit 1
fi

img=$1
x_pos=$2
y_pos=$3
z_pos=$4
radius=$5

img_stem=$(basename -s .nii.gz ${img})

# Create an image (temp_one) of the same dimensions as the input, but filled with ones.
fslmaths ${img} -mul 0 -add 1 temp_one

# Create an image (temp_point) containing a single voxel at the specified voxel coordinates
# (Voxel coordinates are whole numbers, and can be viewed in fsleyes)
fslmaths  temp_one -roi ${x_pos} 1 ${y_pos} 1 ${z_pos} 1 0 1 temp_point

# Create a binary sphere mask around the center specified by temp_point
fslmaths temp_point -kernel sphere ${radius} -dilM ${img_stem}_sphere_mask_x${x_pos}_y${y_pos}_z${z_pos}_${radius}mm

# Remove intermediate files
rm temp_one.nii.gz temp_point.nii.gz

