%% Tufte Boxplot 
% Set color and line style for up to 8 boxes in 2 sets
% nbox must exist in the workspace specifying the number of boxes (6 or 8)

% figure color and linewidth control
% set variables to hold the parts of the plot I want to control
box=findobj('Tag','Box'); %Put the tags for each of the boxes (right to left) into the variable box
medI=findobj('Tag','MedianInner'); %Put the tags for each of the internal median dots (right to left) into the variable medI
medO=findobj('Tag','MedianOuter'); % Outer circle on each median dot
out=findobj('Tag', 'Outliers'); % Put tags for each outlier set into the variable out
whiskU=findobj('Tag','Upper Whisker');
whiskL=findobj('Tag','Lower Whisker');
UAV=findobj('Tag','Upper Adjacent Value');
LAV=findobj('Tag','Lower Adjacent Value');

nboxArray=(1:nbox);
 
% colors r,b,g,k (black), m (magenta), c (cyan)
colorArray={[1 0 0 ], [0 0 1], [0.25,0.75,0.25], [0 0 0], [1 0 1], [0 1 1] };

%Make boxes invisible a la Tufte
set(box(1:nbox*2,:),'Color',[1,1,1]); % the invisible version
set(UAV(1:nbox*2,:),'Color',[1,1,1]);
set(LAV(1:nbox*2,:),'Color',[1,1,1]);

% % Linewidth and color setup colors BA44=r, BA45=b, BA47=g;

for n = nboxArray
   set(whiskU(n,:),'Color',cell2mat(colorArray(:,n)), 'Linestyle','-', 'LineWidth',2); 
   set(whiskU(n+nbox,:),'Color',cell2mat(colorArray(:,n)), 'Linestyle','-', 'LineWidth',2);
   set(whiskL(n,:),'Color',cell2mat(colorArray(:,n)), 'Linestyle','-', 'LineWidth',2); 
   set(whiskL(n+nbox,:),'Color',cell2mat(colorArray(:,n)), 'Linestyle','-', 'LineWidth',2);
   
    set(medI(n,:),'MarkerEdgeColor',cell2mat(colorArray(:,n)));
    set(medI(n+nbox,:),'MarkerEdgeColor',cell2mat(colorArray(:,n)));
    set(medO(n,:),'MarkerEdgeColor',cell2mat(colorArray(:,n)));
    set(medO(n+nbox,:),'MarkerEdgeColor',cell2mat(colorArray(:,n)));
    set(out(n,:),'MarkerEdgeColor',cell2mat(colorArray(:,n)));
    set(out(n+nbox,:),'MarkerEdgeColor',cell2mat(colorArray(:,n)));
 end
