 function [imgVec, adjMat]= toMatIC(input_image)

% INPUT FILE should be UNZIPPED 3D MASK (positive integer values only)
% Output is a vector and an adjacency matrix (reshaped from 3D)
% See ICnet_prep.sh which provides the input files.
% Dianne Patterson, Created: Sep 27, 2013
% Calls tools in the nifti_io toolbox from http://www.rotman-baycrest.on.ca/~jimmy/NIFTI/

% This assumes a 3d mask, which has had 100 added to it. 
% Finally, having squeezed the results to get
% rid of 0's we are free to subtract 100 from the remaining values to get
% our original 1 and 0 values back.

ext='.nii';
img=load_nii([input_image ext]);
X1=uint8(img.img); %extract the raw image from the structure, make it a double to be more compatible with other matlab functions
clear img;
X1=flipdim(X1,1); % do the left-right flip
[lin]=find(X1(:,:,:,1));  %identify the linear index of any nonzero value in the first of the 4d volumes.
X1size=size(X1); % Get the matrix size
[row,col,page]=ind2sub(X1size,lin);% extract the row, column and page from the linear index
Rlen=length(row);% Get the matrix size
vals=zeros(size(X1,4),Rlen); % create an empty matrix of the correct size
for i=1:Rlen  % sadly, use a for loop
     vals(:,i)=squeeze(X1(row(i),col(i),page(i),:)); % get each timeseries, squeeze it and put it in the matrix
    i=i+1; % increment i
end; % end the for loop
imgVec=vals-100; % subtract 100 from each value in the squeezed matrix, and set the output name (now we should have t-values back)
% assignin('base', 'imgVec', imgVec); 
vec2adj;

     function vec2adj
         len=length(imgVec); % get vector length
         adjMat=zeros(len, len); % create matrix of 0's
         
         % Fill in the off-diagonal cells of the adjacency matrix
         % Because i and j are different, we'll never put anything in the diagonal.
         for i= 1:len-1;
             for j=i+1:len;
                 adjMat(i,j)=imgVec(i) .* imgVec(j);
             end;
             i=i+1;
         end;
         
         % To fill in just the upper triangle with the symmetrical matrix
         adjMat=adjMat+adjMat';
         
         clear ans i j len;
     end
 end