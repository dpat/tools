function task_load(hrf_vec, win_width, slide_incr)
% This function generates the task_load following Sakoglu 2010
% Pass in an hrf vector.  This comes from the SPM.mat file SPM.xX.X 
% e.g., double click SPM.mat to load it. hrf_vec=SPM.xX.X(:,4) is column 4 of the design matrix
% For Russian this happens to be column 4 of the design matrix.  In general, you have to look at 
% the design matrix to determine which column to extract.
% pass in win_width (the size of the sliding window)
% pass in slide_incr (the amount by which to move the window for each recalculation)
% example: t=task_load([1,2,4,1,2,3,1,2,4,1], 3, 1)
% this should result in 8 values being returned: 2.3333    2.3333    2.3333    2.0000    2.0000    2.0000    2.3333    2.3333
% t=task_load('hrf_ru.mat',9,1) should generate the values for a window size of
% 9, and a slide_inc of 1.

% Note, the variable in the hrf file MUST be called hrf_vec
load(hrf_vec);

numstps = ((length(hrf_vec)-win_width)/slide_incr);
for i = 1:numstps
   t(i) = mean(hrf_vec(i:(i+win_width)-1));  %Calculation for each window
end

% Make the variable into a vector called taskload.
taskload=t';
save('taskload.mat', 'taskload');
