#!/bin/bash

: <<COMMENTBLOCK

###############################################################################
AUTHOR:         Dianne Patterson University of Arizona
DATE CREATED:   12/21/2007
----------------------------  DEPENDENCIES  -----------------------------------
The script depends on img_profile.sh found in /usr/local/tools/REF/PROFILES.
The profiles define variable names.
===============================================================================
----------------------------  PURPOSE  ----------------------------------------
This is a foreach script, designed to make iterating over subdirectories and
doing something for each one really easy.
===============================================================================
----------------------------  INPUT  ------------------------------------------
Arguments [option -h|-ne|-nl] <main_dir> <list_name>
-h help
-ne for noecho (Useful if you want to cut and paste output into file)
-nl for nolog (Do NOT log the command in each subject's log.txt)

The main directory in which to operate,
A list of subject dirs in that main directory
A command to run.
===============================================================================
----------------------------  OUTPUT  -----------------------------------------
Iterative runs of the given command. Optionally logging.
###############################################################################

COMMENTBLOCK

###############################################################################
#########################  DEFINE FUNCTIONS  ##################################

: <<COMMENTBLOCK

Function:   DoEcho
Purpose:    Echo the command and dir to standard out

COMMENTBLOCK

DoEcho ()
{
    echo ${member}
    echo ${command}
}


#==============================================================================
: <<COMMENTBLOCK

Function:   DoCmdNoEcho
Purpose:    Iteratively run command, but don't echo subject number & command
            to standard out
Input:      Options -ne or -noecho
            Main directory, subject dir list, command (interactive)
Output:     Command execution and, by default, logging

COMMENTBLOCK

DoCmdNoEcho ()
{
 for member in `cat ${list}`
    do
        cd ${member}                            # cd to the member directory
        eval ${command}                         # Run the command
        MkLog                                   # Log script and date for each subject
        cd ${main_dir}                          # cd back up to the main directory
    done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   DoCmdNoLog
Purpose:    Iteratively run command, but don't log
Input:      Options -nl or -nolog
            Main directory, subject dir list, command (interactive)
Output:     Command execution and echoing

COMMENTBLOCK

DoCmdNoLog ()
{
 for member in `cat ${list}`
    do
        DoEcho                                  # Echo subject & command
        cd ${member}                            # cd to the member directory
        eval ${command}                         # run the command
        cd ${main_dir}                          # cd back up to the main directory
    done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   DoCmdNoEchoNoLog
Purpose:    Iteratively run command, but don't log or echo
Input:      Options -nn or -nono
            Main directory, subject dir list, command (interactive)
Output:     Command execution

COMMENTBLOCK

DoCmdNoEchoNoLog ()
{
 for member in `cat ${list}`
    do
        pushd ${member}                         # cd to the member directory
        eval ${command}                         # run the command
        popd                                # cd back up to the main directory
    done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   EchoSubj
Purpose:    Echo the command and dir to standard out

COMMENTBLOCK

EchoSubj ()
{
    echo ${member}
}
#==============================================================================
: <<COMMENTBLOCK

Function:   MkLog
Purpose:    Log the command and date in log.txt for each subject
Output:     entries will be added to the subject log

COMMENTBLOCK

MkLog ()
{
    touch log.txt
    echo "For fe: ${member}: `pwd` on `date`: ${command}" >>log.txt
}

#==============================================================================
: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Provide useful help

COMMENTBLOCK

HelpMessage ()
{
    echo
    echo "Usage: $0 [option] <main_dir> <list_name>"
    echo "Example: $0  ${D}/dti_erp lst_subj_dtierp.txt"
    echo "The default (no option) echoes to the commandline and logs"
    echo
    echo "Example: $0  -nl ${D}/dti_erp lst_subj_dtierp.txt"
    echo "Option -nl or -nolog prevents logging"
    echo
    echo "Example: $0  -ne ${D}/dti_erp lst_subj_dtierp.txt"
    echo "Option -ne or -noecho prevents echoing of subject and command"
    echo
    echo "Example: $0  -nn ${D}/dti_erp lst_subj_dtierp.txt"
    echo "Option -nn or -nono does not echo or log"
    echo
    echo "Example: $0  -h"
    echo "displays this help message"
    echo
    echo "These options work with all of the fe aliases."
    echo "=================="
    exit 1
}

#==============================================================================

: <<COMMENTBLOCK

Function:   Main
Purpose:    Repeatedly run commands for each directory, log and echo by default.
            If session directories in BIDS format are present (e.g. sess-01)
            then execute commands in each session dir instead of the main dir.
Input:      Main directory, subject dir list, command (interactive)
Output:     Command execution and, by default, logging

COMMENTBLOCK

Main ()
{

  for member in `cat ${list}`
  do
    pushd ${member}                           # cd to the member directory
    DoEcho                                   # Echo subject & command
    eval ${command}                         # run the command
    MkLog                                  # Log script & date for each subject
    popd
    echo "==================="
    echo ""
    echo ""
  done

}

#########################  END FUNCTION DEFINITIONS  ##########################
###############################################################################

if [ $# -lt 1 ]; then
    HelpMessage
fi
                                                # Option flags
for arg in $*; do                               # For all commandline args
  case "$1" in                                  # Get the first arg
    -h|-help) HelpMessage;;                     # If arg=-h or -help, display help message
    -ne|-noecho) noecho="yes";                  # Set noecho=yes
        shift;;                                 # Shift to next arg
    -nl|-nolog) nolog="yes";                    # Set nolog=yes
        shift;;                                 # Shift to next arg
    -nn|-nono) nn="yes";                              # Set nn=yes
        shift;;                                 # Shift to next arg
  esac                                          # End case
done                                            # End for loop

main_dir=$1                                     # Define 1st NonOption arg

if [ -z "${main_dir}" ]; then                   # If no 1st arg exists
  HelpMessage                                   # Display help message
  exit 1
else
  echo "${main_dir}"                            # Else display name of main dir
  cd ${main_dir}                                # cd into main dir
fi

list_name=$2                                    # Define 2nd NonOption arg

if [ -z ${list_name} ]; then                    # If no 2nd arg exists
  HelpMessage                                   # Display help message
  exit 1                                        # Exit
else
  list=${TOOLS}/REF/LISTS/${list_name}          # Define list as a list in the standard dir
  echo "List is ${list}"                        # Else display list_name
fi

echo "Enter a command for all list members:"    # Ask user to type a command
read -e command
                                                # Run the command indicated by the options.
if [[ ${noecho} = "yes" ]]; then
    echo "noecho=yes"
    DoCmdNoEcho
elif [[ ${nolog} = "yes" ]]; then
    echo "nolog=${nolog}"
    DoCmdNoLog
elif [[ ${nn} = "yes" ]]; then
    DoCmdNoEchoNoLog
else
    Main                                        # If no options, then run Main
fi
