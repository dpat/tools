function corrdisp(varStemArray, varSuffixArray, field1, field2) 
% Purpose
% For each dataset, corrdisp correlates field1 and field2, 
% formatting the output and marking significance: 
% * p<=0.05; 
% ** p<=0.001
%
% Dataset and variable names are constructed from the input arguments.
% Correlation results are formatted and written out at the MATLAB
% commandline.
%
% Input Arguments
% corrdisp builds the dataset names using varStemArray and varSuffixArray. 
% corrdisp expects field1 and field2 to occur in all the datasets. 
% You can use gen_subsets to create the subsets, as corrdisp will not do
% that.
%
% Example
% varStemArray={'dprime_tracts_r', 'dprime_tracts_l'};
% varSuffixArray={'arc', 'ec', 'ilf', 'iof', 'mdlf', 'slf2', 'vof'};
% field1='MaxMusicSkill'
% field2='FA'
%
% This set of input arguments will construct dataset names like this:
% dprime_tracts_r_arc, dprime_tracts_r_ec, ...dprime_tracts_l_vof;
% and variables like this: 
% dprime_tracts_r_arc.MaxMusicSkill
% dprime_tracts_r_arc.FA
% Each such pair of variables is then correlated.
% Commandline output for this first pair of variables would look like this:
%
% dprime_tracts_r_arc: MaxMusicSkill, FA,  r= 0.21065,  p= 0.27271 
% ===========================
% function corrdisp(varStemArray, varSuffixArray, field1, field2)    

% Author Dianne Patterson 1/26/2013, University of Arizona

    % The dataset stems
    for ivarStem = (varStemArray);
        % The dataset suffixes
        for ivarSuffix= (varSuffixArray); 
            % Concatenate stem and suffix to create the dataset names
            var=char(strcat((ivarStem), '_',(ivarSuffix))); 
            % Concatenate the dataset name and field1 into a string
            % identifying the first variable to correlate.
            corrVar1=char(strcat((var), '.',(field1))); 
            % evalin gets a variable from base workspace into the function 
            % workspace. In this case, we use the concatenated string as
            % the name. This is the first vector for the correlation.
            i=evalin('base',corrVar1); 
            % Create string identifying 2nd field to correlate
            corrVar2=char(strcat((var),'.', (field2))); 
            % Import 2nd variable from the base workspace with the 
            % concatenated name.
            j=evalin('base',corrVar2); 
            % Run the correlation between i and  j, 
            % using rows with no missing values in column i or j
            [r,p]=corr(i,j, 'rows', 'pairwise'); 
           
                % Mark 2 levels of significance: *=0.05; **=0.001
                if p<= .001
                    sig='**';
                elseif p<=.05
                    sig='*';
                else sig=' ';
                end;
            
            % Format an output string reporting the results
            out=[var, ': ', field1, ', ', field2, ',  r= ', num2str(r), ',  p= ', num2str(p), (sig)];
            disp (out) % Display the output string at the commandline
            disp('===========================') 
        end % Finish correlations  for one stem in varStemArray
    end     % Finish correlations for all stems in varStemArray


