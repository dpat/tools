#!/bin/bash
# Written by Dianne Patterson, University of Arizona 2/16/09

if [ $# -lt 2 ]
then
    echo "Usage: $0 <roi intensity> <name of output file>"
    echo "Example: $0 14 frontal_pole_l"
    echo "This extracts a labeled area from the rotated Harvard-Oxford mask"
    echo "and names it with the name you specify"
    echo ""
    exit 1
fi

thresh=$1
out=$2

fslmaths HO_rot -thr $thresh -uthr $thresh $out
