#!/bin/bash

: <<COMMENTBLOCK

Function:   CheckFiles
Purpose:    Determine whether files needed for processing are present.  Add files that are missing,
            exit gracefully with errors if there is a problem.
Input:      A BIDS directory with dwi and anat files at least
Output:     Information.  If there is a problem, then an error file is generated.
COMMENTBLOCK

CheckFiles() {

  input=$1
  info_good=$2
  info_bad=$3
  err_txt=${bids_dir}/${subj}_error.txt

  ls ${input} >/dev/null 2>&1 || if [ $? = "0" ]; then
    echo "${info_good}"
  elif [ ! -e ${err_txt} ]; then
    touch ${err_txt}
    # echo "===========================================" >> ${err_txt}
    # echo "${info_bad}"
    echo "${info_bad}" >>${err_txt}
    echo "${subj}" >>${err_txt}
  elif [ -e ${err_txt} ]; then
    echo "${info_bad}"
    echo "${info_bad}" >>${err_txt}
  fi

}

#==============================================================================

: <<COMMENTBLOCK

Function:   Func2
Purpose:    Make sure necessary files exist, and confirm or exit with message
            if anything crucial is missing.  We need anat/T1w, dwi image, if we have fmap dir, we need PA image magnitude1, magnitude2 and phasediff.  All we have to do is find at least one of them. Note redirecting the output to dev/null prevents errors if there are multiple files that meet criteria.
Output:     ${subj}_error.txt in the bids_dir if there are problems with the available files.

COMMENTBLOCK

Func2() {

  CheckFiles ${bids_dir}/${subj}/fmap/${subj}_*magnitude1.nii.gz "magnitude1 exists" "No magnitude1"
  CheckFiles ${bids_dir}/${subj}/fmap/${subj}_*magnitude2.nii.gz "magnitude2 exists" "No magnitude2"
}

#==============================================================================

: <<COMMENTBLOCK

Function:   CheckFilesSetup
Purpose:    Determine whether files needed for processing are present.  Add files that are missing,
            exit gracefully with errors if there is a problem.
Input:      A BIDS directory with dwi and anat files at least
Output:     Information.  If there is a problem, then an error file is generated.
COMMENTBLOCK

CheckFilesSetup() {
  echo "========================================"
  echo "Check that critical files exist. Exit and produce error report if not."

  # Look in the fmap dir if it exists
  if [ -d ${bids_dir}/${subj}/fmap ]; then
    echo "Checking fmap files now"
    CheckFiles ${bids_dir}/${subj}/fmap/${subj}_*magnitude1.nii.gz "magnitude1 exists" "No magnitude1"
    CheckFiles ${bids_dir}/${subj}/fmap/${subj}_*magnitude2.nii.gz "magnitude2 exists" "No magnitude2"
    CheckFiles ${bids_dir}/${subj}/fmap/${subj}_*phasediff.nii.gz "phasediff exists" "No phasediff"
    CheckFiles ${bids_dir}/${subj}/fmap/${subj}_dir-*epi.nii.gz "ReversePE exists" "No ReversePE"
  else
    echo "There is no fmap dir.  Processing can proceed but corrections will be less sophisticated."
  fi

  # Look in the dwi dir if it exists
  if [ -d ${bids_dir}/${subj}/dwi ]; then
    CheckFiles ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz "dwi exists" "No dwi"
    if [ -e ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz ]; then
      DIM4=$(fslinfo ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz | grep -m 1 dim4 | awk '{print $2}')
      if [[ ${DIM4} -lt 6 ]]; then
        touch ${bids_dir}/${subj}_error.txt
        echo "dwi has multiple volumes. good."
        echo "dwi has ${DIM4} volumes.  Something is wrong."
        echo "dwi has ${DIM4} volumes.  Something is wrong." >>${bids_dir}/${subj}_error.txt
      else
        # If the dwi appears suitable, ensure that index.txt exists.  If it does not exist, then create it with correct number of volumes
        if [ ! -e ${bids_dir}/index*.txt ]; then
          touch ${bids_dir}/index.txt
          numvols=$(fslinfo ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz | grep -m 1 dim4 | awk '{print $2}')
          indx="1"
          for ((i = 1; i < ${numvols}; i += 1)); do indx="$indx 1"; done
          echo $indx >${bids_dir}/index.txt
        fi
      fi
    fi
  else
    touch ${bids_dir}/${subj}_error.txt
    echo "There is no dwi dir"
    echo "There is no dwi dir" >>${bids_dir}/${subj}_error.txt
  fi

  # Look in the anat dir if it exists
  if [ -d ${bids_dir}/${subj}/anat ]; then
    CheckFiles ${bids_dir}/${subj}/anat/${subj}*T1*.nii.gz "T1w exists" "No T1w"
    T1w_count=$(ls ${bids_dir}/${subj}/anat/${subj}*T1*.nii.gz | wc -l)
    echo "T1w_count is ${T1w_count}"
    if [ ${T1w_count} -gt 1 ]; then
      echo "There are multiple anat/T1w volumes. This is a problem for BIP."
      if [ ! -e ${bids_dir}/${subj}_error.txt ]; then
        touch ${bids_dir}/${subj}_error.txt
        echo "${subj}" >>${bids_dir}/${subj}_error.txt
        echo "There are multiple anat/T1w volumes. This is a problem for BIP." >>${bids_dir}/${subj}_error.txt
      else
        echo "There are multiple anat/T1w volumes. This is a problem for BIP." >>${bids_dir}/${subj}_error.txt
      fi
    else
      touch ${bids_dir}/${subj}_error.txt
    fi
  else
    echo "There is no anat dir"
    echo "There is no anat dir" >>${bids_dir}/${subj}_error.txt
  fi

}
#==============================================================================

# We pass 1 arguments for testing in the CWD: the subjectnum

subjectnum=$1
subj=sub-${subjectnum}
###################
echo "====================="
echo "subject=${subj}"
echo "bids_dir = ${PWD}"
echo "====================="

if [ $# -lt 1 ]; then
  "HelpMessage"
  exit 1
fi
CheckFilesSetup subjectnum=$1
