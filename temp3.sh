#!/bin/bash

sub=`basename ${PWD}`

# Calculate brain mask and lesion volumes and then the percentile of lesion relative to brain mask.
# This may not work if total_voxels 1 differs from union voxels (check sub-027)
jaccard.sh ${sub}_brain_mask lesionmask
jaccard.sh ant1/${sub}_MNI_brain_mask ant1/${sub}_MNI_lesion
jaccard.sh ant2/${sub}_MNI_brain_mask ant2/${sub}_MNI_lesion
jaccard.sh fsl/${sub}_MNI_brain_mask fsl/${sub}_MNI_lesion
jaccard.sh spm_brain/${sub}_MNI_brain_mask spm_brain/${sub}_MNI_lesion
#jaccard.sh spm_head/${sub}_MNI_brain_mask spm_head/${sub}_MNI_lesion
#jaccard.sh spm_tpm_brain/${sub}_MNI_brain_mask spm_tpm_brain/${sub}_MNI_lesion
#jaccard.sh spm_tpm_head/${sub}_MNI_brain_mask spm_tpm_head/${sub}_MNI_lesion

#Calculate the overlap of the MNI maps
