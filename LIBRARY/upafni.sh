#!/bin/bash

   #------------------------------------------------------------------------
   #
   #  Purpose: Check for updates and if necessary update afni.
   #
   #------------------------------------------------------------------------

   # get today's date for comparison
   today=`date "+ %d %m %Y"`

   # check that the current AFNI version is recent enough
   afni_history -check_date ${today}

   # When checking the date, if there was an error
   if [[ $? -ne 0 ]]; then
       echo "** this script will get you newer AFNI binaries (newer than "$(date +"%B %d %Y")
       sudo @update.afni.binaries -defaults
   fi
