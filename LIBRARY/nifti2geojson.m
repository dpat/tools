function nifti2geojson(inputImage, orientation, labelFile, resolution, varargin)
% Written by Dianne Patterson and Tom Hicks 7/30/2014
% This function released in accordance with the Creative Commons Attribution-NonCommercial 
% Unported License: http://creativecommons.org/licenses/by-nc/4.0/legalcode
% This function creates geojson files from atlas slices (assumes MNI152 space; FSL atlas).
% It requires jsonlab tools:
% http://www.mathworks.com/matlabcentral/fileexchange/33381-jsonlab--a-toolbox-to-encode-decode-json-files-in-matlab-octave)
% It also requires the Tools for Nifti and ANALYZE image toolbox:
% http://www.mathworks.com/matlabcentral/fileexchange/8797-tools-for-nifti-and-analyze-image
% and the matlab image processing toolbox.

% Correctly named atlas slices can be created with the geo_prep.sh Split function, which will also
% create and populate the subdirs ax, cor and sag 

% to check well-formedness of geojson file, use geoJSONlint.sh

% (geo_wrap.m calls this function iteratively).

% example usages:
%  1)  nifti2geojson('ax_40', 'a', '../labels_all.txt', 1)
%       processes slice ax_40.nii with axial 'a' orientation (upper or lower case allowed for orientation),
%        uses labels.txt for label names, resolution =1mm
%	    output to command window
%   2) nifti2geojson('cor_40', 'C', '../labels_all.txt', 2)
%       processes slice cor_40.nii with 'c' coronal orientation,  uses labels.txt for label names,
%	    resolution =2mm, output to command window
%   3) nifti2geojson('sag_40', 's', '../labels_all.txt', 1, 0.5)
%       processes slice sag_40.nii with 's' sagittal orientation,  uses labels.txt for label names,
%	    resolution =0.5mm, output to file named sag_40.geojson

% orientation will be a=axial,c=coronal or s=sagittal
% labels.txt will be created by running the geo_prep.sh function StripXML.
% This file contains both the short and long names (at least for the 2 Harvard-Oxford atlases)

% resolution will be  2=2mm, 1=1mm, or 0.5=0.5mm

writefile = 0;  % By defult, do not write a file.
if nargin > 4
	writefile = varargin{1}; % In this case, write the file
end

img=load_nii([inputImage '.nii']);  % load the nifti image
im=squeeze(img.img);  % extract the image from the structure into an array
clear img img.img ext; % clean up

% Declare zero mm position on each axis in 2mm standard MNI152 space
mm0_X=45;  % Right-Left is positive-negative from the zero midline
mm0_Y=63;  % Ant-Post is positive-negative from the zero midline
mm0_Z=36;  % Sup-Inf is positive-negative from the zero midline

% Get the list of possible region labels 
Labels=readtable(labelFile); % Read in the 2 columns of short and long region labels
regionLabels=table2cell(Labels(:,1)); % extract the 1st column (short labels) into a cell array 
regionLongLabels=table2cell(Labels(:,2)); % extract the 2nd column (long labels) into a cell array 

% Get the current slice number
sliceNum=(strsplit(inputImage, '_'));
sliceNum=str2num(sliceNum{2});

% Calculate mm position of current slice number
% This will be used in the name of the output geojson file
if strcmp(lower(orientation), 'a') % if this IS an axial slice
	if resolution==2
		mm=(sliceNum - mm0_Z) * 2;
	elseif resolution==1
		mm=(sliceNum - (mm0_Z * 2));
	elseif resolution==0.5
		mm=(sliceNum - (mm0_Z * 4) ) * 0.5;
	end
	% Compute pos variable for axial
	if mm > 0
	   pos='sup';
	   else
	   pos='inf';
	end   	
end

if strcmp(lower(orientation), 'c') % if this IS a coronal slice
	if resolution==2
		mm=(sliceNum - mm0_Y) * 2;
	elseif resolution==1
		mm=(sliceNum - (mm0_Y * 2));
	elseif resolution==0.5
		mm=(sliceNum - (mm0_Y * 4)) * 0.5;
	end	
	% Compute pos variable for coronal
	if mm > 0
	   pos='ant';
	   else
	   pos='post';
	end   
end	

if strcmp(lower(orientation), 's') % if this IS a sagittal slice
	if resolution==2
		mm=(mm0_X - sliceNum) * 2;
	elseif resolution==1
		mm=((mm0_X * 2) - sliceNum);
	elseif resolution==0.5
		mm=((mm0_X * 4) - sliceNum) * 0.5;
	end	
	% Compute pos and side variable for sagittal
	if mm > 0
	   pos='R';
	   else 
	   pos='L';
	end   
	side=pos;
end	

mm=num2str(mm);

geoJ.type = 'FeatureCollection';  % set up initial GeoJSON structure
geoJ.orientation = orientation;  % set up initial GeoJSON orientation information
geoJ.pos = pos;  % set up initial GeoJSON position information
geoJ.mm = mm;  % set up initial GeoJSON mm position information

% for axial and coronal slices, reorient the image to match FSL. NOT needed for sagittals.
if ~strcmp(lower(orientation), 's')
	im=flipdim(im,1);  % left-right flip
end

regions = [];
labelVals=unique(nonzeros(im));  % List unique nonzero values in the image

for i=1:length(labelVals)  % for each brain region label value
	labelName=regionLabels{labelVals(i)};
	longLabel=regionLongLabels{labelVals(i)};
	labelVal=num2str(labelVals(i));  % create a string to hold the label value and use in naming output files

	% compute the perimeters for each brain region
	perims=bwboundaries(im==labelVals(i), 'noholes');  % get the perimeter value

	% extract any cells from the cell arrays and subtract 1 to correct for FSL zero indexing
	for brInstance=1:length(perims)  % for each instance of this brain region
		if length(perims{brInstance}) > 3 % every polygon requires 3 unique vertices
			thisperim=(perims{brInstance});  % create matrix for each cell
			thisperim=thisperim - 1;	% subtract 1 from each matrix to correct for 1-off error in coordinates relative to FSL

			% For axials and coronals, determine left, right or medial for the structures
			% The x values are smaller than the midline value on the right and larger than the midline values on the left
			if ~strcmp(lower(orientation), 's') % if this is NOT a sagittal image
				LRMin=min(thisperim(:,1));
				LRMax=max(thisperim(:,1));
				if resolution==2
					if LRMax < mm0_X % 45 is the midline for 2mm images
						side='R';
					else
						side='L' ;
					end
				end
				if resolution==1
					if LRMax < mm0_X * 2  % 90 is the midline for 1mm images
						side='R';
					else
						side='L' ;
					end
				end
				if resolution==0.5
					if LRMax < mm0_X * 4  % 180 is the midline for 0.5 mm images
						side='R';
					else
						side='L' ;
					end
				end
			end
			js.type = 'Feature';
			js.geometry.type = 'Polygon';
			js.geometry.coordinates = thisperim;
			js.properties.regionName = strcat(labelName,'_',side); % construct a name for each region instance
			js.properties.regionLongName = strcat(longLabel, '_',side); % get the long name for each region instance
			js.properties.regionIdLabel = strcat(labelVal ,'_', side); % construct ID label for each region instance
			
			js.properties.regionId = labelVal;
			regions = [regions js];
		end
	end
end

geoJ.features = regions;  % save array of features

% save in geojson format
if writefile
	filename = strcat(inputImage, '_', pos, '_', mm, 'mm.geojson');
	savegeojson('', geoJ, filename); % leave rootname empty or extra nested level gets created
else
	savegeojson('', geoJ);
end