% This wrapper script creates arrays that are concatenated in for loops to
% create the filenames we pass to toMat.
% Dianne Patterson, Created: Aug 17, 2013


%clustArray={'cluster01_l_anterior_insula', 'cluster02_l_caudate', 'cluster03_l_cerebellum', 'cluster04_l_dorsolateral_prefrontal', 'cluster05_l_dorsomedial_frontal', 'cluster06_l_inferior_frontal', 'cluster07_l_ITG', 'cluster08_l_parietal', 'cluster09_l_STG', 'cluster10_l_thalamus', 'cluster11_r_cerebellum', 'cluster12_r_inferior_frontal', 'cluster13_r_ITG', 'cluster14_r_postcentral_gyrus', 'cluster15_r_precentral_gyrus', 'cluster16_r_STG', 'cluster17_r_dorsomedial_frontal'};
%clustArray={'cluster17_r_dorsomedial_frontal'};
%subjArray={'sub009', 'sub011', 'sub012', 'sub013', 'sub016', 'sub018', 'sub019', 'sub021', 'sub022', 'sub023', 'sub027', 'sub028', 'sub030', 'sub031', 'sub032', 'sub033', 'sub035', 'sub038', 'sub039', 'sub045', 'sub046', 'sub047', 'sub048', 'sub049', 'sub050', 'sub051', 'sub057', 'sub059', 'sub060', 'sub067', 'sub069', 'sub073'};

groupTArray={'100+all_learn_sent_group_t', '100+all_unlearn_sent_group_t'};
pathMat='/Exps/Analysis/HP_WB1_DP/Mat_out';

% Build coordinate sets for each roi.
% Run from groupmask dir.
% Make sure images are unzipped.
% Make sure HP_WB1_DP/Mat_out exists.

% for ivarStem = (clustArray);
%          %  The dataset names
%              image=char(strcat((ivarStem), '.nii')) ; 
%              im_coords=char(strcat((pathMat),'/',(ivarStem), '_coords'));
%              coords=toMatXYZ([image]);
%               save ( [im_coords], 'coords'); % save the output in a single subject mat file.
%          end;
%          clear


% Build groupT-mat file names
% Run inside /Exps/Analysis/HP_WB1_DP/groupmask/group_tstat_clusters
% gunzip nii files first; zip them up afterwards
% This assumes Tstats from afni, which have had 100 added to them and 
% are in a 5D array.  Only the first 4D array contains the T-stats, so we
% dump the 2nd one in toMatT.  Finally, having squeezed the results to get
% rid of 0's we are free to subtract 100 from the remaining values to get
% our original T-values back.
% HP_WB1_DP/Mat_out/GroupT must exist

%  for ivarStem = (clustArray);
%         %  The dataset suffixes
%         for ivarSuffix= (groupTArray); 
%         %  Concatenate stem and suffix to create the dataset names
%             image=char(strcat((ivarStem), '_',(ivarSuffix),'.nii'))  
%             outMat=char(strcat((pathMat),'/GroupT/',(ivarStem), '_',(ivarSuffix)))
%             toMatT([image], [outMat]);
% 
%         end;
%         end;
%         clear
     
% Build Subject T-mat files    
% Run inside /Exps/Analysis/HP_WB1_DP/groupmask/subject_tstat_clusters
% gunzip nii files first; zip them up afterwards
% This assumes Tstats from afni, which have had 100 added to them and 
% are in a 5D array.  Only the first 4D array contains the T-stats, so we
% dump the 2nd one in toMatT.  Finally, having squeezed the results to get
% rid of 0's we are free to subtract 100 from the remaining values to get
% our original T-values back.

  % for ivarStem = (clustArray);
  %       % The dataset suffixes
  %       for ivarSuffix= (subjArray); 
  %           % Concatenate stem and suffix to create the dataset names
  %           image=char(strcat((ivarStem), '_100+all_T_',(ivarSuffix),'.nii'))  
  %           outMat=char(strcat((pathMat),'/subj_t_mats/',(ivarStem), '_100+all_T_',(ivarSuffix)))
  %           toMatT([image], [outMat]);
  %       end;
  %       end;
  %       clear      
%         

% Build Subject Raw-mat files    
% Run inside /Exps/Analysis/HP_WB1_DP/groupmask/subject_raw_clusters
% gunzip nii files first; zip them up afterwards

  % for ivarStem = (clustArray);
  %       % The dataset suffixes
  %       for ivarSuffix= (subjArray); 
  %           % Concatenate stem and suffix to create the dataset names
  %           image=char(strcat((ivarStem), '_all_runs_',(ivarSuffix),'.nii'))  
  %           outMat=char(strcat((pathMat),'/subj_raw_mats/',(ivarStem), '_all_runs_',(ivarSuffix)))
  %           toMat([image], [outMat]);
  %       end;
  %       end;
  %       clear      
%         

% This part was rewritten to handle simpler filenames requested by
% Huanping.  To create those simpler filenames I renamed files in the
% source directories using regular expressions in pathfinder 3/27/2014

clustArray2={'1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11','12', '13', '14', '15', '16', '17'};
Array2={'s9', 's11', 's12', 's13', 's16', 's18', 's19', 's21', 's22', 's23', 's27', 's28', 's30', 's31', 's32', 's33', 's35', 's38', 's39', 's45', 's46', 's47', 's48', 's49', 's50', 's51', 's57', 's59', 's60', 's67', 's69', 's73'};

% Organize mat file by subject
% Run in Mat_out
% example 1: /Exps/Analysis/HP_WB1_DP/Mat_out/subj_raw_mats/s13_3.mat
% example 2: /Exps/Analysis/HP_WB1_DP/Mat_out/subj_t_mats/s13_3_t.mat
% There should be 17 for each subject in each of the above 2 directories
% Load them all in the workspace and then save them as a mat file with the
% subject num, e.g., s13.mat
   for ivarStem= (Array2);
        % The dataset suffixes
         for ivarSuffix = (clustArray2);
            % Concatenate stem and suffix to create the dataset names
            inmatRaw=char(strcat('subj_raw_mats/',(ivarStem),'_',(ivarSuffix),'.mat')); 
            newRawclust=char(strcat((ivarStem),'_',(ivarSuffix),'_raw')); 
            inmatT=char(strcat('subj_t_mats/',(ivarStem), '_',(ivarSuffix),'_t.mat'));
            newTclust=char(strcat((ivarStem),'_',(ivarSuffix),'_t'));
            outMat=char(strcat((pathMat),'/',(ivarStem)));
            load (inmatRaw);
            sprintf(newRawclust)
            eval([sprintf(newRawclust),'=clust;']); % rename raw "clust" variable with a new variable name
            clear clust;
            load (inmatT);  
            eval([sprintf(newTclust),'=clust;']); % rename t "clust" variable with a new variable name
            clear clust;
        end;

        save (outMat, 's*'); % save the output in a single subject mat file.
        clear s*;
        end;
        clear      
%         
%============Old version below ======================
% Organize mat file by subject
% Run in Mat_out
% example 1: /Exps/Analysis/HP_WB1_DP/Mat_out/subj_raw_mats/cluster3_all_runs_sub013.mat
% example 2: /Exps/Analysis/HP_WB1_DP/Mat_out/subj_t_mats/cluster2_100+all_T_sub013.mat
% There should be 17 for each subject in each of the above 2 directories
% Load them all in the workspace and then save them as a mat file with the
% subject num, e.g., sub013.mat
%    for ivarSuffix= (subjArray2);
%         % The dataset suffixes
%          for ivarStem = (clustArray2);
%             % Concatenate stem and suffix to create the dataset names
%             inmatRaw=char(strcat('subj_raw_mats/',(ivarStem), '_all_runs_',(ivarSuffix),'.mat')); 
%             newRawclust=char(strcat((ivarSuffix),'_',(ivarStem),'_raw')); 
%             inmatT=char(strcat('subj_t_mats/',(ivarStem), '_100+all_T_',(ivarSuffix),'.mat'));
%             newTclust=char(strcat((ivarSuffix),'_',(ivarStem),'_t'));
%             outMat=char(strcat((pathMat),'/',(ivarSuffix)));
%             load (inmatRaw);
%             eval([sprintf(newRawclust),'=clust;']); % rename raw "clust" variable with a new variable name
%             clear clust;
%             load (inmatT);  
%             eval([sprintf(newTclust),'=clust;']); % rename t "clust" variable with a new variable name
%             clear clust;
%         end;
% 
%         save (outMat, 'sub*cluster*'); % save the output in a single subject mat file.
%         clear sub*cluster*;
%         end;
%         clear      
%         
