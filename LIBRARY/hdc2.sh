#!/bin/bash

#==============================================================================
: <<COMMENTBLOCK

   Function:   HelpMessage
   Purpose:    function_purpose
   Input:      input_files
   Output:     output

COMMENTBLOCK

function HelpMessage ()
{
cat <<- "EOF"
======================================================
Four arguments are required. An optional 5th argument can be added to encode the session and related info if needed
argument 1: Name of Dicom directory
argument 2: Name of BIDS folder for NIfTI images
argument 3: Name of conversion file in bidsdir/code directory, e.g., convertall.py
argument 4: Name of subject dicom folder to convert, e.g., 219

e.g., hdc2.sh Dicom data convertall.py 304 

OPTIONAL SESSION ARGUMENT 
argument 5: 
This argument specifies that your DICOMS are nested under subject and then session

e.g., hdc2.sh Dicom data convertall.py 304 itbs

Output will be a BIDS directory under the bidsdir folder.  
This assumes you are running Docker
and have downloaded heudiconv: docker pull nipy/heudiconv
It also assumes that you are running from the parent directory to both Dicom and the bids dir

Warning: The nesting check will report the first file it finds.  If you have an extra file in the DICOM directory, it'll find that!
If the nesting check reports a non-dicom file, then you should remove that from the dicom directory and try again.

======================================================
EOF
}

#==========================================================================

: <<COMMENTBLOCK
This code calls the docker heudiconv tool to convert DICOMS into the BIDS data structure.
It requires that you are in the parent directory of both the Dicom and Nifti directories AND that your Nifti directory contain a subdirectory called Code with the conversion routine, e.g., convertall.py in it. See https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/heudiconv.html.
COMMENTBLOCK

# Exit if number of arguments is too small
if [ $# -lt 4 ]
  then
  HelpMessage
  exit 1
fi

# Define the variables
dicomdir=${1}
bidsdir=${2}
converter=${3}
subject=${4}
session=${5} # optional

# If there are 4 arguments
if [ $# -eq 4 ]; then
  echo ""
  echo "================================================"
  echo ""
  echo "BIDS data will be placed in ${bidsdir}/sub-${subject}"
  echo ""
  # Remove any pesky .DS_Store files in the dicomdir
  find ${dicomdir} -type f -name .DS_Store -exec rm {} \;
  echo "nesting check: This should list a real DICOM file:"
  filepath=$(find ${dicomdir}/${subject} -type f | head -n 1)
  echo "${filepath}"   
  echo ""
  echo "================================================"
  echo ""
  filepathdepth=$( echo ${filepath} | awk -F"/" 'NF > max {max = NF} END {print max-3}' )

  thresh=1
  while [  ${thresh} -le ${filepathdepth} ]; do
    nest="${nest}/*"
    ((thresh+=1))
  done

  echo ""
  echo "================================================"
  echo ""
  docker run --rm -it -v ${PWD}:/base -u $UID nipy/heudiconv:latest -d /base/${dicomdir}/{subject}${nest}/*.* -o /base/${bidsdir}/ -f /base/${bidsdir}/code/${converter} -s ${subject} -c dcm2niix -b --minmeta --overwrite
  
  echo "================================================"
  echo ""
  
  docker_command="docker run --rm -it -v ${PWD}:/base -u $UID nipy/heudiconv:latest -d /base/${dicomdir}/{subject}${nest}/*.* -o /base/${bidsdir}/ -f /base/${bidsdir}/code/${converter} -s ${subject} -c dcm2niix -b --minmeta --overwrite"
  echo "This is the Docker command that was run.  See also ${bidsdir}/code/docker_sub-${subject}.txt "
  echo "$docker_command"  
  echo "$docker_command" > ${bidsdir}/code/docker_sub-${subject}.txt
 
# else if there are 5 arguments
elif [ $# -eq 5 ]; then
  echo ""
  echo "================================================"
  echo ""
  echo "BIDS data will be placed in ${bidsdir}/sub-${subject}/ses-${session}" 
  echo ""
  # Remove any pesky .DS_Store files in the dicomdir
  find ${dicomdir} -type f -name .DS_Store -exec rm {} \;
  echo "nesting check: This should list a real DICOM file:"
  filepath=$(find ${dicomdir}/${subject}/${session} -type f | head -n 1)
  echo "${filepath}"   
  echo ""
  echo "================================================"
  echo ""
  filepathdepth=$( echo ${filepath} | awk -F"/" 'NF > max {max = NF} END {print max-4}' )

  thresh=1
  while [  ${thresh} -le ${filepathdepth} ]; do
    nest="${nest}/*"
    ((thresh+=1))
  done

  echo ""
  echo "================================================"
  echo ""
  docker run --rm -it -v ${PWD}:/base -u $UID nipy/heudiconv:latest -d /base/${dicomdir}/{subject}/{session}${nest}/*.* -o /base/${bidsdir}/ -f /base/${bidsdir}/code/${converter} -s ${subject} -ss ${session} -c dcm2niix -b --minmeta --overwrite
  
  echo "================================================"
  echo "" 
  
  docker_command="docker run --rm -it -v ${PWD}:/base -u $UID nipy/heudiconv -d /base/${dicomdir}/{subject}/{session}${nest}/*.* -o /base/${bidsdir}/ -f /base/${bidsdir}/code/${converter} -s ${subject} -ss ${session} -c dcm2niix -b --minmeta --overwrite"
  echo "This is the Docker command that was run. See also ${bidsdir}/code/docker_sub-${subject}_sess-${session}.txt"  
  echo "$docker_command"  
  echo "$docker_command" > ${bidsdir}/code/docker_sub-${subject}_sess-${session}.txt
  
  
# For anything else, spit out the help message and stop.
else
  HelpMessage
  exit 1
fi
