%-----------------------------------------------------------------------
% Job saved on 29-May-2019 12:54:48 by cfg_util (rev $Rev: 6942 $)
% spm SPM - SPM12 (7219)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.tools.MRI.MRnorm.anat = {'anat_T1w.nii,1'};
matlabbatch{1}.spm.tools.MRI.MRnorm.les = {'lesion.nii,1'};
matlabbatch{1}.spm.tools.MRI.MRnorm.t2 = '';
matlabbatch{1}.spm.tools.MRI.MRnorm.modality = 4;
matlabbatch{1}.spm.tools.MRI.MRnorm.brainmask = 0;
matlabbatch{1}.spm.tools.MRI.MRnorm.bb = [-90 -126 -72
                                          90 90 108];
matlabbatch{1}.spm.tools.MRI.MRnorm.vox = [1 1 1];
matlabbatch{1}.spm.tools.MRI.MRnorm.DelIntermediate = 1;
matlabbatch{1}.spm.tools.MRI.MRnorm.AutoSetOrigin = 1;
