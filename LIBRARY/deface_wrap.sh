#!/bin/bash

# This assumes you have moved into the subject dir
# This will be the case if you call this with the fe script.

# Because we are in the subject directory,
# we can get the subject number with basename.
subject=`basename ${PWD}`
# Because we are in the subject directory,
# we can use dirname to get our parent directory.
datadir=`dirname ${PWD}`

# Just making sure
echo "subject is ${subject}"
echo "datadir is ${datadir}"
echo "whole path ${datadir}/sourcedata/${subject}/anat/"

# If the sourcedata subdir does not exist, then create it
if [ ! -d ${datadir}/sourcedata/${subject}/anat/ ]; then
  mkdir -p ${datadir}/sourcedata/${subject}/anat/
fi

# We move to the anat subdir
cd anat

# Check that there is nothing in the anat directory already called deface:
if [ -e ${subj}_T1w_defacemask.nii.gz ] 
  then 
  echo "You have already defaced this image."
  exit 1
fi

# We look for any T1w*.nii.gz files in the current anat dir
for img in *T1w.nii.gz; do
  # Just making sure we have the right thing
  echo ${img}
  # get the image stem: we'll need this for naming the output
  img_stem=`basename -s .nii.gz ${img}`
  # copy the original T1w image to the sourcedata for safekeeping
  cp ${img} ${datadir}/sourcedata/${subject}/anat/
  cp ${img}.json ${datadir}/sourcedata/${subject}/anat/
  # run deface. The output is named with *_defaced appended
  # to the img_stem.
  deface.sh ${img}
  # rename the output of deface in place
  mv ${img_stem}_defaced.nii.gz ${img_stem}.nii.gz
done
