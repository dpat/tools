% Enter the values for the variables required for the ICA analysis.
% Variables are on the left and the values are on the right.
% Characters must be enterd in single quotes
% This example is based on  GroupICATv4.0c/icatb/icatb_batch_files/Input_data_subjects_2.m
%
% After editing the parameters, use icatb_batch_file_run('batch_subjects1'); 

%% Modality. Options are fMRI and EEG
modalityType = 'fMRI';

%% Type of analysis
% Options are 1, 2 and 3.
% 1 - Regular Group ICA
% 2 - Group ICA using icasso
% 3 - Group ICA using MST
which_analysis = 2;

%% ICASSO options.
% This variable will be used only when which_analysis variable is set to 2.
icasso_opts.sel_mode = 'both';  % Options are 'randinit', 'bootstrap' and 'both'
icasso_opts.num_ica_runs = 10; % Number of times ICA will be run
% Most stable run estimate is based on these settings. 
icasso_opts.min_cluster_size = 8; % Minimum cluster size
icasso_opts.max_cluster_size = 10; % Max cluster size. 

%% Enter TR in seconds. If TRs vary across subjects, TR must be a row vector of length equal to the number of subjects.
TR = 1;

%% Group ica type
% Options are spatial or temporal for fMRI modality. By default, spatial
% ica is run if not specified.
group_ica_type = 'spatial';


%% Parallel info
% enter mode serial or parallel. If parallel, enter number of
% sessions/workers to do job in parallel
parallel_info.mode = 'serial';

%% Group PCA performance settings. Best setting for each option will be selected based on variable MAX_AVAILABLE_RAM in icatb_defaults.m. 
% If you have selected option 3 (user specified settings) you need to manually set the PCA options. 
%
% Options are:
% 1 - Maximize Performance
% 2 - Less Memory Usage
% 3 - User Specified Settings
perfType = 1;

%% Design matrix selection 
% Design matrix (SPM.mat) is used for sorting the components
% temporally (time courses) during display. Design matrix will not be used during the
% analysis stage except for SEMI-BLIND ICA.
% options are ('no', 'same_sub_same_sess', 'same_sub_diff_sess', 'diff_sub_diff_sess')
% 1. 'no' - means no design matrix.
% 2. 'same_sub_same_sess' - same design over subjects and sessions
% 3. 'same_sub_diff_sess' - same design matrix for subjects but different
% over sessions
% 4. 'diff_sub_diff_sess' - means one design matrix per subject.

keyword_designMatrix = 'no';

%% Specify location of design matrix here if you have selected 'same_sub_same_sess' or
% 'same_sub_diff_sess' option for keyword_designMatrix variable
% OnedesignMat = '/Volumes/Main/Users/dpat/Downloads/nifti_subjects/derivatives/SPM.mat';

%% There are three ways to enter the subject data
% options are 1, 2, 3 or 4
dataSelectionMethod = 2;

%% Method 2 

% If you have different filePatterns and location for subjects not in one
% root folder then enter the data here.
% Number of subjects is determined getting the length of the selected subjects. specify the data set or data sets needed for 
% the analysis here.

selectedSubjects = {'s1', 's2', 's3'};  % naming for subjects s1 refers to subject 1, s2 means subject 2. Use cell array convention even in case of one subject one session

% Number of Sessions (i.e. number of functional runs)
numOfSess = 1;

% functional data folder, file pattern and file numbers to include
% You can provide the file numbers ([1:220]) to include as a vector. If you want to
% select all the files then leave empty.

s1_s1 = {'/Volumes/Main/Users/dpat/Downloads/nifti_subjects/derivatives/sub-001/func', '*bold.nii.gz'}; % subject 1 session 1
% example of adding another session for subject 1
% s1_s2 = {'/Volumes/Main/Users/dpat/Downloads/nifti_subjects/derivatives/sub-001/func', '*bold.nii.gz'}; % subject 1 session 2

s2_s1 = {'/Volumes/Main/Users/dpat/Downloads/nifti_subjects/derivatives/sub-002/func', '*bold.nii.gz'}; % subject 2 session 1

s3_s1 = {'/Volumes/Main/Users/dpat/Downloads/nifti_subjects/derivatives/sub-003/func', '*bold.nii.gz'}; % subject 3 session 1

%%%%%%%%%%%%%%%%%%%%%%% end for Method 2 %%%%%%%%%%%%%%

%% Enter directory to put results of analysis
outputDir = '/Volumes/Main/Users/dpat/Downloads/nifti_subjects/gift_analysis_batch';

%% Enter Name (Prefix) Of Output Files
prefix = 'vmb';

%% Enter location (full file path) of the image file to use as mask
% or use Default mask which is []
maskFile = [];

%% Group PCA Type. Used for analysis on multiple subjects and sessions.
% Options are 'subject specific' and 'grand mean'. 
%   a. Subject specific - Individual PCA is done on each data-set before group
%   PCA is done.
%   b. Grand Mean - PCA is done on the mean over all data-sets. Each data-set is
%   projected on to the eigen space of the mean before doing group PCA.
%
% NOTE: Grand mean implemented is from FSL Melodic. Make sure that there are
% equal no. of timepoints between data-sets.
%
group_pca_type = 'subject specific';

%% Back reconstruction type. Options are str and gica
backReconType = 'gica';

%% Data Pre-processing options
% 1 - Remove mean per time point
% 2 - Remove mean per voxel
% 3 - Intensity normalization
% 4 - Variance normalization
preproc_type = 3;


%% PCA Type. Also see options associated with the selected pca option.
% Standard PCA and SVD PCA options are commented.
% PCA options are commented.
% Options are 1, 2, 3, 4 and 5.
% 1 - Standard 
% 2 - Expectation Maximization
% 3 - SVD
% 4 - MPOWIT
% 5 - STP
pcaType = 1;

%% PCA options (Standard)

% % a. Options are yes or no
% % 1a. yes - Datasets are stacked. This option uses lot of memory depending
% % on datasets, voxels and components.
% % 2a. no - A pair of datasets are loaded at a time. This option uses least
% % amount of memory and can run slower if you have very large datasets.
pca_opts.stack_data = 'yes';
% 
% % b. Options are full or packed.
% % 1b. full - Full storage of covariance matrix is stored in memory.
% % 2b. packed - Lower triangular portion of covariance matrix is only stored in memory.
% pca_opts.storage = 'full';
% 
% % c. Options are double or single.
% % 1c. double - Double precision is used
% % 2c. single - Floating point precision is used.
pca_opts.precision = 'single';
% 
% % d. Type of eigen solver. Options are selective or all
% % 1d. selective - Selective eigen solver is used. If there are convergence
% % issues, use option all.
% % 2d. all - All eigen values are computed. This might run very slow if you
% % are using packed storage. Use this only when selective option doesn't
% % converge.
% 
% pca_opts.eig_solver = 'selective';

%% Maximum reduction steps you can select is 2. Options are 1 and 2. For temporal ica, only one data reduction step is
% used.
numReductionSteps = 2;

%% Batch Estimation. If 1 is specified then estimation of 
% the components takes place and the corresponding PC numbers are associated
% Options are 1 or 0
doEstimation = 0; 

%% MDL Estimation options. This variable will be used only if doEstimation is set to 1.
% Options are 'mean', 'median' and 'max' for each reduction step. The length of cell is equal to
% the no. of data reductions used.
estimation_opts.PC1 = 'mean';
estimation_opts.PC2 = 'mean';

%% Number of pc to reduce each subject down to at each reduction step
% The number of independent components the will be extracted is the same as 
% the number of principal components after the final data reduction step.  
numOfPC1 = 30;
numOfPC2 = 20;

%% Scale the Results. Options are 0, 1, 2, 3 and 4
% 0 - Don't scale
% 1 - Scale to Percent signal change
% 2 - Scale to Z scores
% 3 - Normalize spatial maps using the maximum intensity value and multiply timecourses using the maximum intensity value
% 4 - Scale timecourses using the maximum intensity value and spatial maps using the standard deviation of timecourses
scaleType = 2;


%% 'Which ICA Algorithm Do You Want To Use';
% see icatb_icaAlgorithm for details or type icatb_icaAlgorithm at the
% command prompt.
% Note: Use only one subject and one session for Semi-blind ICA. Also specify atmost two reference function names

% 1 = infomax, 2 = fastICA, etc.
algoType = 1;

%% Specify at most two reference function names if you select Semi-blind ICA algorithm.
% Reference function names can be acessed by loading SPM.mat in MATLAB and accessing 
% structure SPM.xX.name.
refFunNames = {'Sn(1) right*bf(1)', 'Sn(1) left*bf(1)'};

%% Report generator (fmri and smri only)
display_results.formatName = 'html'; 
display_results.slices_in_mm = (-40:4:72);
display_results.convert_to_zscores = 'yes';
display_results.threshold = 1.0;
display_results.image_values = 'positive';
display_results.slice_plane = 'axial';
display_results.anatomical_file = '/usr/local/MATLAB_TOOLS/GroupICATv4.0c/icatb/icatb_templates/ch2bet_3x3x3.nii';

%% ICA Options - Name by value pairs in a cell array. Options will vary depending on the algorithm. See icatb_icaOptions for more details. Some options are shown below.
% Infomax -  {'posact', 'off', 'sphering', 'on', 'bias', 'on', 'extended', 0}
% FastICA - {'approach', 'symm', 'g', 'tanh', 'stabilization', 'on'}

icaOptions =  {'posact', 'off', 'sphering', 'on', 'bias', 'on', 'extended', 0};