function runMSTParallel(param_file)

if (~exist('param_file', 'var'))
    param_file = icatb_selectEntry('typeEntity', 'file', 'title', 'Select ICA parameter file', 'filter', '*ica*param*mat', 'typeselection', 'single');
end

drawnow;

load(param_file);
outputDir = sesInfo.outputDir;
ICA_Options = sesInfo.ICA_Options;
num_runs = sesInfo.mst_opts.num_ica_runs;
mask_ind = sesInfo.mask_ind;


icaAlgo = icatb_icaAlgorithm;
algoVal = sesInfo.algorithm; % algorithm index
% selected ICA algorithm
algorithmName = deblank(icaAlgo(algoVal, :));

fprintf('\n');
disp(['Number of times ICA will run is ', num2str(num_runs)]);

for nD = 1:sesInfo.numOfSub*sesInfo.numOfSess
    pcain = [sesInfo.data_reduction_mat_file, num2str(1), '-', num2str(nD), '.mat'];
    load(fullfile(outputDir, pcain), 'pcasig');
    if (nD == 1)
        data = zeros(sesInfo.numComp, length(sesInfo.mask_ind), sesInfo.numOfSub*sesInfo.numOfSess);
    end
    
    data(:, :, nD) = pcasig';
end

icasigR = cell(1, num_runs);
parfor nRun = 1:length(icasigR)
    fprintf('\n');
    disp(['Run ', num2str(nRun), ' / ', num2str(num_runs)]);
    fprintf('\n');
    [dd1, dd2, dd3, icasigR{nRun}]  = icatb_icaAlgorithm(algorithmName, data, ICA_Options);
end
clear dd1 dd2 dd3;
[corrMetric, W, A, icasig, bestRun] = icatb_bestRunSelection(icasigR, data);

mstResultsFile = fullfile(outputDir, [sesInfo.userInput.prefix, '_mst_results.mat']);
try
    if (sesInfo.write_analysis_steps_in_dirs)
        mstResultsFile = fullfile(outputDir, [sesInfo.userInput.prefix, '_ica_files', filesep, 'mst_results.mat']);
    end
catch
end

icatb_save(mstResultsFile, 'A', 'W', 'icasig', 'icasigR', 'algorithmName', 'corrMetric', 'bestRun');

clear icasigR;


icaout = [sesInfo.ica_mat_file, '.mat'];
icaout = fullfile(outputDir, icaout);

drawnow;


if (exist('skew', 'var'))
    icatb_save(icaout, 'W', 'icasig', 'mask_ind', 'skew');
else
    icatb_save(icaout, 'W', 'icasig', 'mask_ind');
end


% run from back-reconstruction till group stats
icatb_runAnalysis(sesInfo, 5:7);