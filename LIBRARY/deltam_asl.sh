#!/bin/bash

# Author: Juan Arias Aristizabel
# Updated: June 13, 2022

if [ $# -lt 3 ]
 then
	echo ""
	echo "This script performs motion correction and calls FSL's asl_file function on the motion corrected image,"
	echo "in order to subtract the label from the control perfusion image,"
	echo "and produce a delta magnetization (∆M) image on ASL data."
    echo "One subtracted image for each control-label pair is generated."
	echo "Lastly, the fslmeants function is called to produce a csv file"
	echo "containing the intensity means for each subtracted volume."
	echo "=============================="
	echo "It takes 3 arguments, assuming the images are ordered by repetitions"
	echo "Images ordered by repetitions means that for each TI there will be consecutively one control-label pair"
	echo "and then transitions to the immediate next TI until the last one,"
    echo "Then it will loop as many times as repetitions/averages were set for the image."
	echo "Do not forget to remove the m0scan if applicable."
	echo "=============================="
	echo "FIRST ARGUMENT: image file name"
	echo "SECOND ARGUMENT: number of different TIs for the entire image.  Often this is 1, unless you used a multi-PLD sequence"
	echo "THIRD ARGUMENT: provide control-label orientation, 'ct': control-label or 'tc': label-control."
	echo "example:"
	echo "$0 sub-NfL009_acq-pcasl_asl_cl.nii.gz 1 tc"
	exit 1
fi
# Enter image file name: sub-NfL009_acq-pcasl_asl_cl.nii.gz
img=$1
# Number of TIs collected
ntis=$2
# Control-Label (ct) or Label-Control (tc)
ct=$3

img_base=$(remove_ext ${img})
#==============================================================================
# Motion Correction Block:

mcflirt -in ${img_base}

#==============================================================================

# The main goal of this script, it's to capture the asl control-label magnetization subtraction and paste it into a spreadsheet. 
# By default, this script assumes that images are ordered by repetitions, see note at the end. 

#  Don't forget to remove the m0scan if applicable.
asl_file --data=${img_base}_mcf.nii.gz --ntis=${ntis} --iaf=${ct} --ibf=rpt --diff --out=${img_base}_mcf_diff
echo pair_subtraction > ${img_base}_mcf_diff.csv 
fslmeants -i ${img_base}_mcf_diff >> ${img_base}_mcf_diff.csv 


#  Note:
# Images ordered by repetitions means that for each TI there will be consecutively one control-label pair and then transitions to the immediate next TI until the last one,
# Then it will loop as many times as repetitions/averages were set for the image.