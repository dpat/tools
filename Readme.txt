Dianne Patterson, University of Arizona, SLHS Dept
=======
This directory contains scripts, profiles, lists, luts
and standard space image files used to process and manage
imaging projects.

Many (but not all) of the scripts are written to run FSL commands.
There are some matlab scripts and functions as well.
Everything was written to run under OSX, and presumably would transfer
with little pain to linux.  Any binaries have been compiled for OSX.

IMPORTANT: Variable names used in scripts should be standardized so that
all profiles use the same variable names, although the actual names of
directories and the directory structure may vary from project to
project.

=====================
Revision Control
The directory structure is under version control using git.
-------------
SourceTree (Free from App store)
bitbucket (https://bitbucket.org/dpat/tools) is also being used.
git clone https://dpat@bitbucket.org/dpat/tools.git
to create a read only copy of the tools.
-Subsequently, git push and pull should work.
======================
LIBRARY contains scripts that are general purpose.
=====================
MISC contains scripts used on other systems, temp scripts etc. (some
linux scripts here)
=====================
PROJECTS contains each project and a set of scripts related to it:
For example, Russian, dtierp, IceWord, WordBoundary1, WordBoundary2
Typically, project directories can contain a lot of scripts, so
there are some reasonable subdirectories of scripts, depending
on the project needs. Typical examples of these subdirectories are:
REG: containing scripts related to image registration
MORPH: containing scripts related to morphometric analyses.
DTI: containing scripts related to dti preprocessing and tracking
TEXT: containing scripts related to processing and reorganizing text
files,
typically to prepare them for export to MSAccess for statistical
analysis.
=======================
REF does not contain scripts. It contains useful images under IMAGES
(typically binary masks derived from standard atlases--under
STANDARD_MASKS).
lists, profiles and other kinds of configuration information
(mat files, design specifications, LUTS etc.) are also in subdirs of the
REF dir.
-------------
REF/LISTS: Contains "lists" that are referenced by the scripts
(e.g., lists of subjects or rois that I want to iterate over).
---------------
REF/PROFILES: Contains "profiles" that define global variables.
The main profile is the img_profile.  Profiles define variables for
standard directories, variables that refer to subject subdirectories or
images. Profiles  are hierarchical: All project-specific profiles also
source img_profile.sh; source subject_profile.sh.  subject_profile.sh sources img_profile.sh.
So, any project specific profile can use variables defined in both
subject_profile and img_profile.
----------------
11/18/2015: The qa scripts in the library come from http://cmroi.med.upenn.edu/qascripts/ and are documented in this paper:

Roalf, D. R., Quarmley, M., Elliott, M. A., Satterthwaite, T. D., Vandekar, S. N., Ruparel, K., et al. (2016). The impact of quality assurance assessment on diffusion tensor imaging outcomes in a large-scale population-based cohort. NeuroImage, 125, 903–919. http://doi.org/10.1016/j.neuroimage.2015.10.068
==============
2017_07_05
Created DTI_BIP directory for Aneta's files.  There are 4 categories in her hierarchy: OC=Older controls; YC=Young controls, PPA=primary progressive aphasia, STP=Stroke Patients. There are lists under REF for each of these using these labels.  They will sort together because they are all prepended with _AK_ (Aneta Kielar).
==============
2017_05_16
Backed up the whole directory in /usr/local/archive_tools on saci 
