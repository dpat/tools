#!/bin/bash

: <<COMMENTBLOCK
This code calls the docker QSIPrep tool to preprocess DWI and structural data in the BIDS data structure. 
This script requires that you are in the parent of your BIDS directory (above data and derivatives).
COMMENTBLOCK


# Exit and spit out help message if number of arguments is too small
if [ $# -lt 1 ]
  then
    echo "======================================================"
    echo "This assumes you are running docker"
    echo "and have downloaded qsiprep: docker pull pennbbl/qsiprep"
    echo "e.g. Run subject sub-304: $0 304"
    echo "e.g. Run multiple subjects: $0 304 305 306"
    echo "On the mac, ensure Docker has more than the default 2 GB of RAM:"
    echo "Docker->Preferences->Advanced"
    echo ""
    echo "run this from the project directory above data so that derivatives, data, and qsiprep_work are all siblings"
    echo "======================================================"
    exit 1
fi


# Just to be safe, create the derivatives subdirectory if it does not exist.
if [ ! -d ${PWD}/derivatives ]; then
mkdir ${PWD}/derivatives
fi

# Just to be safe, create the work directory if it does not exist.
# Apparently Docker does not care if this is under the BIDS input dir.
if [ ! -d ${PWD}/qsiprep_work ]; then
mkdir ${PWD}/qsiprep_work
fi

# Including "" around PWD ensures it will handle spaces in the path names.
# qsiprep 10 hours
docker run --rm -it -v ${HOME}/license.txt:/opt/freesurfer/license.txt:ro -v "${PWD}"/data:/data:ro -v "${PWD}"/derivatives:/out -v "${PWD}"/qsiprep_work:/work pennbbl/qsiprep /data /out participant --participant_label ${*} --output-resolution 1.3 -w /work -v -v
