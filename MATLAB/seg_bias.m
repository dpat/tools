% This is the script that accompanies the seg_bias_job.m batch script.
% See detailed description in that file.

% This is a comment: clear any existing variables
clc; clear all
% Insure SPM is ready to go
spm('defaults', 'FMRI');
% Define the data directory path containing your subject folders
dataDir = '/Volumes/Main/Working/Beeson_Data_Review/patients47';
% Define a list of subject directories (probably longer than this)
subjList={'sub-0107', 'sub-0108','sub-0123','sub-0131','sub-0137','sub-0149','sub-0150','sub-0161','sub-0167','sub-0168','sub-0175','sub-0184'}
% for each subject in subjList, put the subject in a variable called "subj"
for subj = subjList
    % display the subject (not necessary, but helpful)
    disp(strcat('Subject:   ',subj));
    % clear matlabbatch variables (we don't want any leftovers contaminating this run)
    clear matlabbatch;
    % cd into the subject directory.
    cd(char(subj));
    % call the job seg_bias_job.m
    spm_jobman('run','seg_bias_job.m')
    % When finished, go back up to the dataDir
    cd(dataDir)
end;
