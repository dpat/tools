#!/bin/bash

# Author: Dianne Patterson, Ph.D.

if [ $# -lt 3 ]
then
    echo "Usage: $0 <input file> <stats_threshold> <cluster_size>"
    echo "Example: $0 stats1 2.1 20"
    echo "This cleans a stats image to retain only clusters that meet two criteria:"
    echo "The minimum statistical value, e.g., 2.1 in the example "
    echo "the minimum cluster size, e.g., 20 in the example"
    echo ""
    echo ""
    exit 1
fi

input=$(remove_ext $1)
stats_thresh=$2
cluster_size=$3


echo "Clusters before thresholding"
# This step thresholds the statistical image and produces an osize image which contains the cluster size
# in each voxel instead of the original voxel value 
fsl-cluster -i ${input} -t ${stats_thresh} --osize=${input}_cluster_size
# This step creates a mask image thresholded by the given cluster size
fslmaths ${input}_cluster_size -thr ${cluster_size} -bin ${input}_mask -odt char
# This step multiplies the original image by the mask image we just created
fslmaths ${input} -mul ${input}_mask ${input}_stat-${stats_thresh}_clustsize-${cluster_size}
 
# Remove intermediate images
imrm ${input}_cluster_size ${input}_mask.nii.gz

echo ------------------------------------------------------------------------------
echo "See ${input}_stat-${stats_thresh}_clustsize-${cluster_size}"