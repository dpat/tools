#!/bin/bash

subnum=${1}
subj=sub-${subnum}
bids_dir=$PWD
img=`ls ${bids_dir}/${subj}/anat/${subj}*T1*.nii.gz`
img_base=`basename ${img}`
img_base_stem=`remove_ext ${img_base}`

echo "subject is ${subj}"
echo "bids_dir is ${bids_dir}"
echo "img is ${img}"
echo "img_base is ${img_base}"
echo "img_base_stem is ${img_base_stem}"
