

% These work, but only the last one specified gets output to the txt file.
% You can add another by running again, but this suggests we want a bunch of little 
% stats batch files...and then we'll call one after another.
%%%%%%%%%%%% START ONE SAMPLE T-TESTS LEARNABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% RUN 1 %%%%%%%%
% Evaluate the significance of each component for the given condition, Run1

% T-test one sample works: CUE
% averageRuns = 0;
% desCriteria = 1;
% groupInfo={'Learnable1', (1:16), (1:1); ...
% 'Learnable2', (1:16), (2:2); ...
% 'Learnable3', (1:16), (3:3)}           
% selGroups = [1];
% % cond1=cue
% selConditions = [1];

% % T-test one sample works: TONE
% averageRuns = 0;
% desCriteria = 1;
% groupInfo={'Learnable1', (1:16), (1:1); ...
% 'Learnable2', (1:16), (2:2); ...
% 'Learnable3', (1:16), (3:3)}           
% selGroups = [1];
% % cond2=tone
% selConditions = [2];

% % T-test one sample works: SENTENCE
% averageRuns = 0;
% desCriteria = 1;
% groupInfo={'Learnable1', (1:16), (1:1); ...
% 'Learnable2', (1:16), (2:2); ...
% 'Learnable3', (1:16), (3:3)}           
% selGroups = [1];
% % cond3=sentence
% selConditions = [3];

%%%%% RUN 2 %%%%%%%%%%%
% Evaluate the significance of each component for the given condition, Run2

% T-test one sample works: CUE
% averageRuns = 0;
% desCriteria = 1;
% groupInfo={'Learnable1', (1:16), (1:1); ...
% 'Learnable2', (1:16), (2:2); ...
% 'Learnable3', (1:16), (3:3)}           
% selGroups = [2];
% % cond1=cue
% selConditions = [1];

% % T-test one sample works: TONE
% averageRuns = 0;
% desCriteria = 1;
% groupInfo={'Learnable1', (1:16), (1:1); ...
% 'Learnable2', (1:16), (2:2); ...
% 'Learnable3', (1:16), (3:3)}           
% selGroups = [2];
% % cond2=tone
% selConditions = [2];

% % T-test one sample works: SENTENCE
% averageRuns = 0;
% desCriteria = 1;
% groupInfo={'Learnable1', (1:16), (1:1); ...
% 'Learnable2', (1:16), (2:2); ...
% 'Learnable3', (1:16), (3:3)}           
% selGroups = [2];
% % cond3=sentence
% selConditions = [3];

%%%%% RUN 3 %%%%%%%%%%%
% Evaluate the significance of each component for the given condition, Run3

% T-test one sample works: CUE
% averageRuns = 0;
% desCriteria = 1;
% groupInfo={'Learnable1', (1:16), (1:1); ...
% 'Learnable2', (1:16), (2:2); ...
% 'Learnable3', (1:16), (3:3)}           
% selGroups = [3];
% % cond1=cue
% selConditions = [1];

% % T-test one sample works: TONE
% averageRuns = 0;
% desCriteria = 1;
% groupInfo={'Learnable1', (1:16), (1:1); ...
% 'Learnable2', (1:16), (2:2); ...
% 'Learnable3', (1:16), (3:3)}           
% selGroups = [3];
% % cond2=tone
% selConditions = [2];

% % T-test one sample works: SENTENCE
% averageRuns = 0;
% desCriteria = 1;
% groupInfo={'Learnable1', (1:16), (1:1); ...
% 'Learnable2', (1:16), (2:2); ...
% 'Learnable3', (1:16), (3:3)}           
% selGroups = [3];
% % cond3=sentence
% selConditions = [3];

%%%%%% END ONE SAMPLE T-TESTS LEARNABLE %%%%%%%%
%%%%%%%%%%%% START ONE SAMPLE T-TESTS UNLEARNABLE %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% RUN 1 %%%%%%%%
% Evaluate the significance of each component for the given condition, Run1

% T-test one sample works: CUE
% averageRuns = 0;
% desCriteria = 1;
% groupInfo={'Unlearnable1', (1:16), (1:1); ...
% 'Unlearnable2', (1:16), (2:2); ...
% 'Unearnable3', (1:16), (3:3)}           
% selGroups = [1];
% % cond1=cue
% selConditions = [1];

% % T-test one sample works: TONE
% averageRuns = 0;
% desCriteria = 1;
% groupInfo={'Unlearnable1', (1:16), (1:1); ...
% 'Unlearnable2', (1:16), (2:2); ...
% 'Unlearnable3', (1:16), (3:3)}           
% selGroups = [1];
% % cond2=tone
% selConditions = [2];

% % T-test one sample works: SENTENCE
% averageRuns = 0;
% desCriteria = 1;
% groupInfo={'Unlearnable1', (1:16), (1:1); ...
% 'Unlearnable2', (1:16), (2:2); ...
% 'Unlearnable3', (1:16), (3:3)}           
% selGroups = [1];
% % cond3=sentence
% selConditions = [3];

%%%%% RUN 2 %%%%%%%%%%%
% Evaluate the significance of each component for the given condition, Run2

% T-test one sample works: CUE
% averageRuns = 0;
% desCriteria = 1;
% groupInfo={'Unlearnable1', (1:16), (1:1); ...
% 'Unlearnable2', (1:16), (2:2); ...
% 'Unlearnable3', (1:16), (3:3)}           
% selGroups = [2];
% % cond1=cue
% selConditions = [1];

% % T-test one sample works: TONE
% averageRuns = 0;
% desCriteria = 1;
% groupInfo={'Unlearnable1', (1:16), (1:1); ...
% 'Unlearnable2', (1:16), (2:2); ...
% 'Unlearnable3', (1:16), (3:3)}           
% selGroups = [2];
% % cond2=tone
% selConditions = [2];

% % T-test one sample works: SENTENCE
% averageRuns = 0;
% desCriteria = 1;
% groupInfo={'Unlearnable1', (1:16), (1:1); ...
% 'Unlearnable2', (1:16), (2:2); ...
% 'Unlearnable3', (1:16), (3:3)}           
% selGroups = [2];
% % cond3=sentence
% selConditions = [3];

%%%%% RUN 3 %%%%%%%%%%%
% Evaluate the significance of each component for the given condition, Run3

% T-test one sample works: CUE
% averageRuns = 0;
% desCriteria = 1;
% groupInfo={'Unlearnable1', (1:16), (1:1); ...
% 'Unlearnable2', (1:16), (2:2); ...
% 'Unlearnable3', (1:16), (3:3)}           
% selGroups = [3];
% % cond1=cue
% selConditions = [1];

% % T-test one sample works: TONE
% averageRuns = 0;
% desCriteria = 1;
% groupInfo={'Unlearnable1', (1:16), (1:1); ...
% 'Unlearnable2', (1:16), (2:2); ...
% 'Unlearnable3', (1:16), (3:3)}           
% selGroups = [3];
% % cond2=tone
% selConditions = [2];

% % T-test one sample works: SENTENCE
averageRuns = 0;
desCriteria = 1;
groupInfo={'Unlearnable1', (1:16), (1:1); ...
'Unlearnable2', (1:16), (2:2); ...
'Unlearnable3', (1:16), (3:3)}           
selGroups = [3];
% cond3=sentence
selConditions = [3];

%%%%%% END ONE SAMPLE T-TESTS %%%%%%%%

%%%%%% START 2 SAMPLE T-TESTS %%%%%%%%
% Compare Run1 and Run3 T-test: SENTENCE
% averageRuns = 0;
% desCriteria = 2;
% groupInfo={'Learnable1', (1:16), (1:1); ...
% 'Learnable2', (1:16), (2:2); ...
% 'Learnable3', (1:16), (3:3)}           
% % Selected groups
% selGroups = [1 3];
% % cond3=sentence
% selConditions = [3];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%% START 2 SAMPLE T-TESTS %%%%%%%%
% Compare Run1 and Run3 T-test: SENTENCE
% averageRuns = 0;
% desCriteria = 2;
% groupInfo={'Learnable1', (1:16), (1:1); ...
% 'Learnable2', (1:16), (2:2); ...
% 'Learnable3', (1:16), (3:3)}           
% % Selected groups
% selGroups = [1 3];
% % cond3=sentence
% selConditions = [3];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%% START MULTIPLE REGRESSION %%%%%%%%%%%%%%%%
% Correlate dprime for Run1 to Run1
% averageRuns = 0;
% desCriteria = 6;

% groupInfo={'Learnable1', (1:16), (1:1); ...
% 'Learnable2', (1:16), (2:2); ...
% 'Learnable3', (1:16), (3:3)}           
% selGroups = [1];

% % Selected conditions
% % cond3=sentence
% selConditions = [3];

% multi_regress_files = {'/usr/local/tools/REF/FUNC/dprime_learnable_wb1_run1.txt'}

%%%%%%%%%%%%% END MULTIPLE REGRESSION %%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%% START OF ONE WAY ANOVA (GROUPS)  %%%%%%%%%%%%%%%%%%%%%%%%%
% Anova (this works!!) Crucially I have to be in the dir with the txt file or specify the full path to the txt file
% averageRuns = 0;
% desCriteria = 3;

% groupInfo={'Learnable1', (1:16), (1:1); ...
% 'Learnable2', (1:16), (2:2); ...
% 'Learnable3', (1:16), (3:3)}             

% selGroups = [1 3];

% selConditions = [2 3];

% eq_regressors = [1 -1]; 

%%%%%%%% END OF ONE WAY ANOVA (GROUPS) %%%%%%%%%%%%%%


%%%%%%%%% START OF ONE WAY ANOVA (REGRESSORS) %%%%%%%%%%%%%%%%%%%%%%%%%
%%%% This allows comparison of multiple conditions (i.e., "regressors") %%%%%%
%%% Seems more capable than option 3, but clearly I don't get anova %%%%%%
% averageRuns = 0;
% desCriteria = 4;

% groupInfo={'Learnable1', (1:16), (1:1); ...
% 'Learnable2', (1:16), (2:2); ...
% 'Learnable3', (1:16), (3:3)}             

% selGroups = [1 3];

% selConditions = [2 3];

% eq_regressors = [1 -1]; 
%%%%%%%%% END OF ONE WAY ANOVA (REGRESSORS) %%%%%%%%%%%%%%%%%%%%%%%%%




