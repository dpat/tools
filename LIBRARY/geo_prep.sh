#!/bin/sh

# Written by Dianne Patterson, Ph.D.
# 9/13/2014
# The following script is released in accordance with the Creative Commons Attribution-NonCommercial
# Unported License: http://creativecommons.org/licenses/by-nc/4.0/legalcode

###############################################################################
############################  DEFINE FUNCTIONS  ##################################

#==============================================================================

: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Print relevant help message

COMMENTBLOCK

function HelpMessage
{
    echo "$0 runs several separate functions."
    echo "Typically Reslice1mmx0.5mmAtlas would be run first to create the"
    echo "atlas in 0.5mm space."
    echo "after that Split and StripXML are necessary to prepare the files for Matlab"
    echo "In matlab, we run geo_wrap (or some version of it) which iteratively runs nifti2geojson."
    echo "Finally, we can run MkUniqueRegionList to get the unique region labels out of the geojson files."
    echo "These unique regions will be used as the skeleton of the data table for Weave."
    echo "geo_prep.sh MkUniqueRegionList"
    echo " print a lists of all the unique regions in the geojson files"
    echo ""
    echo "geo_prep.sh Reslice1mmx0.5mmAtlas HO_all_1mm"
    echo "Reslice a 3d label atlas from 1mm to 0.5mm "
    echo ""
    echo "geo_prep.sh Split HO_all_0.5"
    echo "split a 3d atlas into slices along each dimension"
    echo ""
    echo "geo_prep.sh StripXML HarvardOxford-Cortical.xml"
    echo "extract the label text files from XML"
    echo ""
    echo "================================ "
}

#==============================================================================
: <<COMMENTBLOCK

Function:    MkUniqueRegionList
Purpose:     Create GeoJson directory and populate it.
			Scour the geojson files to identify each unique region
		 	This will include a regionId and a side (L, R or Med)
Input:      	ax, cor and sag directories containing geojson files
			(this requires you run geo_wrap.m which calls nifti2geojson)
Output:      A GeoJson directory containing only the geojson files
			and a sorted list of all the unique regions in the atlas:
			UniqueRegionList.txt

COMMENTBLOCK

function MkUniqueRegionList
{

# Get the geojson files into their own directories
mkdir GeoJson
cp ax/*.geojson GeoJson
cp cor/*.geojson GeoJson
cp sag/*.geojson GeoJson

# Get all of the regionIdLabels
cd GeoJson
touch regId.txt regName.txt regLongName.txt

for file  in *.geojson ; do
  	cat  ${file} | grep "regionIdLabel" | awk '{print $2}'  >> regId.txt
done

for file  in *.geojson ; do
  	cat ${file} | grep "regionName" | awk '{print $2}' >> regName.txt
done

for file  in *.geojson ; do
  	cat ${file} | grep "regionLongName" | awk '{print $2}' >> regLongName.txt
done

paste -d ,  regName.txt regLongName.txt regId.txt > reg.txt
cat reg.txt | sed 's/",//g'  | sed 's/"//g'  | sort -u -f > ../UniqueRegionList.txt

rm reg.txt regId.txt regName.txt regLongName.txt

}

#==============================================================================

: <<COMMENTBLOCK

Function:   MyLog
Purpose:    log the activity of a script
Input:      None 
Output:     Entry in log.txt

COMMENTBLOCK

function MyLog
{

if [ $# -lt 1 ]
        then
        touch log.txt; echo "For `pwd` on `date`: $0 $1 $2 $3 $4 $5 $6 $7 $8 $9" >>log.txt
        elif  [ $# -eq 1 ]; then
        touch log.txt; echo "For `pwd` on `date`: $0 $1 " >>log.txt
    fi

}


#==============================================================================
: <<COMMENTBLOCK

Function:   RemoveZero
Purpose:    Remove zero padding from filenames
Input:        None.  Works on current dir
Output:     Filenames changed

COMMENTBLOCK

function RemoveZero
{

for im in *.nii.gz; do
	oldstem=`basename ${im} .nii.gz`  # return file basename
	# echo "oldstem=${oldstem}"
	newstem=`echo ${oldstem} | sed 's/_0/_/' | sed 's/_00/_/' | sed 's/_000/_/' | sed 's/_0/_/'`
	# echo "newstem=${newstem}"
	mv ${oldstem}.nii.gz ${newstem}.nii.gz
done

}

#==============================================================================
: <<COMMENTBLOCK

Function:   Reslice1mmx05mmAtlas
Purpose:    Reslice a 3D labeled atlas from 1mm to 0.5 mm space
Input:        1) a 1mm 3d labelled atlas file, e.g. HO_all_1mm
			2) MNI152_T1_0.5mm_brain.nii.gz (0.5mm standard space MNI image)
			3) stand1stand0.5.mat (registration matrix)
Output:     A 0.5 mm atlas file with 0.5mm appended to the name

COMMENTBLOCK

function Reslice1mmx05mmAtlas
{

if [ $# -eq 1 ]              						# If there is one argument
    then
        atlas=$1                           			# that argument is the atlas
    else
        atlas=$atlas                         			# else the atlas is passed in
 fi

# reslice using nearest neighbour interpolation and int data type so as not to
# create fractional values in the resulting atlas.
   flirt -in ${atlas} -datatype int -ref MNI152_T1_0.5mm_brain  -interp nearestneighbour \
 -init stand1stand0.5.mat -applyxfm -out ${atlas}_0.5mm

}

#==============================================================================
: <<COMMENTBLOCK

Function:   Split
Purpose:    To spilt a 3d atlas file into slices
Input:        a 3D atlas file to split
			e.g., 1) HarvardOxford-cort-maxprob-thr25-2mm
Output:     a set of correctly named unzipped slices in each of 3 subdirectories

COMMENTBLOCK

function Split
{

if [ $# -eq 1 ]              						# If there is one argument
    then
        atlas=$1                           			# that argument is the atlas
    else
        atlas=$atlas                         			# else the atlas is passed in
 fi

echo $atlas
mkdir ax cor sag  # make subdirectories

# Populate each subdirectory with slices
fslsplit ${atlas} ax/ax_ -z
fslsplit ${atlas} sag/sag_ -x
fslsplit ${atlas} cor/cor_ -y

# rename files to remove zero padding
# and unzip the files.
 cd ax
 RemoveZero
 gunzip *
 rm ax_.nii
 cd ../cor
 RemoveZero
 gunzip *
 rm cor_.nii
 cd ../sag
 RemoveZero
gunzip *
rm sag_.nii
 cd ..

}


#==============================================================================
: <<COMMENTBLOCK

Function:   StripXML
Purpose:    Create label text files from the names in an FSL atlas xml specification.
Input:     	xml file
Output:      An ordered list of short region names (labels_short.txt), with "regions" at the top of the list
			An ordered list of long region names (labels_long.txt) with regions at the top of the list
			An ordered csv (labels.txt) containing short and long names for each region.

COMMENTBLOCK

function StripXML
{

if [ $# -eq 1 ]              						# If there is one argument
    then
        xml=$1                           			# that argument is the atlas
    else
        xml=$xml                        			# else the atlas is passed in
 fi

echo ${xml}
 fullname=`cat ${xml} | grep index= | awk -F\> '{print $2}' | sed 's/<\/label//' | sed 's/ /_/g' | sed 's/,//'`

 shortname=`cat ${xml} | grep index= | awk -F\> '{print $2}' | sed 's/<\/label//' | sed 's/ /_/g' | sed 's/,//' | \
 sed 's/[_ ]$//g' | sed "s/\'//g" | sed 's/__ /_/g' | sed 's/-/_/g' | \
 sed 's/anterior_division/ant/g' | sed 's/posterior_division/post/g' | \
 sed 's/superior_division/sup/g' | sed 's/inferior_division/inf/g' | \
 sed 's/Lateral/Lat/g' | sed 's/Medial/Med/g' | sed 's/Central/Cen/g' | \
 sed 's/Superior_Frontal_Gyrus/SFG/g' | sed 's/Middle_Frontal_Gyrus/MFG/g' | sed 's/Inferior_Frontal_Gyrus/IFG/g' | \
 sed 's/Superior_Temporal_Gyrus/STG/g' | sed 's/Middle_Temporal_Gyrus/MTG/g' | sed 's/Inferior_Temporal_Gyrus/ITG/g' | \
 sed 's/Angular_Gyrus/AnG/g' | sed 's/Supramarginal_Gyrus/SMG/g' | \
 sed 's/Parahippocampal_Gyrus/PhG/g' | sed 's/Insular_Cortex/Ins/g' | \
 sed 's/Juxtapositional_Lobule_Cortex/SMA/g' | sed 's/Frontal_Pole/FRP/g' | sed 's/(formerly_Supplementary_Motor_Cortex)//g' |\
 sed 's/SMA_/SMA/g' | sed 's/IFG_pars_triangularis/BA45/g' | sed 's/IFG_pars_opercularis/BA44/g' | \
 sed 's/Precentral_Gyrus/PrG/g' | sed 's/Temporal_Pole/tmp/g' | \
 sed 's/temporooccipital_part/tempocc/g' | sed 's/Postcentral_Gyrus/PoG/g' | \
 sed 's/Superior_Parietal_Lobule/SPL/g' | sed 's/Occipital_Cortex/OL/g' | \
 sed 's/Subcallosal_Cortex/SCA/g' | sed 's/Cingulate_Gyrus/CgG/g' | \
 sed 's/Precuneous_Cortex/PCu/g' | sed 's/Cuneal_Cortex/Cun/g' | \
 sed 's/Frontal_Orbital_Cortex/OFC/g' | sed 's/Fusiform_Cortex/FuG/g' | sed 's/Fusiform_Gyrus/FuG/g' | \
 sed 's/Operculum_Cortex/Op/g' | sed 's/Opercular_Cortex/Op/g' | \
 sed 's/Planum_Polare/PP/g' | sed 's/Planum_Temporale/PT/g' | sed 's/Heschls_Gyrus/HG/g' |  sed 's/(includes_H1_and_H2)//g' | \
 sed 's/HG_/HG/g' | sed 's/Supracalcarine_Cortex/SCLC/g' | sed 's/Occipital_Pole/OCP/g' | sed 's/Occipital/OL/g' | \
 sed 's/Lingual_Gyrus/LiG/g' | sed 's/Ventrical/Ventricle/g' | \
 sed 's/Paracingulate_Gyrus/PAC/g' | sed 's/Intracalcarine_Cortex/CALC/g' | \
 sed 's/Temporal/TL/g' | sed 's/Frontal/FL/g' | sed 's/Parietal/PL/g' | \
 sed 's/Cortex/GM/g' | sed 's/White_Matter/WM/g' | \
 sed 's/Left_//g' | sed 's/Right_//g' | \
 sed 's/Thalamus/Th/g' | sed 's/Caudate/Cd/g' | \
 sed 's/Putamen/Pu/g' | sed 's/Pallidum/GP/g' | \
 sed 's/Amygdala/Amg/g' | sed 's/Hippocampus/Hi/g' | \
 sed 's/Accumbens/Acb/g' | sed 's/Right_Accumbens/Acb/g' `

touch labels_short.txt labels_long.txt
echo "regionLongName" >> labels_long.txt
 echo "${fullname}" >>labels_long.txt
 echo "regionName" >> labels_short.txt
 echo "${shortname}" >>labels_short.txt
paste -d , labels_short.txt labels_long.txt > labels.txt

}

#==============================================================================

while getopts h options 2> /dev/null; do        # Setup -h flag. redirect errors
    case $options in                            			# In case someone invokes -h
    h) HelpMessage;;                            			# Run the help message function
    \?) echo "only h is a valid flag" 1>&2      	# If bad options are passed, print message
    esac
done

shift $(( $OPTIND -1))                          		# Index option count, so h isn't treated as arg
optnum=$(( $OPTIND -1))                        		# optnums hold # of options passed in

if [ ${optnum} -lt 1 -a $# -lt 1 ]              		# If no options or args
    then
        dothis=HelpMessage                            			# Default to running Main
    else
        dothis=$1                          				# Run the function as indicated
        echo "${dothis}"
         # if the function takes an argument, set the argument here so we can pass it in at the commandline
         if [ ${dothis} = "Split" ] || [  ${dothis}="Reslice1mmx05mmAtlas" ]; then
            atlas=$2
         fi

         if  [ ${dothis} = "StripXML" ]; then
         	xml=$2
         fi
fi

$dothis

MyLog
