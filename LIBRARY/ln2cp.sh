#!/bin/bash

: <<COMMENTBLOCK
Change linked files from bip setup and prep to be copies so globus doesn't mess things up. Run this from the bids directory.
Note, you can rerun it, if it finds the links, it'll make copies instead. If it finds the file, it'll tell you everything is okay.
COMMENTBLOCK

if [ $# -lt 2 ] ; then
  echo "Usage: $0 <subject> <output_dir>"
  echo "e.g. ln2cp.sh sub-327 derivatives"
  exit 1 ;
fi

subject=$1
output_dir=$2 #this is the base output dir, might be derivatives or derivatives/bip for example.

anat=${PWD}/${output_dir}/${subject}/anat
anat_sub=${anat}/${subject}_T1w.anat
reg=${PWD}/${output_dir}/${subject}/reg

echo "==========================="
echo "subject is ${subject}"

# If you have run setup, then you'll want to replace the T1 file
if [ -h ${anat_sub}/T1.nii.gz ]; then
  echo "The link ${subject}_T1w.anat/T1.nii.gz exists, replacing"
  echo   echo "---------------------------------------------------"
  rm ${anat_sub}/T1.nii.gz
  cp ${anat_sub}/T1_biascorr.nii.gz ${anat_sub}/T1.nii.gz
elif [ -e ${anat_sub}/T1.nii.gz ]; then
  echo "Congratulations! The T1.nii.gz link has already been converted to a file"
fi

# If you have run prep, you'll need to replace several more links:

if [ -h ${anat}/${subject}_T1w_csfseg.nii.gz ]; then
  echo "The link anat/${subject}_T1w_csfseg.nii.gz exists, replacing"
  echo   echo "---------------------------------------------------"
  rm ${anat}/${subject}_T1w_csfseg.nii.gz
  cp ${anat_sub}/T1_fast_pve_0.nii.gz ${anat}/${subject}_T1w_csfseg.nii.gz
elif [ -e ${anat}/${subject}_T1w_csfseg.nii.gz ]; then
  echo "Congratulations! The csf mask link has already been converted to a file"
fi

if [ -h ${anat}/${subject}_T1w_gmseg.nii.gz ]; then
  echo "The link anat/${subject}_T1w_gmseg.nii.gz exists, replacing"
  echo "---------------------------------------------------"
  rm ${anat}/${subject}_T1w_gmseg.nii.gz
  cp ${anat_sub}/T1_fast_pve_1.nii.gz ${anat}/${subject}_T1w_gmseg.nii.gz
elif [ -e ${anat}/${subject}_T1w_gmseg.nii.gz ]; then
  echo "Congratulations! The gm mask link has already been converted to a file"
fi

if [ -h ${anat}/${subject}_T1w_wmseg.nii.gz ]; then
  echo "The link anat/${subject}_T1w_wmseg.nii.gz exists, replacing"
  echo "---------------------------------------------------"
  rm ${anat}/${subject}_T1w_wmseg.nii.gz
  cp ${anat_sub}/T1_fast_pve_2.nii.gz ${anat}/${subject}_T1w_wmseg.nii.gz
elif [ -e ${anat}/${subject}_T1w_wmseg.nii.gz ]; then
  echo "Congratulations! The wm mask link has already been converted to a file"
fi
###############
if [ -h ${reg}/${subject}_mni_str_warp.nii.gz ]; then
  echo "The link reg/${subject}_mni_str_warp.nii.gz exists, replacing"
  echo "---------------------------------------------------"
  rm ${reg}/${subject}_mni_str_warp.nii.gz
  cp ${anat_sub}/MNI_to_T1_nonlin_field.nii.gz ${reg}/${subject}_mni_str_warp.nii.gz
elif [ -e ${reg}/${subject}_mni_str_warp.nii.gz ]; then
  echo "Congratulations! The mni2struct warp link has already been converted to a file"
fi

if [ -h ${reg}/${subject}_str_mni_warp.nii.gz ]; then
  echo "The link reg/${subject}_str_mni_warp.nii.gz exists, replacing"
  echo "---------------------------------------------------"
  rm ${reg}/${subject}_str_mni_warp.nii.gz
  cp ${anat_sub}/T1_to_MNI_nonlin_field.nii.gz ${reg}/${subject}_str_mni_warp.nii.gz
elif [ -e ${reg}/${subject}_str_mni_warp.nii.gz ]; then
  echo "Congratulations! The struct2mni warp link has already been converted to a file"
fi
echo "===================================================="
