
% This script operates on the datasets in varStemArray
% and creates a new dataset split on the members of varSuffixCat available
% in varSuffixArray
% sample input to create subsets:
% varStemArray={'lr_endpoints_A', 'lr_endpoints_B' }; 
% varSuffixCat={'tract'}
% varSuffixArray={'arc','aslant','ec', 'ilf', 'iof', 'mdlf', 'slf2', 'unc', 'vof', 'wernickes_motor' };

%The dataset stems
for ivarStem = (varStemArray);
%The dataset suffixes
    for ivarSuffix=(varSuffixArray); 
    % Concatenate stem and suffix to create the dataset names
        var=char(strcat((ivarStem), '_',(ivarSuffix)));
        myCat=char(strcat((ivarStem), '.',(varSuffixCat)));
        % Create a string that can be evaluated.  Use '' to create a
        % literal '
        evalstring=char(strcat((var),'=',(ivarStem),'(',(myCat),'==''',(ivarSuffix),''',:);'));
        eval(evalstring);
    end
end

clear evalstring myCat var
