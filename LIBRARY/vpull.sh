#!/bin/bash

# Designed to facilitate syncing operations
# Written by Dianne Patterson, University of Arizona 1/23/2015


if [ $# -lt 1 ]
then
    echo "Usage: $0 <volume> [r|d|rd]"
    echo "Example: $0  XFER r"
    echo "r=recursive, d=delete extraneous destination files and directories"
    echo "rd=recursive and delete" 
    echo "Pull stuff FROM the volume (directory with same name as cwd) TO current working directory"  
    echo "Warning: This will not update subdirectories unless you use r (recursive)"
    echo "Warning: This will not remove extraneous files or directories on the destination unless you use d" 
    echo ""  
    exit 1
fi

sourcedir=`pwd`
dir=`basename ${sourcedir}`
volume=$1
machine=feckless
flags=$2

if [ "${flags}" = "r" ]
then 
    thisflag="-r"
    elif [ "${flags}" = "d" ]
    then
        thisflag="--delete"
    elif [ "${flags}" = "rd" ]
    then
        thisflag="--delete -r"
    elif [ "${flags}" = "dr" ]
    then
        thisflag="--delete -r"
fi
echo "syncing  to /Volumes/${volume}/${dir} to ${dir}  with flags=${flags}"
echo "continue? yes/no"
read answer

if [ "$answer" = "yes" ]; then
     rsync -vtdpulgopzE ${thisflag}  /Volumes/${volume}/${dir}/ .
else
    echo "backing out"
    exit 1
fi 
