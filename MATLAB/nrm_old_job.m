%-----------------------------------------------------------------------
% Job saved on 18-Aug-2018 15:51:31 by cfg_util (rev $Rev: 6460 $)
% spm SPM - SPM12 (6906)
% cfg_basicio BasicIO - Unknown
% This is a batch script implementing normalization in the old SPM8 style.
% It accompanies nrm_old.m.
% We use the lesion mask to weight the image before normalization and
% we use the extracted brain.
% See https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/spm_anat_normalization-lesion-old.html
%-----------------------------------------------------------------------
% Provide the names of the files we need
T1='T1w.nii'
T1_brain='T1w_brain.nii'
T1_brain_mask='T1w_brain_mask.nii'
lesion='lesionmask.nii'
lesioninv='lesionmaskinv.nii'

matlabbatch{1}.spm.tools.oldnorm.estwrite.subj.source = {T1};
matlabbatch{1}.spm.tools.oldnorm.estwrite.subj.wtsrc = {lesioninv};
matlabbatch{1}.spm.tools.oldnorm.estwrite.subj.resample = {
                                                           lesion,
                                                           T1_brain_mask
                                                           };
matlabbatch{1}.spm.tools.oldnorm.estwrite.eoptions.template = {'/MATLAB_TOOLS/spm12/toolbox/OldNorm/T1.nii,1'};
matlabbatch{1}.spm.tools.oldnorm.estwrite.eoptions.weight = '';
matlabbatch{1}.spm.tools.oldnorm.estwrite.eoptions.smosrc = 8;
matlabbatch{1}.spm.tools.oldnorm.estwrite.eoptions.smoref = 0;
matlabbatch{1}.spm.tools.oldnorm.estwrite.eoptions.regtype = 'mni';
matlabbatch{1}.spm.tools.oldnorm.estwrite.eoptions.cutoff = 25;
matlabbatch{1}.spm.tools.oldnorm.estwrite.eoptions.nits = 16;
matlabbatch{1}.spm.tools.oldnorm.estwrite.eoptions.reg = 1;
matlabbatch{1}.spm.tools.oldnorm.estwrite.roptions.preserve = 0;
matlabbatch{1}.spm.tools.oldnorm.estwrite.roptions.bb = [-78 -112 -70
                                                         78 76 85];
matlabbatch{1}.spm.tools.oldnorm.estwrite.roptions.vox = [2 2 2];
matlabbatch{1}.spm.tools.oldnorm.estwrite.roptions.interp = 0;
matlabbatch{1}.spm.tools.oldnorm.estwrite.roptions.wrap = [0 0 0];
matlabbatch{1}.spm.tools.oldnorm.estwrite.roptions.prefix = 'w';
matlabbatch{2}.spm.tools.oldnorm.write.subj.matname(1) = cfg_dep('Old Normalise: Estimate & Write: Norm Params File (Subj 1)', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('()',{1}, '.','params'));
matlabbatch{2}.spm.tools.oldnorm.write.subj.resample = {
                                                        T1,
                                                        T1_brain
                                                        };
matlabbatch{2}.spm.tools.oldnorm.write.roptions.preserve = 0;
matlabbatch{2}.spm.tools.oldnorm.write.roptions.bb = [-78 -112 -70
                                                      78 76 85];
matlabbatch{2}.spm.tools.oldnorm.write.roptions.vox = [2 2 2];
matlabbatch{2}.spm.tools.oldnorm.write.roptions.interp = 1;
matlabbatch{2}.spm.tools.oldnorm.write.roptions.wrap = [0 0 0];
matlabbatch{2}.spm.tools.oldnorm.write.roptions.prefix = 'w';
