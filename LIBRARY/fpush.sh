#!/bin/bash

# Designed to facilitate syncing operations
# Written by Dianne Patterson, University of Arizona 2/1/2011

# paths for imaging and subject directories.  
# To be used when operating inside a subject directory

if [ $# -lt 1 ]
then
    echo "Usage: $0 <dir> [r|d|rd]"
    echo "Example: $0 DTI r"
    echo "r=recursive, d=delete extraneous destination files and directories"
    echo "rd=recursive and delete" 
    echo "Push stuff FROM the local smachine TO feckless"  
    echo "Local is whatever you are logged in to when you start the process."  
    echo "Warning: This will not update subdirectories unless you use r (recursive)"
    echo "Warning: This will not remove extraneous files or directories unless you use d" 
    echo ""  
    exit 1
fi

cd $1
# the nice thing about using pwd, is that you can specify a relative path, or even use "."
# and the script will behave as if you have given it an absolute path.
sourcedir=`pwd`
machine=feckless
flags=$2

if [ "${flags}" = "r" ]
then 
    thisflag="-r"
    elif [ "${flags}" = "d" ]
    then
        thisflag="--delete"
    elif [ "${flags}" = "rd" ]
    then
        thisflag="--delete -r"
    elif [ "${flags}" = "dr" ]
    then
        thisflag="--delete -r"
fi
echo "syncing ${sourcedir} to ${machine}:/Volumes/Main${sourcedir}  with flags=${flags}"

echo "continue? yes/no"
read answer

if [ "$answer" = "yes" ]; then
     rsync -vtdpulgopzE ${thisflag} -e ssh ${sourcedir}/ ${machine}:/Volumes/Main${sourcedir} 
else
    echo "backing out"
    exit 1
fi 
