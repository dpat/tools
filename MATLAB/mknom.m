for idata =(datasetArray);
    % set nominal variables shared by all datasets
    for ifield = (fieldArray);
        field=char(strcat((idata), '.', (ifield))); % converts cells to strings, so sprintf can use them
        sprintf('%s=nominal(%s);', field, field); % construct the command in a string "ans"
        eval(ans); % Run the command in ans
    end
end
clear idata ifield field ans

% {'subject', 'sex', 'condition', 'DominantHand', 'tract'};
% % Set remaining nominal variables
% dprime_tracts.R_L=nominal(dprime_tracts.R_L);
% dprime_endpoints.R_L=nominal(dprime_endpoints.R_L);
% dprime_endpoints.roi=nominal(dprime_endpoints.roi);
% dprime_endpoint_lat.roi=nominal(dprime_endpoint_lat.roi);
% dprime_endpoints.AB=nominal(dprime_endpoints.AB);
% dprime_endpoint_lat.AB=nominal(dprime_endpoint_lat.AB);

% rename ans (in the function ws) to the string in var in the base ws
%        assignin('base',(var), ans);

% Import 2nd variable from the base workspace with the 
            % concatenated name.
          %  j=evalin('base',corrVar2); 