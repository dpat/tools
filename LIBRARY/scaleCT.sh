#!/bin/bash

: <<COMMENTBLOCK
This code sets the background to 0 and rescales the bone in a CT image.
Rescaled images are usable for simple structural lesion mapping.
COMMENTBLOCK


# Exit if number of arguments is too small
if [ $# -lt 1 ]
    then
        echo "Please enter the name of a CT image in NIFTI format that needs to be rescaled"
        echo "e.g., $0 anat_CT"
        exit 1
fi

# get the input stem
input=`remove_ext ${1}`
#threshold at 0 to get rid of negative values outside the head in CT
fslmaths ${input} -thr 0 ${input}_thr
# get mean of non-zero voxels
mean=`fslstats ${input}_thr -M`
echo " mean is: ${mean}"
# create a skull image by removing all voxels less than the mean
fslmaths ${input}_thr -thr ${mean} ${input}_skull -odt short
# divide that skull image by 10 to reduce its intensity (maybe another value would be better?)
fslmaths ${input}_skull -div 10 ${input}_skull_rev -odt short
# subtract the original skull image from the 0 thresholded image
fslmaths ${input}_thr -sub ${input}_skull ${input}_thr_sub -odt short
# Add the revised skull image back into the 0 thresholded image. Make the output 16 bit
fslmaths ${input}_thr_sub -add ${input}_skull_rev ${input}_scaled -odt short
