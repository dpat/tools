#!/bin/bash

# Define paths and variables
# source img_profile.sh; source subject_profile.sh

: <<COMMENTBLOCK
Pass in t-thresh and cluster size for cleanup. Start with the T-images for the ICs
of interest in a single dir named Net. In our case 15 images for 5 ICs and 3 sessions.
/Exps/Analysis/WB1/Net
s*_IC*


Parameters needed by the scripts:
1) a minimum acceptable cluster size: e.g., 274 for WB1

    Afni documentation recommends 3dClustSim:

    3dClustSim -nxyz 91 109 91 -dxyz 2 2 2 -niter 2000 -pthr 0.01 -fwhm 7
    # 3dClustSim -nxyz 91 109 91 -dxyz 2 2 2 -niter 1000 -pthr 0.01 -fwhm 7
    # Grid: 91x109x91 2.00x2.00x2.00 mm^3 (902629 voxels)
    #
    # CLUSTER SIZE THRESHOLD(pthr,alpha) in Voxels
    # -NN 1  | alpha = Prob(Cluster >= given size)
    #  pthr  |  0.100  0.050  0.020  0.010
    # ------ | ------ ------ ------ ------
     0.010000   209.2  230.0  251.0  274.0

     274 is min cluster size at 0.01

     voxel size 2x2x2
     matrix size 91x109x91


2) a t-stat threshold: e.g., 2.6026  for WB1
(one-tailed?). n-1=15 for wb1

    This is based on the talairach table work and the spm tool Srinivas
recommended:

     t_thresh=spm_u(0.05, [1,15],'T') % WB1  1.7531 (p=0.05) 
     t_thresh=spm_u(0.01, [1,15],'T') % WB1  2.6026 (p=0.01) 
     t_thresh=spm_u(0.05,[1,13],'T')  % Iceword 1.7709 (p=0.05)
     t_thresh=spm_u(0.01,[1,13],'T') % Iceword 2.6503 (p=0.01)
    (see emails ~Sept 12, 2012)

COMMENTBLOCK

#==============================================================================
: <<COMMENTBLOCK

Function:   Add100Crop
Purpose:    Add 100 to eachIC image and crop it with the master mask
Input:        A directory containing all the images to work on
Output:      A set of *_100_mask images that are all exactly the same size as
      the master image...ready for export to Matlab


COMMENTBLOCK

function Add100Crop
{

 img=$1 # pass in regex basename of images to work on

for im in ${img}.nii.gz; do
   echo "Add100Crop will work on ${im}"
   # define basename
   im_base=`basename -s .nii.gz ${im}`
   # Add 100 to every voxel
   fslmaths ${im_base} -add 100 ${im_base}_100
   # Remove the original image
   imrm ${im}
   # Multiply my new image by the master mask
   fslmaths ${im_base}_100 -mul all_ICs_bin ${im_base}_100_mask
   imrm ${im_base}_100
 done

}

#==============================================================================
: <<COMMENTBLOCK

Function:   CheckSize
Purpose:    remove rois less than or equal to the specified # of voxels
Input:      all images in the directory
Output:     just the images that meet criterion

COMMENTBLOCK

function CheckSize
{

size_thresh=$1

for roi  in *.nii.gz; do
  # Use fslstats to get the number of voxels in roi
  total_voxels=`fslstats ${roi} -V | awk '{printf $1 "\n"}'`

  if [ ${total_voxels} -le ${size_thresh}  ] ; then
      echo "removing ${roi} which only has ${total_voxels} voxels."
      imrm ${roi}
  fi
done
}

 #==============================================================================
: <<COMMENTBLOCK

Function:   CleanList
Purpose:    Organize NodeList.txt into the format ica2gat wants.
Input:         NodeList.txt
Output:      Nodes.txt: reformatted for ica2gat.

COMMENTBLOCK

function CleanList
{

 if [[  -e NodeList.txt  ]]; then
    IfE Nodes.txt "rm Nodes.txt"
    touch Nodes.txt
    cat NodeList.txt | tr '\n' ' ' | tr ':' '\n ' |  sed 's/roi_//g' | sed 's/.nii.gz//g' | tr -d ' ' >> Nodes.txt
 fi

  if [[  -e NodeValues.txt  ]]; then
    IfE NodesV.txt "rm NodesV.txt"
    touch NodesV.txt
    cat NodeValues.txt |  sed 's/roi_//g' | sed 's/.nii.gz//g' |  sed -e 's/_IC/,/g' | sed 's/_s1p/,1/g' | \
    sed 's/_s2p/,2/g' | sed 's/_s3p/,3/g' | tr -d ' ' >> NodesV.txt
 fi

}


 #==============================================================================
: <<COMMENTBLOCK

Function:   ExtractICs
Purpose:    Process scaling component files output by gift to produce individual 3d
            volumes of the ICs of interest
Input:      Run this in the scaling_components_files dir produced by Gift.  It unzips the zip
            files, gets the component file (4d image containing a 3d volume for each IC) and extracts volumes corresponding to the ICs of interest.

		    2 argument:
		      1) the list of ICs of interest stored in the ${LISTS} dir under tools.
		    		lst_wb1_learnICs.txt contains the components for the WordBoundary1 learnable condition
		    		lst_wb1_unlearnICs.txt contains the components for the WordBoundary1 unlearnable condition
		    	2) optionally the condition, used for labelling the output

		    /Exps/Analysis/WB1/GiftAnalysis/Learnable/Learnable_scaling_components_files contains files named like this:
		    sub001_component_ica_s1_.zip
		    After they are unzipped, we find  sub001_component_ica_s1_.nii
		    This contains % signal change images for each IC for that subject and run
		    (e.g., 41 images in this case, because there are 41 components)
        Either 3 arguments (including condition) or 2 args with only the list
		     e.g., >ICnet_prep.sh ExtractICs lst_wb1_unlearnICs.txt unlearn
         e.g., >ICnet_prep.sh ExtractICs lst_wb1_unlearnICs.txt
         Note that any named variables must be defined in ICnet_prep Main and not within the ExtractICs function.

Output:  In a subdirectory subICs, *.nii.gz files for each 3d volume of interest): e.g.,
         3 arguments: sub001_learn_IC2_s1.nii.gz; 2 arguments: sub001_IC2_s1.nii.gz

COMMENTBLOCK

function ExtractICs
{

 if [ $# -eq 2 ]              					# If 2 args, 1=function_name & 2 = IC_list
     then
         iclist=${LISTS}/$list          # this argument is the list of components of interest
         echo $list
     else
         iclist=${LISTS}/$list          #
         condition=$condition			      # else 3 args, adds condition
         echo $list $condition
  fi


# Unzip each zip file and get the name of the 4d image
 for zip  in sub*_.zip; do
   echo  ${zip}
   zipbase=`basename -s .zip ${zip}`
   unzip ${zip}  #run from the script, this extracts the images to the parent dir
   echo  ${zipbase}.nii

   #extract subnum
   subnum=`echo ${zip} | sed 's/_/ /g' | awk '{printf $1 "\n"}'`
   #extract session (run)
   run=`echo ${zip} | sed 's/_/ /g' | awk '{printf $4 "\n"}'`

mkdir subICs
for ic  in `cat ${iclist}`; do
  let im=${ic}-1  # $im is the image index of the IC (1-off)
  echo "numargs: $numargs; subnum: $subnum run: $run; condition: $condition"
  if [ $numargs -eq 2 ]; then
    echo "creating subICs/${subnum}_IC${ic}_${run}"
    fslroi ${subnum}_component_ica_${run}_.nii subICs/${subnum}_IC${ic}_${run} ${im} 1
  else
    echo "creating subICs/${subnum}_${condition}_IC${ic}_${run}"
    fslroi ${subnum}_component_ica_${run}_.nii subICs/${subnum}_${condition}_IC${ic}_${run} ${im} 1
  fi
done

# clean up
rm *.nii

done

#MyLog "ExtractICs"

}
#==============================================================================
: <<COMMENTBLOCK

Function:   ExtractICsGroup
Purpose:    Process t-stat files output by gift to produce 3d volumes of the ICs of interest
Input:      Copy the relevant tstats files from the group_stats directory to a group stats
            dir. It unzips the zip files, gets the component file (4d image containing a 3d volume for each IC) and extracts volumes corresponding to the ICs of interest).

1 argument: the list of ICs of interest stored in the ${LISTS} dir under tools.
		    		lst_wb1_learnICs.txt contains the components for the WordBoundary1 learnable condition
		    		lst_wb1_unlearnICs.txt contains the components for the WordBoundary1 unlearnable condition

		    /Exps/Analysis/Russian/GiftAnalysis/LearnSlo2ERBM/LearnSlo2ERBM_group_stats_files contains files named like this:
		    tmap_component_ica_s1_.zip
		    After they are unzipped, we find  tmap_component_ica_s1.nii
		    This contains t-stat images for each IC for run
		    (e.g., 50 images in this Russian ERBM analysis, because there are 50 components)
		   e.g., >ICnet_prep.sh ExtractICsGroup lst_wb1_unlearnICs.txt unlearn

Output:      *.nii.gz files for each 3d volume of interest): IC2_s1.nii.gz

COMMENTBLOCK

function ExtractICsGroup
{



if [ $# -eq 1 ]              					# If there is one argument
    then
        list=$1                           			# that argument is the list
      else
      	   list=$list                         			# else list is passed in from the commandline
 fi

iclist=${LISTS}/${list}  # this should be the list of components of interest

 echo "List dir is ${LISTS}"
 echo "${iclist}"
 # Unzip each zip file and get the name of the 4d image
 for zip  in tmap_*component_ica_.zip; do
   echo  ${zip}
   zipbase=`basename -s .zip ${zip}`
   unzip ${zip}  #run from the script, this extracts the images to the parent dir
   echo  ${zipbase}.nii

   #extract session (run).  Apparently this is arg 3 in the old nad data.
   # it used to be arg 4 in whatever I did last
  run=`echo ${zip}  | sed 's/_/ /g' | sed 's/s//g' | awk '{printf $3 "\n"}'`
  echo "run=${run}"

for ic  in `cat ${iclist}`; do
  let im=${ic}-1  # $im is the image index of the IC (1-off)
  echo "creating IC${ic}_${run}"
  #extract 1 image at the appropriate index from each session tmap
  # Old naming, before I realized that current gift with our choices makes tmap_component_ica_s*
  # the same as the sess data (so why generate the latter redundantly?)
  fslroi tmap_sess_${run}_component_ica_.nii IC${ic}_run${run} ${im} 1
  #fslroi tmap_scomponent_ica_s${run}_.nii IC${ic}_run${run} ${im} 1
done

# clean up
rm *.nii

done

#MyLog "ExtractICsGroup"

}
#==============================================================================
: <<COMMENTBLOCK

Function:   GMmask
Purpose:    Apply 2mm Standard space Grey Matter mask
Input:         A directory containing all and only the images to work on
Output:      Thresholded mask images with gm_bin appended


COMMENTBLOCK

function GMmask
{

for img  in *.nii.gz; do
   # define basename
    img_base=`basename -s .nii.gz ${img}`
    fslmaths ${img_base} -mul ${mask_stand_gm} -thr 0.5 -bin ${img_base}_gm_bin
    imrm ${img}
done

}

#==============================================================================
: <<COMMENTBLOCK

Function:   MkMaster
Purpose:    Make a master binary image containing all the ICs.  Used in the 4mm network approach
Input:        A directory containing all the binary images to work on
Output:     The master hotspot image: all_ICs
                 The master mask image: all_ICs_bin


COMMENTBLOCK

function MkMaster
{

bin_images=$1 # specify a regular expression for the binary images to use as input
AddImages all_ICs ${bin_images}
fslmaths all_ICs -thr 0.5 -bin all_ICs_bin

}

#==============================================================================
: <<COMMENTBLOCK

Function:   MkICMask
Purpose:    Make mask images that includes all and only those regions that meet criterion,
			Typically this would be applied to IC t-stat images at the group level, output by gift.
			The mask could then be used to create a figure for a paper or do other simple comparisons.
			This will make a positive mask for significant regions greater than or equal to the criterion
			(e.g., 2.54)
			and, if relevant, a negative mask less than or equal to the negative criterion (e.g., -2.54)
Input:         IC images and ${WEAVE_ROIS} need to exist

			Pass in 1 argument, the criterion number:
			ICnet_prep.sh MkICMask 2.5524
			You must run this function in the directory containing the images you wish to add and threshold.
Output:      This creates up to 2 mask images named for the IC and scan and suffixed with either _pos_mask or _neg_mask:
			e.g. IC23_run003_pos_mask.nii.gz and IC23_run003_neg_mask.nii.gz
			(images are only created if there is something in them)

COMMENTBLOCK

function MkICMask
{

# calculate the significant negative t-threshold, based on the tthresh
# argument passed to the function
tthresh_neg=`echo "0 - ${tthresh}" | bc`

for IC  in  *.nii.gz ; do
  echo ${IC}
  ic_base=`basename -s .nii.gz ${IC}`
  for roi  in ${WEAVE_ROIS}/roi*.nii.gz; do
    roi_base=`basename -s .nii.gz ${roi} | sed 's/roi_//g' `
    # Calculate the mean value in each region from the weave ROIs
    MeanV=`fslstats ${IC} -k ${roi} -M`

    # If the mean is greater than or equal to the tthresh (for the positive mask)
     if [ "$(echo ${MeanV} '>=' ${tthresh} | bc -l)" -eq 1 ]; then
     	# echo the information
    	echo "${ic_base}, ${roi_base}, ${MeanV}"
    	# if the mask image does not exist, create it.
	    	if [ ! -e ${ic_base}_pos_mask.nii.gz ]; then
	    		imcp ${roi}  ${ic_base}_pos_mask
	    		echo "creating mask ${ic_base}_pos_mask"
	       	# subtract the image from itself to get an empty image.
	       	fslmaths ${ic_base}_pos_mask -sub ${ic_base}_pos_mask ${ic_base}_pos_mask
	     fi
     # add each roi that meets criterion to the mask image
     fslmaths ${ic_base}_pos_mask -add ${roi} ${ic_base}_pos_mask
     fi

	# If the mean is less than or equal to the tthresh (for the negative mask)
	if [ "$(echo ${MeanV} '<=' ${tthresh_neg} | bc -l)" -eq 1 ]; then
		# echo the information
 	  	echo "negative: ${ic_base}, ${roi_base}, ${MeanV}"
 	  	# if the mask image does not exist, create it.
 	  	if [ ! -e ${ic_base}_neg_mask.nii.gz ]; then
	    		imcp ${roi}  ${ic_base}_neg_mask
	    		echo "creating mask ${ic_base}_neg_mask"
	       	# subtract the image from itself to get an empty image.
	       	fslmaths ${ic_base}_neg_mask -sub ${ic_base}_neg_mask ${ic_base}_neg_mask
	     fi
     # add each roi that meets criterion to the mask image
     fslmaths ${ic_base}_neg_mask -add ${roi} ${ic_base}_neg_mask
     fi

  done
done
#MyLog "MkICMask"

}


#==============================================================================
: <<COMMENTBLOCK

Function:   MkMean
Purpose:    Create a mean 3d image from a set of 3d input images.
            Used for slomoco clustering analysis.
            data.  In this case we assume that ExtractICs was run first to create the individual images that contribute to the mean.
Input:      IC_list, Subject_list, run_number (e.g., 1)
            Lists are assumed to be in the LISTS directory under /usr/local/tools.
            Run this in the directory containing the images to work on (i.e., the output of ExtractICs)
Example:    ICnet_prep.sh MkMean odd.txt ics.txt 1
Output:     A 3d image containing the mean of all the images added together.
Note:       Variables are named in the main ICnet_prep.sh (scroll down)

COMMENTBLOCK

function MkMean
{
# Load shared functions (need AddImages)
. functions.sh
# Make a directory from the name of the subject list
sublist_base=`basename -s .txt ${sublist}`
echo "${sublist_base}"
mkdir ${sublist_base}
echo "scan is ${scan}"
# For each subject in the subject list, and the specified scan,
# put the appropriate image file into the subdirectory.
for sub in `cat ${LISTS}/${sublist}`; do
  echo ${sub}
  cp ${sub}*s${scan}* ${sublist_base}
done


# cd to the subdir
cd ${sublist_base}
for ic in `cat ${LISTS}/${iclist}`; do
  echo " IC is ${ic}"
  # For each IC and run number in a list, create an instem
  # Concatenate and take the mean
  echo "outimg=${sublist_base}_IC${ic}_s${scan}"
  echo "instem=IC${ic}"
  AddImages ${sublist_base}_IC${ic}_s${scan} IC${ic}
  fslmaths ${outimg} -div ${count} ${outimg}_mean
  rm ${outimg}.nii.gz
done

#MyLog "MkMean"

}

#==============================================================================
: <<COMMENTBLOCK

Function:   MkMeanTlist
Purpose:    Make a list of regions for each IC where the mean value of that IC
			      is significant at a given t-threshold
Input:      IC images (typically masked with gm) and ROIS/roi* need to exist
            A t-threshold should be passed in as the only argument
Output:     A single list of image regions that meet criterion: NodeList.txt

COMMENTBLOCK

function MkMeanTlist
{
tthresh=$1
touch NodeList.txt
touch NodeValues.txt
echo regionName, condition, IC, run, MeanT, MaxT >>NodeValues.txt

for IC  in  *.nii.gz ; do
  echo ":${IC}:" >> NodeList.txt
  echo ==========================
  echo ${IC}
  for roi  in ROIS/roi*.nii.gz; do
    roi_base=`basename -s _gm_bin.nii.gz ${roi}`
    # Calculate the mean value in each region from Atlas/roi*
    MeanT=`fslstats ${IC} -k ${roi} -M`
    MaxT=`fslstats ${IC} -k ${roi} -R | awk '{print $2}'`
    # echo "IC is ${IC}, roi is ${roi}, Mean t is ${MeanT}"
    # MeanT is probably a float, so use bc to determine whether
    # MeanT is above or below the threshold.  This particular
    # use of bc returns a boolean: 1=yes, 0=no
    good=`echo "${MeanT}  >= ${tthresh}" | bc`
    # If the roi meets the criterion threshold, add it to Nodelist.txt
    if [[ ${good}  -eq 1  ]]; then
      echo "${roi_base}, ${IC},${MeanT},${MaxT}" >>NodeValues.txt
      echo "${roi_base} is good with mean t-value: ${MeanT}"
      echo "${roi_base}," >> NodeList.txt
    fi

    # if [[ ${good}  -eq 0  ]]; then
    #   echo "${roi_base}, ${IC},0, 0" >>NodeValues.txt
    # fi

  done
done

 #MyLog "MkMeanTlist"

}
#==============================================================================
: <<COMMENTBLOCK

Function:   MkRegionList
Purpose:    Make a list of mean and max activations in each region for each IC
Input:      IC images (typically masked with gm) and ${WEAVE_ROIS} need to exist
			      This is similar to MkMeanTlist except there is no threshold, so we
			      could apply it to % signal change images or others
			      Pass in 2 arguments condition and project:
			      ICnet_prep.sh MkRegionList Learnable Russian
Output:     A single list of image regions and values: NodeValues.csv

COMMENTBLOCK

function MkRegionList
{
touch NodeList.txt
touch NodeValues.txt
echo regionName,IC,run,MeanValue,MaxValue,x_max,y_max,z_max,Condition,Project >>NodeValues.csv


for IC  in  *.nii.gz ; do
  echo ":${IC}:" >> NodeList.txt
  echo ==========================
  echo ${IC}
  ic_base=`basename -s .nii.gz ${IC} | sed 's/_/, /g' `
  for roi  in ${WEAVE_ROIS}/roi*.nii.gz; do
    roi_base=`basename -s .nii.gz ${roi} | sed 's/roi_//g' `
    # Calculate the mean value in each region from the weave ROIs
    MeanV=`fslstats ${IC} -k ${roi} -M`
    # The OFS is the output field separator
    MaxV=`fslstats ${IC} -k ${roi} -R -x | awk '{OFS = ", "; print $2, $3, $4, $5}'`
      #echo "IC is ${IC}, roi is ${roi_base}, Mean t is ${MeanV}"
      echo "${roi_base},${ic_base},${MeanV},${MaxV},${condition},${project}" >> NodeValues.csv
      echo "${roi_base}, ${ic_base}, ${MeanV}, ${MaxV}, ${condition}, ${project}"
  done
done

#MyLog "MkRegionList"

}

#==============================================================================
: <<COMMENTBLOCK

Function:   MkRegionListPDHSL
Purpose:    Make a list of mean and max activations in each region for each IC
Input:      IC images (typically masked with gm) and ${WEAVE_ROIS} need to exist
			      This is similar to MkMeanTlist except there is no threshold, so we
			      could apply it to % signal change images or others
			      Pass in 2 arguments condition and project:
			      ICnet_prep.sh MkRegionList Learnable Russian
            In this case, we want to look at a very limited list of regions.
Output:     A single list of image regions and values: NodeValues.csv

COMMENTBLOCK

function MkRegionListPDHSL
{
touch NodeList.txt
touch NodeValues.txt
echo regionName,IC,run,MeanTValue,PeakTValue,x_max,y_max,z_max,Condition,Project >>NodeValues.txt
PDHSL_ROIS=/usr/local/tools/REF/IMAGES/STANDARD_MASKS/PDHSL_ROIS

for IC  in  *.nii.gz ; do
  echo ":${IC}:" >> NodeList.csv
  echo ==========================
  echo ${IC}
  ic_base=`basename -s .nii.gz ${IC} | sed 's/_/, /g' `
  for roi  in ${PDHSL_ROIS}/roi*.nii.gz; do
    roi_base=`basename -s .nii.gz ${roi} | sed 's/roi_//g' `
    # Calculate the mean value in each region from the weave ROIs
    MeanV=`fslstats ${IC} -k ${roi} -M`
    # The OFS is the output field separator
    MaxV=`fslstats ${IC} -k ${roi} -R -x | awk '{OFS = ", "; print $2, $3, $4, $5}'`
      #echo "IC is ${IC}, roi is ${roi_base}, Mean t is ${MeanV}"
      echo "${roi_base},${ic_base},${MeanV},${MaxV},${condition},${project}" >> NodeValues.csv
      echo "${roi_base},${ic_base},${MeanV},${MaxV},${condition},${project}"
  done
done

#MyLog "MkRegionListPDHSL"

}
 #==============================================================================
 : <<COMMENTBLOCK

Function:   MkWholeBrainList
Purpose:    Make a list of mean and max values for each IC
Input:         IC images need to exist
Output:      A single list of Mean and Max values for the images passed in: WholeBrainValues.txt

COMMENTBLOCK

   function MkWholeBrainList
{

echo image, mean, max >>WholeBrainValues.txt
for im  in  *.nii.gz ; do
 im_base=`basename -s .nii.gz ${im}`
    MeanV=`fslstats ${im} -M`
    MaxV=`fslstats ${im} -R | awk '{print $2}'`
      echo "${im_base}, ${MeanV}, ${MaxV}" >>WholeBrainValues.txt
      echo "${im_base}, ${MeanV}, ${MaxV}"
  done

#MyLog "MkWholeBrainList"

}

#==============================================================================
: <<COMMENTBLOCK

Function:   Reslice
Purpose:    Reslice2mm to 4mm
Input:      A directory containing all and only the images to work on
Output:     resliced images with 4mm appended


COMMENTBLOCK

function Reslice
{

for img  in *.nii.gz; do
  # define basename
  img_base=`basename -s .nii.gz ${img}`
  Reslice2x4mask ${img}
  imrm ${img}
done

}

#==============================================================================
: <<COMMENTBLOCK

Function:   ThreshClust
Purpose:    Apply cluster size threshold. Typically we apply this to the output of ThreshT
Input:      A directory containing all and only the images to work on
            c_size, the desired cluster size.
Output:     Positive value images with c appended

COMMENTBLOCK

function ThreshClust
{
c_size=$1

for img  in *.nii.gz; do
  # define basename
  img_base=`basename -s .nii.gz ${img}`
  # make binary masks
  fslmaths ${img} -thr 0.5 -bin ${img_base}_bin
  # cluster clean the binary masks at the threshold
  cluster_clean.sh ${img_base}_bin ${c_size}  6
  imrm *cluster*
  imrm *labels*
  fslmaths ${img_base} -mul ${img_base}_bin ${img_base}c
  # clean up the binary masks, we don't need them yet.
  rm ${img_base}*_bin*
  imrm ${img}
done

}

#==============================================================================
: <<COMMENTBLOCK

Function:   ThreshPos
Purpose:    Retain only positive values in the image
Input:        A directory containing all and only the images to work on
Output:     Positive value images with p appended

COMMENTBLOCK

function ThreshPos
{

for img  in *.nii.gz; do
  # define basename
  img_base=`basename -s .nii.gz ${img}`
  # make positive value images
  fslmaths ${img} -thr 0 ${img_base}p
  imrm ${img}
done

}
#==============================================================================
: <<COMMENTBLOCK

Function:   ThreshT
Purpose:    Apply t-stat threshold
Input:         A directory containing all and only the images to work on
                  t, the desired statistical threshold
Output:      Thresholded images with _t appended
                  An Orig dir containing the original unthresholded images

COMMENTBLOCK

function ThreshT
{
t=$1

mkdir Orig

for img  in *.nii.gz; do
  # define basename
  img_base=`basename -s .nii.gz ${img}`
  fslmaths ${img_base} -thr ${t} ${img_base}_t
  immv ${img}  Orig
done

}
 #==============================================================================
: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Print relevant help message

COMMENTBLOCK

function HelpMessage
{

echo "       "
echo "$0"
echo "Run gift analysis first, create a Net dir with the group t-stats for ICs of interest, for each session."
echo "Requires that you specify MainWB1 or something else."
echo "You will need to calculate the tthresh and cluster size criterion as described in the comment block."
echo "at the top of this file."
echo "================================ "

}

#==============================================================================
: <<COMMENTBLOCK

Function:   MainWB14mm
Purpose:    Run the functions to prepare the 4mm network analysis files
Input:      a directory of selected ICs for WB1
Output:     Cleaned up files ready for Matlab

COMMENTBLOCK

function MainWB14mm
{

 ThreshT 2.6026
 ThreshClust 274
 GMmask
 Reslice
 MkMaster s*bin
 Add100Crop s*bin

}
#==============================================================================
: <<COMMENTBLOCK

Function:   MainMask
Purpose:    Run the functions to do the GM masking of the atlas rois
Input:      A directory Atlas containing all the roi_* files
            The atlas files are created by atlas_extract_all.sh in subdirectories,
            So we have to check the names and move them up to the main ROIS directory
            before running this function. This may include removing rois that are not useful
            such as the entire cortex, the brainstem, the white matter etc
Output:     Cleaned up GM masked regions for further processing

COMMENTBLOCK

function MainMask
{
 cd ROIS
 GMmask                       # GM mask my Atlas rois
 CheckSize 6                  # Remove masks with fewer than 6 voxels
 cd ..                        # Get back out of the atlas dir

}


 #==============================================================================

: <<COMMENTBLOCK

Function:   MainWB1
Purpose:    Run the functions to decide which regions are "IN" the network.
			      We start by thresholding the t-stat image at 0 to retain only positive values.
			      Then we apply the p=0.05 threshold such that if the mean value in the region
			      is at or above the threshold, then it is "IN" the network"
			      (0.05 value determined using matlab for N=16)
Input:      A directory of selected ICs for WB1 (run from this dir)
            An ROIS subdir containing the atlas regions roi* for crossing with the ICs
Output:     Cleaned up text files ready for ica2gat

COMMENTBLOCK

function MainWB1
{

 #ThreshPos                     # This creates positive value only images
 MkMeanTlist 1.7531             # Save list of each roi w mean > p=0.05 for 16 subjects
 CleanList                      # Create the properly formatted Node.txt
}

#==============================================================================
: <<COMMENTBLOCK

Function:   MainIce
Purpose:    Run the functions to decide which regions are "IN" the network.
			We start by thresholding the t-stat image at 0 to retain only positive values.
			Then we apply the p=0.05 threshold such that if the mean value in the region
			is at or above the threshold, then it is "IN" the network"
			(0.05 value determined using matlab for N=14.)
Input:        No arguments. input IC files must exist in current dir.  Masks must exist
			in subdir called ROIS
Output:      A clean list ready for ica2gat

COMMENTBLOCK

function MainIce
{

ThreshPos                       # This cleans up little clusters after masking at 0.5
 MkMeanTlist 1.7709       # Save list of each roi w mean > p=0.05
 CleanList                        # Create the properly formatted Node.txt

}


#==============================================================================

while getopts h options 2> /dev/null; do        # Setup -h flag. redirect errors
    case $options in                            # In case someone invokes -h
    h) HelpMessage;;                            # Run the help message function
    \?) echo "only h is a valid flag" 1>&2      # If bad options are passed, print message
    esac
done

shift $(( $OPTIND -1))                          # Index option count, so h isn't treated as arg
optnum=$(( $OPTIND -1))                         # optnums hold # of options passed in

if [ ${optnum} -lt 1 ]              		        # If no options or args
    then
        dothis=$1                           	  # Run the function indicated
                          				              # Run the function as indicated
        echo "${dothis}"

         # if the function takes an argument, set the argument here so we can pass it in at the commandline
          if  [ ${dothis} = "ExtractICs" ] && [ $# = 2 ]; then
            list=$2
            echo ${list}
            numargs=$#
          else
              list=$2
              condition=$3
              numargs=$#
              echo ${list}  ${condition} $#
          fi

     	   if  [ ${dothis} = "ExtractICsGroup" ]; then
         	list=$2
         	echo ${list}
         fi

         if  [ ${dothis} = "MkMean" ]; then
           sublist=$2
           iclist=$3
           scan=$4
         fi

         if  [ ${dothis} = "MkRegionList" ] || [ ${dothis} = "MkRegionListPDHSL" ] ; then
         	condition=$2
         	project=$3
         	echo ${condition}, ${project}
         fi

         if  [ ${dothis} = "MkICMask" ]; then
         	tthresh=$2
         	echo ${tthresh}
         fi

fi

$dothis                                                            # Run

#MyLog
