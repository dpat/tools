#!/bin/bash


###############################################################################
#########################  DEFINE FUNCTIONS  ##################################
: <<COMMENTBLOCK

Function:   FixNaN
Purpose:    Fix problem with SPM warped images containing NaNs
            Most FSL tools cannot handle the NaNs

COMMENTBLOCK

FixNaN ()
{
dir_list="spm_brain spm_head spm_old spm_old_nosw spm_tpm_brain spm_tpm_head"

for dir in ${dir_list}; do
  echo ${dir}
  bm=${dir}/${s}_MNI_brain_mask.nii.gz
  b=${dir}/${s}_MNI_brain.nii.gz
  l=${dir}/${s}_MNI_lesion.nii.gz
  h=${dir}/${s}_MNI.nii.gz

  # Fix brain_mask
  if [ ! -e ${bm} ]; then
    fslmaths ${dir}/${s}_MNI_brain_mask.nii -nan ${dir}/${s}_MNI_brain_mask.nii.gz
    rm ${dir}/${s}_MNI_brain_mask.nii
  else
    rm ${dir}/${s}_MNI_brain_mask.nii
  fi

  # Fix brain
  if [ ! -e ${b} ]; then
    fslmaths ${dir}/${s}_MNI_brain.nii -nan ${dir}/${s}_MNI_brain.nii.gz
    rm ${dir}/${s}_MNI_brain.nii
  else
    rm ${dir}/${s}_MNI_brain.nii
  fi

  # Fix lesion
  if [ ! -e ${l} ]; then
    fslmaths ${dir}/${s}_MNI_lesion.nii -nan ${dir}/${s}_MNI_lesion.nii.gz
    rm ${dir}/${s}_MNI_lesion.nii
  else
    rm ${dir}/${s}_MNI_lesion.nii
  fi

  # Fix head
  if [ ! -e ${h} ]; then
    fslmaths ${dir}/${s}_MNI.nii -nan ${dir}/${s}_MNI.nii.gz
    rm ${dir}/${s}_MNI.nii
  else
    rm ${dir}/${s}_MNI.nii
  fi

done
}
#==============================================================================

: <<COMMENTBLOCK

Function:   ResliceSPM
Purpose:    Fix problem with SPM warped images containing NaNs
            Most FSL tools cannot handle the NaNs

COMMENTBLOCK

ResliceSPM ()
{

### Reslice into 91,109,91 space ####
template=/usr/local/fsl/data/standard/MNI152_T1_2mm_brain.nii.gz
#
dir_list_new="spm_brain spm_head spm_tpm_brain spm_tpm_head"
dir_list_old="spm_old spm_old_nosw"

for dir in ${dir_list_new}; do
  echo "${dir}/${s}"

  if [ ! -e ${dir}/${s}_MNI_brain_mask_r.nii.gz ]; then
    flirt -in ${dir}/${s}_MNI_brain_mask  -applyxfm -init /Volumes/Main/Working/LesionMapping/DataC8/spm_new_reslice.mat -out ${dir}/${s}_MNI_brain_mask_r.nii.gz -paddingsize 0.0 -interp nearestneighbour -ref ${template}
  fi

  if [ ! -e ${dir}/${s}_MNI_lesion_r.nii.gz ]; then
    flirt -in ${dir}/${s}_MNI_lesion  -applyxfm -init /Volumes/Main/Working/LesionMapping/DataC8/spm_new_reslice.mat -out ${dir}/${s}_MNI_lesion_r.nii.gz -paddingsize 0.0 -interp nearestneighbour -ref ${template}
  fi

  if [ ! -e ${dir}/${s}_MNI_brain_r.nii.gz ]; then
    flirt -in ${dir}/${s}_MNI_brain  -applyxfm -init /Volumes/Main/Working/LesionMapping/DataC8/spm_new_reslice.mat -out ${dir}/${s}_MNI_brain_r.nii.gz -paddingsize 0.0 -interp trilinear -ref ${template}
  fi

  if [ ! -e ${dir}/${s}_MNI_r.nii.gz ]; then
    flirt -in ${dir}/${s}_MNI  -applyxfm -init /Volumes/Main/Working/LesionMapping/DataC8/spm_new_reslice.mat -out ${dir}/${s}_MNI_r.nii.gz -paddingsize 0.0 -interp trilinear -ref ${template}
  fi
done

for dir in ${dir_list_old}; do
  echo "${dir}/${s}"
  if [ ! -e ${dir}/${s}_MNI_brain_mask_r.nii.gz ]; then
    flirt -in ${dir}/${s}_MNI_brain_mask  -applyxfm -init /Volumes/Main/Working/LesionMapping/DataC8/spm_old_reslice.mat -out ${dir}/${s}_MNI_brain_mask_r.nii.gz -paddingsize 0.0 -interp nearestneighbour -ref ${template}
  fi

  if [ ! -e ${dir}/${s}_MNI_lesion_r.nii.gz ]; then
    flirt -in ${dir}/${s}_MNI_lesion  -applyxfm -init /Volumes/Main/Working/LesionMapping/DataC8/spm_old_reslice.mat -out ${dir}/${s}_MNI_lesion_r.nii.gz -paddingsize 0.0 -interp nearestneighbour -ref ${template}
  fi

  if [ ! -e ${dir}/${s}_MNI_brain_r.nii.gz ]; then
    flirt -in ${dir}/${s}_MNI_brain  -applyxfm -init /Volumes/Main/Working/LesionMapping/DataC8/spm_old_reslice.mat -out ${dir}/${s}_MNI_brain_r.nii.gz -paddingsize 0.0 -interp trilinear -ref ${template}
  fi

  if [ ! -e ${dir}/${s}_MNI_r.nii.gz ]; then
    flirt -in ${dir}/${s}_MNI  -applyxfm -init /Volumes/Main/Working/LesionMapping/DataC8/spm_old_reslice.mat -out ${dir}/${s}_MNI_r.nii.gz -paddingsize 0.0 -interp trilinear -ref ${template}
  fi
done
}

#########################  END FUNCTION DEFINITIONS  ##########################
###############################################################################

s=`basename ${PWD}`
FixNaN
ResliceSPM
