tic
% =========load the output of img2adj in the workspace========

% the variables from the img2adj workspace, are
% used to run BCT functions AND reconstruct a 3d image containing the network measures.
% Orientation and location should always be checked against the original.

% ======APPLY FUNCTION from BCT (or whatever) to create the column vec_metric============
% e.g., vec_metric=modularity_und(r); % use r as an adjacency matrix for running Sporns' connectivity tools (modularity is an example).  
%====================

ext='.nii';

% Create output 3d image from vector of values and information about
% dimensions of the input.  The latter are loaded in from the workspace
% variables saved by img2adj.m
Xout=zeros(x1,x2,x3); % create an empty array to hold the output image
for k=1:length(vec_metric)  % sadly, use a for loop 
    Xout(row(k),col(k),page(k))=vec_metric(k); % Put the values into the 3d image array
end

Xout_nii=make_nii(Xout, [dim_x dim_y dim_z]); % call make_nii with appropriate voxel dimensions to create struct
save_nii(Xout_nii, ([input_image '_out' ext])); % export struct to file with *.nii suffix

clear;  
toc