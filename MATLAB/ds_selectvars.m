% Written by Dianne Patterson 1/31/2013
%
% Expected input to this script:
% 2 cell arrays: 
% 1 contains the dataset name:
% dsName={'d_end_mdlf'};
%
% 2 lists the fields for the new dataset, e.g., 
% fieldNameArray={'R_L', 'volmm_stand', 'MaxMusicSkill'};    
% the new dataset will be called 'ds_filtered'

dsVarList=cell(size(fieldNameArray)); % Create cell array of the same size as fieldNameArray
len_ds=length(fieldNameArray); % Store length of fieldNameArray
 
dsVarList(1:len_ds)=strcat((dsName),'.',(fieldNameArray(1:len_ds)),','); % Create string of variable names with commas                                   
fields=sprintf('%s ', dsVarList{:});   % Create a string from all the elements in the cell array
sprintf('dataset(%s ''VarNames'', (fieldNameArray));', fields)  % Print out the dataset statement
ds_filtered=eval(ans); % Create the filtered dataset
clear dsVarList len_ds i fields ans; 
 