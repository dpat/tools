#!/bin/bash

# Define paths and variables
# source img_profile.sh; source subject_profile.sh
# Load shared functions
#. /usr/local/tools/LIBRARY/functions.sh
#. /usr/local/tools/LIBRARY/prep_functions.sh

: <<COMMENTBLOCK
Extract mean positive z-score for each subject/session in Individual_ru folder
Elena wants these mean values.  Note that this is done on the voxel level.
We generate whole brain, left hemisphere and right hemisphere values
COMMENTBLOCK

touch indiv_mean_z.csv
echo "giftSubjectNum,IC,scan,brain_mean_pos,brain_SD_pos,L_mean_pos,L_SD_pos,R_mean_pos,R_SD_pos" >> indiv_mean_z.csv

# Operate on all img files in the directory
for input in *.nii.gz; do
  # Parse the filename into variables
  # run simple stats on the images and redirect those to a csv file
  datasource=`echo ${input} | sed 's/_/ /g' | sed 's/.nii.gz//g' | awk '{print $1, $2, $3}'`
  stats_pos=`fslstats ${input} -l 0 -M -S`
  stats_pos_left=`fslstats ${input} -k ${mask2mm_L} -l 0 -M -S`
  stats_pos_right=`fslstats ${input} -k ${mask2mm_R} -l 0 -M -S`
  # echo the variables, add in commas
  echo ${datasource} ${stats_pos} ${stats_pos_left} ${stats_pos_right}| sed 's/ /,/g' >> indiv_mean_z.csv
done
