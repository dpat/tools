#!/bin/bash

: <<COMMENTBLOCK
This code calls GDCM (http://gdcm.sourceforge.net/) to anonymize dates in a DICOM dataset.
COMMENTBLOCK

#==============================================================================

: <<COMMENTBLOCK

Function:   Help
Purpose:    Provide Help message

COMMENTBLOCK

Help ()
{
if [ $# -lt 1 ]
    then
      echo "This code calls GDCM (http://gdcm.sourceforge.net/) to anonymize dates in a DICOM dataset."
      echo "The code will either query or revise the dates"
      echo ""
      echo "To query the dates, provide one argument:" 
      echo "argument 1: the name of the dicom directory to deidentify"
      echo ""
      echo "e.g., $0 inputs/sub-219_no_session_dicoms"
      echo ""
      echo "Supply a second and third argument to revise the dates:"
      echo "arg2 is the revised scan date and arg3 is the revised birth date:"  
      echo "Date must be yearmonthday, e.g., 20180101."  
      echo "The scan date is replaced in 6 fields"
      echo "The birth date is replaced in one field"
      echo ""
      echo "e.g., $0 inputs/sub-219_no_session_dicoms 20180101 19610101"
      echo ""
      echo "Revised dicoms are placed in a directory dicom_anon"
      exit 1
fi
}


#==============================================================================

: <<COMMENTBLOCK

Function:   Query
Purpose:    Return information about Date fields in DICOM header

COMMENTBLOCK

Query ()
{
# Remove any pesky .DS_Store files in the dicomdir
  find ${ddir} -type f -name .DS_Store -exec rm {} \;
  # nesting check: This should list a real DICOM file:
  filepath=$(find ${ddir} -type f | head -n 1)
  gdcmdump $filepath | grep Date
}


#==============================================================================

: <<COMMENTBLOCK

Function:   Replace
Purpose:    Replace date fields with provided values

COMMENTBLOCK

Replace ()
{
  if [ ! -d dicom_anon ]; then mkdir dicom_anon; fi
  gdcmanon --dumb --replace 8,12,${scandate} --replace 8,20,${scandate} --replace 8,21,${scandate} \
  --replace 8,22,${scandate} --replace 8,23,${scandate} --replace 40,244,${scandate} \
  --replace 10,30,${birthdate} --input ${dicomdir} --output dicom_anon --recursive
}

#============================================================================

: <<COMMENTBLOCK

Function:   Main
Purpose:    Run main functions
Calls:      Input Dicom directory must exist

COMMENTBLOCK

if [ $# -lt 1 ]; then Help; fi
if [ $# -eq 1 ]; then
  ddir=${1}
  Query ${ddir}
  elif [ $# -eq 2 ]; then Help; fi
if [ $# -eq 3 ]; then
  dicomdir=${1} 
  scandate=$2
  birthdate=$3
  ddir=dicom_anon
  Replace ${dicomdir} ${scandate} ${birthdate} 
  echo "dicom_anon has these revised date values:"
  Query ${ddir}
fi
