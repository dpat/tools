function dfnc_states_block_hilo(postprocess_file, block_file, taskload_file, numstates, hilo)
% example: 
% dfnc_states_block_hilo('LearnSlo2ERBM_dfnc_post_process.mat', 'block_windowed.mat', 'taskload_ru.mat', 9, 1)
% state data in contained in the DFNC subdir in *_dfnc_post_process.mat
% The number of states (numstates)is the number of centroids (k) used in 
% dFNC post procesing.
% hilo=1 if you want values greater than the mean of the task; 
% hilo=0 if you want values less than the mean of the task
load(postprocess_file);
% load the windowed vector of blocks for all sessions.  
% This is generated with block_win.m 
load(block_file);
% load the taskload file (which must contain a variable called taskload)
load(taskload_file);
task_mean=mean(taskload);
% Does each value in the vector meets criterion?  Create vector
% of logical 1s and 0s
task_hilo=(taskload>task_mean);
% permute the dimensions of the states into a more approachable format.
% (a column of timepoints for each subject and each session.)
states=permute(clusterInfo.states,[3,1,2]);
numsess=size(states, 3); % dimension 3 is the number of sessions
numsub=size(states,2);   % dimension 2 is the number of subjects
numwins=size(states,1);  % dimension 1 is the number of windowed timepoints
statevals=[1:numstates]; % a vector of the possible states

% reshape the block file to be more useful in this context
% so each column corresponds to one session
sess_block=reshape(block_windowed,[numwins,numsess]);

for sess=1:numsess
    for sub=1:numsub
        % iterate over records 
        for block=min(sess_block(:,sess)):max(sess_block(:,sess))
             % if the block value = the block at this iteration 
             % and it is marked hi (value > mean of taskload)
             indx=find(sess_block(:,sess)==block & task_hilo==hilo);
             % get all the values in the block that meet criterion
             vec=(states(indx,sub,sess));
             nstate=nnz(diff(vec)); % number of state transitions
             state_cat=categorical(vec, statevals); % count of each state
             cnt=countcats(state_cat)';
             cnt1=table(sub, sess, block, hilo, nstate, cnt);
             if ~exist ('t', 'var')
                 t=cnt1;
                 % if t exists, concatenate cnt1 onto t
                 elseif exist ('t', 'var')
                 t=[t;cnt1];
             end  
        end 
    end  
end    
% write the concatenated table for subjects and sessions.
if hilo == 1
    writetable(t,'state_stats_block_hi.csv');
else writetable(t,'state_stats_block_lo.csv');
end

