#!/bin/bash

: <<COMMENTBLOCK
This code calls the docker heudiconv tool to convert DICOMS into the BIDS data structure.
It requires that you are in the parent directory of both the Dicom and Nifti directories AND that your Nifti directory contain a subdirectory called Code with the conversion routine, e.g., convertall.py in it. See https://neuroimaging-core-docs.readthedocs.io/en/latest/pages/heudiconv.html.
COMMENTBLOCK


# Exit if number of arguments is too small
if [ $# -lt 2 ]
    then
        echo "======================================================"
        echo "Two arguments are required. An optional 3rd argument can be added to encode the session if there is one"
        echo "argument 1: name of conversion file in the Nifti/Code directory, e.g., convertall.py"
        echo "argument 2: name of subject Dicom folder to convert"
        echo "argument 3: optional name of the session"
        echo "e.g., $0 convertall.py 304"
        echo "e.g., $0 convertall.py 304 ses1"
        echo "output will be a BIDS directory under the Nifti folder"
        echo "This assumes you are running docker"
        echo "and have downloaded heudiconv: docker pull nipy/heudiconv"
        echo "It also assumes that you are running from the parent directory to both Dicom and Nifti"
        echo "If you have a session argument, this assumes your DICOMS are nested under subject and then session"
        echo "Finally, note that the dicoms are assumed to be *.dcm files"
        echo "======================================================"
        exit 1
fi

# Define the variables
converter=${1}
subject=${2}
session=${3}


# If there is one argument
if [ $# -eq 2 ]; then
  echo "The subject directory for ${subject} under Dicom will be"
  echo "converted to BIDS and output to sub-${subject} under Nifti"
  docker run --rm -it -v ${PWD}:/base nipy/heudiconv:latest -d /base/Dicom/{subject}/*/*.dcm -o /base/Nifti/ -f /base/Nifti/Code/${converter} -s ${subject} -c dcm2niix -b --minmeta --overwrite
# else if there are 3 arguments
elif [ $# -eq 3 ]; then
  echo "The subject directory for ${subject} and session ${session} under Dicom will be"
  echo "converted to BIDS and output to sub-${subject} and session ${session} under Nifti"
  docker run --rm -it -v ${PWD}:/base nipy/heudiconv:latest -d /base/Dicom/{subject}/{session}/*/*.dcm -o /base/Nifti/ -f /base/Nifti/Code/${converter} -s ${subject} -ss ${session} -c dcm2niix -b --minmeta --overwrite
# For anything else, spit out the help message and stop.
else
  echo "======================================================"
  echo "Two arguments are required. An optional 3rd argument can be added to encode the session if there is one"
  echo "argument 1: name of conversion file in the Code directory, e.g., convertall.py"
  echo "argument 2: name of subject Dicom folder to convert"
  echo "argument 3: optional name of the session"
  echo "e.g., $0 convertall.py 304"
  echo "e.g., $0 convertall.py 304 ses1"
  echo "output will be a BIDS directory under the Nifti folder"
  echo "This assumes you are running docker"
  echo "and have downloaded heudiconv: docker pull nipy/heudiconv"
  echo "======================================================"
  exit 1
fi
