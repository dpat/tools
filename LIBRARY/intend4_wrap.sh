#!/bin/bash

: <<COMMENTBLOCK
This code calls the docker intend4 tool. 
COMMENTBLOCK


# Exit and spit out help message if number of arguments is too small
if [ $# -lt 1 ]
    then
        echo "======================================================"
        echo "This assumes you are running docker"
        echo "and have downloaded intend4: docker pull diannepat/intend4"
        echo "You should run this from your bids_dir"
        echo "Either provide specific participants to process, e.g., $0 083 087"
        echo "or process all of them if the argument = all: e.g., $0 all"
        echo ""
        echo "The program will report its progress for each subject to the terminal."
        echo "Processing a subject multiple times does no harm."
        echo "The program will skip any subject with the wrong number of phasediff images."
        echo ""
        echo "WARNING: The program will overwrite any existing IntendedFor field!"
        echo "======================================================"
        exit 1
fi


# assign the argument to "input"
input=${1}

if [ ${input} = "all" ]; then
 echo "JSON files for all subjects will be updated with the IntendedFor field"
 docker run -it --rm --name intend4 -v ${PWD}:/data diannepat/intend4:latest --verbose --bids_dir=/data	
else 
  # Including "" around PWD ensures it will handle spaces in the path names.
 echo "running these subjects ${*}" 
 docker run -it --rm --name intend4 -v ${PWD}:/data diannepat/intend4:latest --verbose --bids_dir=/data --participant_label ${*}
fi