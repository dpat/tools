function coords=toMatXYZ(input_image)

% % INPUT FILE should be UNZIPPED 3D or 4D image (positive integer values
% % only) Output is a matrix with each column representing the x,y,z
% % coordinates corresponding to the image vector Dianne Patterson, Created:
% % Sep 27, 2013 Calls tools in the nifti_io toolbox from
% % http://www.rotman-baycrest.on.ca/~jimmy/NIFTI/

ext='.nii';
img=load_nii([input_image ext]);
X1=double(img.img); %extract the raw image from the structure, make it a double to be more compatible with other matlab functions
clear img;
X1=flipdim(X1,1); % do the left-right flip
[lin]=find(X1(:,:,:,1));  %identify the linear index of any nonzero value in the first of the 4d volumes.
X1size=size(X1); % Get the matrix size
[row,col,page]=ind2sub(X1size,lin);% extract the row, column and page from the linear index
% This corrects for the 0-1 difference between fsl and matlab
coords=[row-1,col-1,page-1];
coords=coords';
end


