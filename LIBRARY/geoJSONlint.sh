#!/bin/sh

# This script is released in accordance with the Creative Commons Attribution-NonCommercial 
# Unported License: http://creativecommons.org/licenses/by-nc/4.0/legalcode
# Example (this script takes one argument, a geojson file, and verifies that it is correctly formatted):
# geoJSONlint.sh ax_144_inf_0mm.geojson

validate="http://geojsonlint.com/validate"

if [ $# -lt 1 ]; then
  echo "Usage: $0 geojson-filename"
  exit 1
fi

jsonFile=${1:-test.geojson}
curl -X POST -d @${jsonFile} $validate
echo
