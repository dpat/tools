% Written by Dianne Patterson 1/31/2013
%
% Help: expected input.
% We need 2 cell arrays, the first contains the dataset name.  The second
% is a list of the fields I want in the new dataset, e.g., 
% dsName={'d_end_mdlf'};
% fieldNameArray={'R_L', 'volmm_stand', 'MaxMusicSkill'};    
%
% fitted and stepwise linear models are displayed.

ds_selectvars;
%======================================
% GLM simple fit to look at everything.  
% We are predicting the last value in the fieldNameArray from the other
% variables in the fieldNameArray
%myfit=LinearModel.fit(ds_filtered)
% plot(myfit)
% Look for heteroskedasticity (we want a random cloud):
% plotResiduals(myfit, 'fitted', 'marker', 'o', 'MarkerFaceColor', 'b')
% Look for autocorrelation (suggesting there is a trend in the data we
% don't account for). We want a random cloud.
% figure; plotResiduals(myfit, 'lagged', 'marker', 'o', 'MarkerFaceColor', 'b')
% Make residuals histogram
% figure; plotResiduals(myfit)
% case values and leverage:
% figure; plotDiagnostics(myfit)

%stepwise to find the best predictors
mystep=LinearModel.stepwise(ds_filtered)
%plot(mystep)
% Here is how you do a quadratic curve
% myquad=LinearModel.fit(ds_filtered, 'quadratic');