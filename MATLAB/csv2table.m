function csv2table(data)
% Purpose
% csv2dataset creates datasets from csv files.
% dataset names are the same as the csv files.
%
% Input Arguments and Examples
% pass in a cell array of strings, e.g.,
% csv2dataset({'dprime', 'dprime_tracts'})
% csv2dataset will look for dprime.csv and dprime_tracts.csv to convert.
% Resulting variable names will be the same as the csv file without 
% the extension. They will be in the base workspace.
% function csv2dataset(data)

    for idata=(data);
        % converts cells to strings, so sprintf can use them
        var=char(strcat((idata))); 
         % creates a string to identify the csv file
        myFile=char(strcat((idata), '.csv'));
        % create new table
        readtable(myFile);
        % rename ans (in the function workspace) to the string in var in the base workspace
        assignin('base',(var), ans); 
        clear ans;
    end;  