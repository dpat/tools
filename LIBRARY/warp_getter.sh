#!/bin/bash

: <<COMMENTBLOCK
This script is intended to get the warp file I need.
Run it from the main bids dir
Created: 2020_07_14
Author: Dianne Patterson, Ph.D.
COMMENTBLOCK

mkdir -p missing_stuff/derivatives/fsl_dwi_proc

for subj in sub-*; do
 warp=derivatives/fsl_dwi_proc/${subj}/reg/${subj}_b0_mni_warp.nii.gz
 mkdir -p missing_stuff/derivatives/fsl_dwi_proc/${subj}/reg/
 cp $warp missing_stuff/derivatives/fsl_dwi_proc/${subj}/reg/
done

