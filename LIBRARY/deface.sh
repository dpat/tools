#!/bin/bash

if [ $# -lt 1 ]
then
    echo "Usage: $0 <input file>"
    echo "Example: $0 mprage.nii.gz"
    echo "defaces a structural image, creating img_defaced.nii.gz"
    echo "prefers T1 images"
    exit 1
fi

img=$1
stem1=`remove_ext $1`

echo "stem1=${stem1}"

GCA_DIR=/usr/local/tools/REF/IMAGES
echo "GCA_DIR=${GCA_DIR}"
# Ensure orientation is correct, as this helps deface.
echo "reorienteing"
fslreorient2std ${img} ${img}

echo "defacing"
if [ ! -e ${stem1}_defaced.nii.gz ]; then
  mri_deface ${img} ${GCA_DIR}/talairach_mixed_with_skull.gca ${GCA_DIR}/face.gca ${stem1}_defaced.nii

  gzip ${stem1}_defaced.nii
fi

fslmaths ${stem1}_defaced.nii.gz ${stem1}_defaced.nii.gz -odt short

# Generate a difference mask image.
if [ ! -e ${stem1}_deface_mask.nii.gz ]; then
  fslmaths ${stem1}_defaced -bin ${stem1}_defacemask -odt char
fi
echo "=================================================================="
echo "You should check your defacing results.  The mask should cover the entire brain:"
echo ">fsleyes ${stem1} ${stem1}_defacemask -cm Red -a 40"
echo "If the mask does not entirely overlap the brain, manually add voxels, then,"
echo ">fslmaths ${stem1} -mul ${stem1}_defacemask ${stem1}_defaced"
