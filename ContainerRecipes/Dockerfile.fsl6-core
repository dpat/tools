FROM ubuntu:18.04
MAINTAINER Dianne Patterson <dkp@email.arizona.edu>

# Core system capabilities required.
# fslreorient2std needs bc
# /usr/local/fsl/bin/standard_space_roi needs dc
# flirt needs libopenblas-dev
RUN apt-get update && apt-get install -y \
    bc \
    dc \
    git \
    libopenblas-dev \
    nodejs \
    npm \
    python \
    tar \
    unzip \
    wget \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN npm install -gq bids-validator

# Run FSL installer and remove unneeded directories
RUN wget -q http://fsl.fmrib.ox.ac.uk/fsldownloads/fslinstaller.py && \
    chmod 775 fslinstaller.py
RUN /fslinstaller.py -d /usr/local/fsl -q \
    && rm -rf /usr/local/fsl/doc /usr/local/fsl/data/first /usr/local/fsl/data/atlases \
              /usr/local/fsl/data/possum /usr/local/fsl/src /usr/local/fsl/extras/src \
              /usr/local/fsl/bin/fslview* /usr/local/fsl/bin/FSLeyes

# Make FSL happy
ENV FSLDIR=/usr/local/fsl
ENV PATH=$FSLDIR/bin:$PATH
RUN /bin/bash -c 'source /usr/local/fsl/etc/fslconf/fsl.sh'
ENV FSLMULTIFILEQUIT=TRUE
ENV FSLOUTPUTTYPE=NIFTI_GZ
RUN ln -s /usr/local/fsl/bin/eddy_openmp /usr/local/fsl/bin/eddy
