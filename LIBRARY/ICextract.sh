#!/bin/bash

#==============================================================================
: <<COMMENTBLOCK

Function:   ExtractICGroup
Purpose:    Process group t-stat files output by GIFT in order to produce 3d volumes of the ICs of interest.
            The function extracts volumes corresponding to the ICs of interest from each *component*.nii 
            (4d image containing a 3d volume for each IC) file it finds in the directory.
Input:      Directory of group tstat files:
            Copy the relevant tstats files from the GiftAnalysis group_stats directory to a separate extracts directory:
            e.g., nad2_extracts/group
            If you created subdirectories when you ran GIFT, tstats files are in a subdirectory labeled *_group_stats_files. The files are named like this: tmap_component_ica_s1.nii
            This assumes the files are NOT zipped!
            ------------------------
            List of the ICs to extract: e.g., lst_nad2_learn_ICs.txt
            The list is a text file containing the numerical values of the ICs of interest.
            The list should contain one IC per row, e.g.:
            23
            11
            14

            For convenience, place the list in the parent directory of group. 
            ------------------------
    		    Pass in 2 arguments to ICextract.sh:
            1) the name of this function: ExtractICsGroup
            2) the path to the list: ../lst_nad2_learn_ICs.txt

            e.g., ICextract.sh ExtractICsGroup ../lst_nad2_learn_ICs.txt 
Calls:      ExtractIC
Output:     *.nii.gz files for each 3d volume of interest): e.g., IC2_sub002_run1.nii.gz

COMMENTBLOCK

ExtractICGroup ()
{
  echo "group func list is $iclist"
  
  for tmap  in tmap_component_ica_*.nii; do
    # Get the name of the 4d image using FSL's remove_ext function
    tmapbase=$(remove_ext ${tmap})
    gift_4dimg=${tmapbase}.nii
    # extract session (run) for naming. Assume it is the 4th value in the name
    # where values are separated by underscores
    run=$(echo ${tmap} | sed 's/_/ /g' | sed 's/s//g' | awk '{printf $4 "\n"}')
    echo "run=${run}"
    # Use the run number for the output name
    outname=run${run}

    for ICnum in $(cat ${iclist}); do
      echo $ICnum
      ExtractIC ${gift_4dimg} ${ICnum} ${outname}
    done
  done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   ExtractICIndiv
Purpose:    Process scaling component files component output by GIFT in order to produce 3D volumes of the individual ICs of interest.
            The function extracts volumes corresponding to the ICs of interest from each *component*.nii 
            (4d image containing a 3d volume for each IC x individual and run) file it finds in the directory.
Input:      Directory of individual scaling component files (not timecourses):
            Copy the relevant scaling component files from the GiftAnalysis directory to a separate extracts directory:
            e.g., nad2_extracts/indiv
            If you created subdirectories when you ran GIFT, scaling_components files are in a subdirectory labeled *scaling_components_files. 
            The files are named like this: sub001_component_ica_s1_.nii
            This assumes the files are NOT zipped!
            ------------------------
            List of the ICs to extract: e.g., lst_nad2_learn_ICs.txt
            The list is a text file containing the numerical values of the ICs of interest.
            The list should contain one IC per row, e.g.:
            23
            11
            14

            For convenience, place the list in the parent directory of group. 
            ------------------------
    		    Pass in 2 arguments to ICextract.sh:
            1) the name of this function: ExtractICsIndiv
            2) the path to the list: ../lst_nad2_learn_ICs.txt
            e.g., ICextract.sh ExtractICsIndiv ../lst_nad2_learn_ICs.txt

Output:  *.nii.gz files for each 3d volume of interest): e.g.,
         IC9_run1.nii.gz 

COMMENTBLOCK

ExtractICIndiv ()
{
  for ic in sub*component*.nii; do
    # Get the name of the 4d image using FSL's remove_ext function
    icbase=$(remove_ext ${ic})
    gift_4dimg=${icbase}.nii
     # extract subject for naming. Assume it is the 1st value in the name
    # where values are separated by underscores
    sub=$(echo ${ic} | sed 's/_/ /g' | awk '{printf $1 "\n"}')
    echo "sub=${sub}"

    # extract session (run) for naming. Assume it is the 4th value in the name
    # where values are separated by underscores
    run=$(echo ${ic} | sed 's/_/ /g' | sed 's/s//g' | awk '{printf $4 "\n"}')
    echo "run=${run}"
   
    # Construct the output name
    outname=${sub}_run${run}

    # For each IC number in the list, extract the data and create the file
    for ICnum in $(cat ${iclist}); do
      ExtractIC ${gift_4dimg} ${ICnum} ${outname}
    done
  done
}
#==============================================================================
: <<COMMENTBLOCK

Function:   ExtractIC
Purpose:    Extract the correct image from the 4D IC
Input:      Three arguments
            1) The name of the 4d image to process
            2) The number of the IC to extract
            3) the output name (e.g. fred) to be appended to the IC: IC4_fred
Caller:     ExtractICGroup and ExtractICIndiv
COMMENTBLOCK

ExtractIC ()
{
  if [ $# -lt 3 ]; then
    echo "Extract expects 3 arguments:"
    echo "1) The 4D image to extract from, 2) the IC number to extract, 3) an output basename"
    echo "e.g. Extract myinputimg 3 fred"
    echo "would create the output: IC3_fred.nii.gz"
    exit 1
  fi

  gift_4dimg=${1}
  ICnum=${2}
  outname=${3}
  im=$(echo "${ICnum}-1" | bc )  # $im is the image index of the IC (one-off)
  echo "----------------------------------"
  echo "IC = ${im}"
  echo "creating IC${ICnum}_${outname}"

  #extract one image at the appropriate index
  fslroi ${gift_4dimg} IC${ICnum}_${outname} ${im} 1
  echo "fslroi ${gift_4dimg} IC${ICnum}_${outname} ${im} 1"
}

#==============================================================================
: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Print relevant help message

COMMENTBLOCK

HelpMessage ()
{
  echo "       "
  echo "$0 is used to extract 3D IC images from the 4D stats files generated by GIFT"
  echo "It assumes the GIFT naming structure has not changed, because it extracts"
  echo "strings from the stat file names by position."
  echo " In particular the run number, e.g., s2 must be in the 4th position in the filename divided by underscores"
  echo ""
  echo "It also assumes you are in a directory containing all and only"
  echo "the nii files you wish to process"
  echo ""
  echo "There are two options: ExtractICGroup and ExtractICIndiv"
  echo "------------------------"
  echo "Both assume a list of ICs to extract"
  echo "List of the ICs to extract: e.g., lst_nad2_learn_ICs.txt"
  echo "The list is a text file containing the numerical values of the ICs of interest."
  echo "The list should contain one IC per row, e.g.:"
  echo "23"
  echo "11"
  echo "14"
  echo "For convenience, place the list in the parent directory of group." 
  echo "------------------------"
  echo "example calls:"
  echo "ICextract.sh ExtractICGroup ../lst_nad2_learn_ICs.txt"
  echo "ICextract.sh ExtractICIndiv ../lst_nad2_learn_ICs.txt"
  echo ""
  echo "Output files are labelled IC*run*.nii.gz,"
  echo "e.g. Group: IC4_run2.nii.gz or Indiv: IC4_sub009_run2.nii.gz"
  echo "ExtractICGroup can take a third optional argument specifying an output name"
  echo "e.g., ICextract.sh ExtractICsGroup ../lst_nad2_learn_ICs.txt unlearn"
  echo "in this case, output files will be named like IC4_run2_unlearn.nii.gz"
  echo "================================ "
}

#==============================================================================

while getopts h options 2> /dev/null; do        # Setup -h flag. redirect errors
case $options in                            # In case someone invokes -h
  h) HelpMessage;;                            # Run the help message function
  \?) echo "only h is a valid flag" 1>&2      # If bad options are passed, print message
esac
done

shift $(( $OPTIND -1))                          # Index option count, so h isn't treated as arg
optnum=$(( $OPTIND -1))                         # optnums hold # of options passed in

if [ $# -lt 1 ]; then
  HelpMessage
fi  

if [ ${optnum} -lt 1 ]              		    # If no options or args
  then
    dothis=$1                           	  # Run the function indicated
    # Run the function as indicated
    echo "${dothis}"

  # if the function takes an argument, set the argument here so we can pass it in at the commandline
  if  [[ ${dothis} = ExtractICIndiv ]]; then
    iclist=$2
    echo ${iclist}
  fi

  if  [[ ${dothis} = ExtractICGroup ]]; then
  iclist=$2
  echo ${iclist}”
  fi
fi

$dothis                                      # Run the selected choice
