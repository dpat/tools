#!/bin/bash

: <<COMMENTBLOCK
get volume and COG (center of gravity) stats for a mask
COMMENTBLOCK

mask=$(remove_ext "$1")

if [ ! -e "stats_lesions.tsv" ]; then
  # Tell echo to honor backslashed characters (tab, in this case)
  echo -e "mask\tvolmm\tCOG_x\tCOG_y\tCOG_z" > stats_lesions.tsv
fi

# Get volume in cubic mm
vol=`fslstats ${mask} -V | awk '{printf $2 "\n"}'`
# get center of gravity in mm coordinates
# all positive values which facilitate comparisons
COG_x=$(fslstats ${mask} -c | awk '{printf $1 "\n"}')
COG_y=$(fslstats ${mask} -c | awk '{printf $2 "\n"}')
COG_z=$(fslstats ${mask} -c | awk '{printf $3 "\n"}')

printf  "%s\t%s\t%s\t%s\t%s\t%s\n" ${mask} ${vol} ${COG_x} ${COG_y} ${COG_z} >> stats_lesions.tsv
