#!/bin/bash

: <<COMMENTBLOCK
copy sform to qform for all NIFTI images in the current directory.
Note that the s-form can be larger and more complex than the q-form, so even afer copying, they may not be equivalent.
COMMENTBLOCK

# If there is an argument
if [ $# -eq 1 ]; then
  img=$1
  fslorient -copysform2qform ${img}
  sqdiff.sh ${img}
else # if there are no arguments
  echo "Run s2q.sh on all NIFTI images in this directory?"
  echo "yes to continue"
  echo "hit any other key to abort and view help."
  read answer
  if [ "$answer" = "yes" ]; then
    echo "applying s2q to all image files in the directory"
    for img in *.nii *.nii.gz; do
        if [ -e  ${img} ]; then
          echo "image is ${img}"
          fslorient -copysform2qform ${img}
          sqdiff.sh ${img}
        fi
    done
  else
    echo "HELP on s2q.sh"
    echo "===================="
    echo "With one image file as the argument,"
    echo "e.g., s2q.sh mask"
    echo "s2q.sh copies the sform to the qform for that image."
    echo "--------------------"
    echo "With no arguments, s2q.sh asks what you want."
    echo "IF you answer yes,"
    echo "then s2q.sh runs on all NIFTI images in the current directory."
    echo "Otherwise, hit any key to see this help message."
    echo ""
    echo "Note that the s-form can be larger and more complex than the q-form,"
    echo "This is because the s-form can handle non-rigid body transformations"
    echo "like scale and shear."
    echo "So even afer copying s to q, they may not be equivalent."
  fi
fi
