#!/bin/bash

: <<COMMENTBLOCK
copy qform to sform for all NIFTI images in the current directory.
The qform will always fit in the s-form, but not vice versa.
COMMENTBLOCK

# If there is an argument
if [ $# -eq 1 ]; then
  img=$1
  fslorient -copyqform2sform ${img}
  #sqdiff.sh ${img}
else # if there are no arguments
  echo "Run q2s.sh on all NIFTI images in this directory?"
  echo "yes to continue"
  echo "hit any other key to abort and view help."
  read answer
  if [ "$answer" = "yes" ]; then
    echo "applying q2s to all image files in the directory"
    for img in *.nii *.nii.gz; do
      if [ -e  ${img} ]; then
        echo "image is ${img}"
        fslorient -copyqform2sform ${img}
        sqdiff.sh ${img}
      fi
    done
  else
    echo "HELP on q2s.sh"
    echo "===================="
    echo "With one image file as the argument,"
    echo "e.g., q2s.sh mask"
    echo "q2s.sh copies the qform to the sform for that image."
    echo "--------------------"
    echo "With no arguments, q2s.sh asks what you want."
    echo "IF you answer yes,"
    echo "then q2s.sh runs on all NIFTI images in the current directory."
    echo "Otherwise, hit any key to see this help message."
    echo ""
    echo "The qform will always fit in the s-form, but not vice versa."
    echo "So, the s-form and q-form should be equivalent after applying this"
    echo "but, you may lose scale and shear information."
  fi
fi
