
% Wrapper script to run toMatIC.  Passes in wb1 image arrays.
% Creates vectors and adjacency matrices and saves them with unique names.

sessArray={'s1','s2','s3'};
icArray={'IC25','IC2','IC7','IC25','IC31', 'IC39'};

for sessPrefix = (sessArray);
        % The dataset suffixes
         for icSuffix = (icArray);
            % Concatenate stem and suffix to create the dataset names
            input_image=char(strcat((sessPrefix), '_', (icSuffix), '_msk_4mm'));
            outmat=char(strcat('adj_',(sessPrefix), '_', (icSuffix)));
            outvec=char(strcat('vec_',(sessPrefix), '_', (icSuffix)));
            [imgVec, adjMat]=toMatIC (input_image);
            eval([sprintf(outmat),'=adjMat;']);
            eval([sprintf(outvec),'=imgVec;']);
            clear adjMat;
        end;       
end

clear sessPrefix
for sessPrefix = (sessArray);             
    s=sprintf(sessPrefix{1});
    sessadj=sprintf('adj_%s*_*', s);
    sessvec=sprintf('vec_%s*_*',s);
    save (s, sessadj, sessvec); 
    save (s, sessadj, sessvec); 
end


