#!/bin/bash
# Scale an image range to 0-1000

if [ $# -lt 1 ] ; then
  echo "Usage: $0 <filename>" ;
  exit 1 ;
fi

input=`remove_ext ${1}`

min=`fslstats ${input} -R | awk '{ print $1 }'`
max=`fslstats ${input} -R | awk '{ print $2 }'`

scaling=`echo "scale=5; 1000.0 / ( $max - $min )" | bc`

fslmaths ${input} -sub $min -mul $scaling ${input}_scaled
# If you want all values to be ints, you can do this:
#fslmaths ${input}_scaled ${input}_int_scaled -odt int
