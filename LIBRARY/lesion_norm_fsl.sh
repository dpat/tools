#!/bin/bash

: <<COMMENTBLOCK
Calls fsl_anat_alt.sh and optiBET.sh to run cleanup and normalization to standard MNI space.
Assume we are in the directory with the images we need
Pass in 2 arguments:
1) a T1 anatomical image
2) a lesion mask (1=lesion; 0=non-lesion)
e.g. lesion_norm_fsl.sh sub-001.nii.gz sub-001_LesionSmooth.nii.gz
COMMENTBLOCK

# If there are less than 2 arguments
if [ $# -lt 2 ] ; then
  echo "Usage: $0 <T1w_image> <lesion_mask>"
  echo "e.g. lesion_norm_fsl.sh sub-001.nii.gz sub-001_LesionSmooth.nii.gz"
  echo "This script calls fsl_anat_alt.sh and optibet.sh to clean up the T1w image"
  echo "and create normalized T1w and lesion images"
  exit 1 ;
fi

image=$1
lesion_mask=$2
stem=`remove_ext ${image}`


if [ ! -d ${stem}.anat ]; then
  # run our super spiffy fsl_anat_alt.sh with optiBET.sh
  fsl_anat_alt.sh -i ${stem} -m ${lesion_mask} --nosubcortseg
fi
  # copy the head warped into mni space from the subdir
  imcp ${stem}.anat/T1_to_MNI_nonlin.nii.gz ${stem}_MNI_head
  # copy the best native space brain mask from the subdir.
  # This results from fsl_anat not just optibet.
  imcp ${stem}.anat/T1_biascorr_brain_mask.nii.gz ${stem}_brain_mask.nii.gz

  #### Normalize everything ####
  # warp the lesion mask into MNI space, use nearest neighbour interpolation
  applywarp -i ${stem}.anat/lesionmask -w ${stem}.anat/T1_to_MNI_nonlin_field --interp=nn -r  $FSLDIR/data/standard/MNI152_T1_2mm -o ${stem}_MNI_lesion
  # warp the brain mask into MNI space, use nearest neighbour interpolation
  applywarp -i ${stem}_brain_mask.nii.gz  -w ${stem}.anat/T1_to_MNI_nonlin_field --interp=nn -r $FSLDIR/data/standard/MNI152_T1_2mm -o ${stem}_MNI_brain_mask
  # warp the skull stripped brain into MNI space
  applywarp -i ${stem}.anat/T1_biascorr_brain -w ${stem}.anat/T1_to_MNI_nonlin_field -r $FSLDIR/data/standard/MNI152_T1_2mm -o ${stem}_MNI_brain
  # create the binary version of the MNI lesion mask (mostly harmless, though I think
  # not needed since we are doing nearest neighbour interpolation).
  fslmaths ${stem}_MNI_lesion -thr 0.5 -bin ${stem}_MNI_lesion -odt char
