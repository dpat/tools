#!/bin/bash

: <<COMMENTBLOCK
Renaming rotman data  temporary file.
COMMENTBLOCK

# This is the subject directory
SUBJECTDIR=`pwd`
bids_dir=`dirname $SUBJECTDIR`
subj=`basename $SUBJECTDIR`
output_dir=${bids_dir}/derivatives/${subj}

echo "subjectdir is ${subj}"
echo "OUT is ${output_dir}"

fslroi ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz ${output_dir}/dwi/nodif.nii.gz 0 1

  mv ${OUT}/${subj}/anat/${subj}_T1w_cropped_defaced.nii.gz
  ${subj}/anat/${subj}_T1w.nii.gz


# DWI cleanup
mv ${OUT}/${subj}/dwi/Bed.bedpostX ${OUT}/${subj}/dwi.bedpostX



##########################
# ${SUBJECT}_test.txt

#ak1=/Volumes/Main/Exps/Analysis/TRACT_DISPLAY
#dak=/Volumes/Main/Exps/Data/DTI_BIP_CLEAN/YC

#MkDirIfNot derivatives
#MkDirIfNot sourcedata
#ARCHIVE=/Volumes/Main/Exps/Data/BIP_ROTMAN/Archive_Rotman

# Rename anatomy files in main directory
#rename_all.sh _struct _T1w "anat/${SUBJECT}_struct*"
#rename_all.sh _mprage _T1w "anat/${SUBJECT}_mprage*"

# Rename anatomy files in archive using fe_arch_oc (ppa, stp or yc)
#rename_all.sh _struct _T1w "${SUBJECT}_struct*"
#rename_all.sh _mprage _T1w "${SUBJECT}_mprage*"

#clean up raw directory:
# rm -fr reg regtest stats subjectrois
# rm anat/*T1w_*
# rm dwi/*B0*
# rm -fr dwi/Bed

# work on derivatives directory
#ls ../derivatives/${SUBJECT}
# rm ../derivatives/${SUBJECT}/anat/*T1w.nii.gz
# rm ../derivatives/${SUBJECT}/dwi/*dwi.nii.gz
# rm ../derivatives/${SUBJECT}/dwi/*.bv*

# For stp, rename anatomy files in derivatives directory, clean raw anat files
# rename_all.sh _struct _T1w "../derivatives/${SUBJECT}/anat/${SUBJECT}_struct*"
# rename_all.sh _mprage _T1w "../derivatives/${SUBJECT}/anat/${SUBJECT}_mprage*"
# rm ../derivatives/${SUBJECT}/anat/*T1w.nii.gz
# rm -fr anat/${SUBJECT}_T1w.anat
# rm anat/*cropped*
# mv ${SUBJECT}_archive ../derivatives/${SUBJECT}/

#mango_bet result updates
#cp ../../mango_bet/${SUBJECT}_T1w_mask.nii.gz ../derivatives/${SUBJECT}/anat/
#cp ../../mango_bet/${SUBJECT}_B0_brain_mask.nii.gz ../derivatives/${SUBJECT}/dwi/
#rm ../derivatives/${SUBJECT}/dwi/${SUBJECT}_B0_brain.nii.gz
#fslmaths ../derivatives/${SUBJECT}/dwi/${SUBJECT}_B0 -mul ../derivatives/${SUBJECT}/dwi/${SUBJECT}_B0_brain_mask ../derivatives/${SUBJECT}/dwi/${SUBJECT}_B0_brain

#fslmaths ../derivatives/${SUBJECT}/anat/${SUBJECT}_T1w_cropped_defaced -mul ../derivatives/${SUBJECT}/anat/${SUBJECT}_T1w_mask ../derivatives/${SUBJECT}/anat/${SUBJECT}_T1w_brain

# ls ../derivatives/${SUBJECT}/anat/*brain.nii.gz
# echo "-----------------------"
# ls ../derivatives/${SUBJECT}/dwi/*brain.nii.gz

# Chou data
#mkdir ../derivatives/${SUBJECT}
#mkdir ../sourcedata/${SUBJECT}

#mv Text ../sourcedata/${SUBJECT}
# mkdir anat fmap dwi
# mkdir ../derivatives/${SUBJECT}/anat
# mkdir ../derivatives/${SUBJECT}/dwi
# mkdir ../derivatives/${SUBJECT}/fmap
#
# mv *deface* ../derivatives/${SUBJECT}/anat
# mv *reoriented* ../derivatives/${SUBJECT}/anat
# mv *flair.nii.gz anat
# mv *spgr.nii.gz anat
# mv *mag* fmap
# mv *phase* fmap
# mv *dwi.nii.gz dwi

# if [ -d sess-01 ]; then
#   #cp -fr anat fmap dwi sess-01
#   #mkdir sess-02/anat sess-02/fmap sess-02/dwi
#   # mkdir -p ../derivatives/${SUBJECT}/sess-01/anat
#   # mkdir -p ../derivatives/${SUBJECT}/sess-01/dwi
#   # mkdir -p ../derivatives/${SUBJECT}/sess-01/fmap
#   # mkdir -p ../derivatives/${SUBJECT}/sess-02/anat
#   # mkdir -p ../derivatives/${SUBJECT}/sess-02/dwi
#   # mkdir -p ../derivatives/${SUBJECT}/sess-02/fmap
#   # rm -fr ../derivatives/${SUBJECT}/anat
#   # rm -fr ../derivatives/${SUBJECT}/fmap
#   # rm -fr ../derivatives/${SUBJECT}/dwi
#
# mv sess-02/*reorient* ../derivatives/${SUBJECT}/sess-02/anat
# mv sess-02/*flair* sess-02/anat
# mv sess-02/*spgr* sess-02/anat
# mv sess-02/*mag* sess-02/fmap
# mv sess-02/*phase* sess-02/fmap
# mv sess-02/*dwi* sess-02/dwi
#
# fi

# The first image in the directory should be a magnitude image
# mag_vol1=`ls -1 fmap | head -n 1`
# # The last image in the directory should be the phase image
# phase=`ls -1 fmap | tail -n 1`
# mag1_stem=`basename -s .nii.gz ${mag_vol1}`
# phase_stem=`basename -s .nii.gz ${phase}`
# #json/${mag1_stem}.json
# #json/${phase_stem}.json
#
# ET1=`cat json/${mag1_stem}.json | grep -m 1 EchoTime | sed 's/,//g' | awk '{print $2}'`
# echo "ET1 is ${ET1}"
# ET2=`cat json/${phase_stem}.json | grep -m 1 EchoTime | sed 's/,//g' | awk '{print $2}'`
# echo "ET2 is ${ET2}"
#
# echodiff=`echo "scale=2; (${ET2} - ${ET1}) * 1000" | bc`
# echo "echodiff is ${echodiff}"

# for fmri_base in func/*basevol.nii.gz; do
#   fmr_stem=`basename -s _basevol.nii.gz ${fmri_base}`
#   EES=`cat json/${fmr_stem}.json | grep -m 1 EffectiveEchoSpacing | sed 's/,//g' | awk '{print $2}'`


# This is BBR. The values are correct for our scanner.
# It will optimize registration of the functional to the structural
#   epi_reg --fmap=fmap/${SUBJECT}_fmap_rads \
#   --fmapmag=fmap/${SUBJECT}_mag_vol1 \
#   --fmapmagbrain=fmap/${SUBJECT}_mag_brain \
#   --pedir=-y --echospacing=0.000325 -v \
#   --epi=${fmri_base} \
#   --t1=anat/${SUBJECT}_mprage_cropped \
#   --t1brain=anat/${SUBJECT}_mprage_brain \
#   --out=reg/${fmr_stem}_2T1w
# # Both a linear flirt registration (*.mat) and a warp file (*.nii.gz) are generated
# # These are register each fmri to the mprage space
# # We want to create the warp from fmri space to standard space (-o=out)
# convertwarp -r ${stand} -w reg/T1w2mni -m reg/${fmr_stem}_2T1w.mat -o reg/${fmr_stem}_2mni
# # And we want the inverse image to warp mni space into fmri space (well, maybe you do)
# # Comment this out the invwarp statement if you don't care.
# invwarp -w reg/${fmr_stem}_2mni -o reg/mni2_${fmr_stem} -r ${fmri_base}
#done

#Check defacing

# if [ ! -d sess-01 ]; then
#   if [ -e anat/*spgr.nii.gz ]; then
#     echo "SPGR DEFACING"
#     fsleyes ../derivatives/${SUBJECT}/anat/*spgr_reoriented.nii.gz ../derivatives/${SUBJECT}/anat/**spgr_reoriented_deface_diff.nii.gz -cm Red
#   fi
#   if [ -e anat/*flair.nii.gz ]; then
#     echo "FLAIR DEFACING"
#     fsleyes ../derivatives/${SUBJECT}/anat/*flair_reoriented.nii.gz ../derivatives/${SUBJECT}/anat/**flair_reoriented_deface_diff.nii.gz -cm Red
#   fi
# elif [ -d sess-01 ]; then
#   for sess in sess-01 sess-02; do
#     if [ -e ${sess}/anat/*spgr.nii.gz ]; then
#       echo "SPGR DEFACING: ${sess}"
#       fsleyes ../derivatives/${SUBJECT}/${sess}/anat/*spgr_reoriented.nii.gz ../derivatives/${SUBJECT}/${sess}/anat/**spgr_reoriented_deface_diff.nii.gz -cm Red
#     fi
#     if [ -e sess-01/anat/*flair.nii.gz ]; then
#       echo "FLAIR DEFACING: ${sess}"
#       fsleyes ../derivatives/${SUBJECT}/${sess}/anat/*flair_reoriented.nii.gz ../derivatives/${SUBJECT}/${sess}/anat/**flair_reoriented_deface_diff.nii.gz -cm Red
#     fi
#   done
# fi


#echo "Hey, this won't work unless the names are fully specified, and I want to remove the scan numbers first. "
# if [ ! -d sess-01 ]; then
#   if [ -e anat/*spgr.nii.gz ]; then
#     echo "SPGR DEFACE MASK"
#     fslmaths ../derivatives/${SUBJECT}/anat/${SUBJECT}_spgr_reoriented_defaced.nii.gz ../derivatives/${SUBJECT}/anat/${SUBJECT}_spgr_reoriented_defaced.nii.gz -odt short
#
#     fslmaths ../derivatives/${SUBJECT}/anat/${SUBJECT}_spgr_reoriented_deface_diff.nii.gz -bin
#     ../derivatives/${SUBJECT}/anat/${SUBJECT}_spgr_reoriented_deface_diff_mask.nii.gz -odt short
#   fi
#   if [ -e anat/*flair.nii.gz ]; then
#     echo "FLAIR DEFACE MASK"
#     fslmaths ../derivatives/${SUBJECT}/anat/${SUBJECT}_flair_reoriented_defaced.nii.gz ../derivatives/${SUBJECT}/anat/${SUBJECT}_flair_reoriented_defaced.nii.gz -odt short
#
#     fslmaths ../derivatives/${SUBJECT}/anat/${SUBJECT}_flair_reoriented_deface_diff.nii.gz -bin
#     ../derivatives/${SUBJECT}/anat/${SUBJECT}_flair_reoriented_deface_diff_mask.nii.gz -odt short
#   fi
# elif [ -d sess-01 ]; then
#   for sess in sess-01 sess-02; do
#     if [ -e ${sess}/anat/*spgr.nii.gz ]; then
#       echo "SPGR DEFACE MASK: ${sess}"
#
#       fslmaths ../derivatives/${SUBJECT}/${sess}/anat/${SUBJECT}_spgr_reoriented_deface_diff.nii.gz ../derivatives/${SUBJECT}/${sess}/anat/${SUBJECT}_spgr_reoriented_deface_diff.nii.gz -odt short
#
#       fslmaths ../derivatives/${SUBJECT}/${sess}/anat/${SUBJECT}_spgr_reoriented_deface_diff.nii.gz ../derivatives/${SUBJECT}/${sess}/anat/${SUBJECT}_spgr_reoriented_deface_diff_mask.nii.gz -odt short
#     fi
#     if [ -e sess-01/anat/*flair.nii.gz ]; then
#       echo "FLAIR DEFACE MASK: ${sess}"
#       fslmaths ../derivatives/${SUBJECT}/${sess}/anat/${SUBJECT}_flair_reoriented_defaced.nii.gz
#       ../derivatives/${SUBJECT}/${sess}/anat/${SUBJECT}_flair_reoriented_defaced.nii.gz -odt short
#
#       fslmaths ../derivatives/${SUBJECT}/${sess}/anat/${SUBJECT}_flair_reoriented_deface_diff.nii.gz -bin
#       ../derivatives/${SUBJECT}/${sess}/anat/${SUBJECT}_flair_reoriented_deface_diff_mask.nii.gz -odt short
#     fi
#   done
# fi

#zip -r study_${SUBJECT}.zip study
#fslinfo anat/${SUBJECT}_mprage_brain.nii.gz
#fslinfo anat/${SUBJECT}_mprage_cropped.nii.gz
#fslinfo anat/${SUBJECT}_mprage_mask.nii.gz
#fslmaths anat/${SUBJECT}_mprage_cropped.nii.gz anat/${SUBJECT}_mprage_cropped.nii.gz -odt short
#fslmaths anat/${SUBJECT}_mprage_mask.nii.gz anat/${SUBJECT}_mprage_mask.nii.gz -odt short
#rm anat/${SUBJECT}_mprage_brain.nii.gz
#fslmaths anat/${SUBJECT}_mprage_cropped.nii.gz -mul anat/${SUBJECT}_mprage_mask.nii.gz  anat/${SUBJECT}_mprage_brain.nii.gz -odt short
#fslmaths fmap/${SUBJECT}_mag_brain.nii.gz fmap/${SUBJECT}_mag_brain.nii.gz -odt short

# Rename anatomy files in main directory
#rename_all.sh _struct _T1w "anat/${SUBJECT}_struct*"
#rename_all.sh _mprage _T1w "anat/${SUBJECT}_mprage*"

# Rename anatomy files in archive using fe_arch_oc (ppa, stp or yc)
#rename_all.sh _struct _T1w "${SUBJECT}_struct*"
#rename_all.sh _mprage _T1w "${SUBJECT}_mprage*"

# Rename anatomy files in archive using fe_arch_oc (ppa, stp or yc)
#rename_all.sh _struct _T1w "${SUBJECT}_struct*"
#rename_all.sh _mprage _T1w "${SUBJECT}_mprage*"

# if [ ! -d sess-01 ]; then
#   if [ -e anat/*spgr.nii.gz ]; then
#     echo "SPGR DEFACE MASK"
#     fslmaths ../derivatives/${SUBJECT}/anat/${SUBJECT}_spgr_reoriented_defaced.nii.gz ../derivatives/${SUBJECT}/anat/${SUBJECT}_spgr_reoriented_defaced.nii.gz -odt short
#
#     fslmaths ../derivatives/${SUBJECT}/anat/${SUBJECT}_spgr_reoriented_deface_diff.nii.gz -bin
#     ../derivatives/${SUBJECT}/anat/${SUBJECT}_spgr_reoriented_deface_diff_mask.nii.gz -odt short
#   fi
#   if [ -e anat/*flair.nii.gz ]; then
#     echo "FLAIR DEFACE MASK"
#     fslmaths ../derivatives/${SUBJECT}/anat/${SUBJECT}_flair_reoriented_defaced.nii.gz ../derivatives/${SUBJECT}/anat/${SUBJECT}_flair_reoriented_defaced.nii.gz -odt short
#
#     fslmaths ../derivatives/${SUBJECT}/anat/${SUBJECT}_flair_reoriented_deface_diff.nii.gz -bin
#     ../derivatives/${SUBJECT}/anat/${SUBJECT}_flair_reoriented_deface_diff_mask.nii.gz -odt short
#   fi
# elif [ -d sess-01 ]; then
#   for sess in sess-01 sess-02; do
#     if [ -e ${sess}/anat/*spgr.nii.gz ]; then
#       echo "SPGR DEFACE MASK: ${sess}"
#
#       fslmaths ../derivatives/${SUBJECT}/${sess}/anat/${SUBJECT}_spgr_reoriented_deface_diff.nii.gz ../derivatives/${SUBJECT}/${sess}/anat/${SUBJECT}_spgr_reoriented_deface_diff.nii.gz -odt short
#
#       fslmaths ../derivatives/${SUBJECT}/${sess}/anat/${SUBJECT}_spgr_reoriented_deface_diff.nii.gz ../derivatives/${SUBJECT}/${sess}/anat/${SUBJECT}_spgr_reoriented_deface_diff_mask.nii.gz -odt short
#     fi
#     if [ -e sess-01/anat/*flair.nii.gz ]; then
#       echo "FLAIR DEFACE MASK: ${sess}"
#       fslmaths ../derivatives/${SUBJECT}/${sess}/anat/${SUBJECT}_flair_reoriented_defaced.nii.gz
#       ../derivatives/${SUBJECT}/${sess}/anat/${SUBJECT}_flair_reoriented_defaced.nii.gz -odt short
#
#       fslmaths ../derivatives/${SUBJECT}/${sess}/anat/${SUBJECT}_flair_reoriented_deface_diff.nii.gz -bin
#       ../derivatives/${SUBJECT}/${sess}/anat/${SUBJECT}_flair_reoriented_deface_diff_mask.nii.gz -odt short
#     fi
#   done
# fi
