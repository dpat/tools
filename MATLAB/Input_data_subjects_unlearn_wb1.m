% Enter the values for the variables required for the ICA analysis.
% Variables are on the left and the values are on the right.
% Characters must be entered in single quotes
%
% After entering the parameters, type
% icatb_batch_file_run('Input_data_subjects_unlearn_wb1.m')

%% Modality. Options are fMRI and EEG
modalityType = 'fMRI';

%% Type of analysis
% Options are 1 and 2.
% 1 - Regular Group ICA
% 2 - Group ICA using icasso
which_analysis = 2;

%% ICASSO options.
% This variable will be used only when which_analysis variable is set to 2.
icasso_opts.sel_mode = 'both';  % Options are 'randinit', 'bootstrap' and 'both'
icasso_opts.num_ica_runs = 10; % Number of times ICA will be run
% Most stable run estimate is based on these settings. 
icasso_opts.min_cluster_size = 2; % Minimum cluster size
icasso_opts.max_cluster_size = 15; % Max cluster size. Max is the no. of components


%% Group PCA performance settings. Best setting for each option will be selected based on variable MAX_AVAILABLE_RAM in icatb_defaults.m. 
% If you have selected option 3 (user specified settings) you need to manually set the PCA options. 
%
% Options are:
% 1 - Maximize Performance
% 2 - Less Memory Usage
% 3 - User Specified Settings
perfType = 3;


%% Design matrix selection
% Design matrix (SPM.mat) is used for sorting the components
% temporally (time courses) during display. Design matrix will not be used during the
% analysis stage except for SEMI-BLIND ICA.
% options are ('no', 'same_sub_same_sess', 'same_sub_diff_sess', 'diff_sub_diff_sess')
% 1. 'no' - means no design matrix.
% 2. 'same_sub_same_sess' - same design over subjects and sessions
% 3. 'same_sub_diff_sess' - same design matrix for subjects but different
% over sessions
% 4. 'diff_sub_diff_sess' - means one design matrix per subject.

keyword_designMatrix = 'same_sub_same_sess';

% specify location of design matrix here if you have selected 'same_sub_same_sess' or
% 'same_sub_diff_sess' option for keyword_designMatrix variable
% OnedesignMat = 'C:\MATLAB6p5p2\work\Example Subjects\Visuomotor_data\SPM.mat';
OnedesignMat = '/usr/local/tools/REF/FUNC/SPM.mat';


%% There are three ways to enter the subject data
% options are 1, 2, 3 or 4
dataSelectionMethod = 1;

%% Method 1

% If you have all subjects in one directory and their sessions in a separate folder or in the subject folder then specify 
% root directory, filePattern, flag and file numbers to include.
% Options for flag are: data_in_subject_folder, data_in_subject_subfolder
%
% 1. data_in_subject_subfolder - Data is selected from the subject sub
% folders. Number of sessions is equal to the number of sub-folders
% containing the specified file pattern.
%
% 2. data_in_subject_folder - Data is selected from the subject
% folders. Number of sessions is 1 and number of subjects is equal to the number of subject folders
% containing the specified file pattern.

% You can provide the file numbers ([1:220]) to include as a vector. If you want to
% select all the files then leave empty.

% Note: Make sure the sessions are the same over subjects.

sourceDir_filePattern_flagLocation = {'/Exps/Analysis/WB1/BatchA/Unlearnable/Subjects', 'run*.nii', ...
        'data_in_subject_subfolder'};

% Specify design matrix filter pattern here if you have selected
% 'diff_sub_diff_sess' option for keyword_designMatrix variable for Method 1. It looks
% for the design matrix in the respective subject folder or session folders.
% spmDesignFilter = 'SPM*.mat';    

%%%%%%%%%%%%%%%%%%%%%%%%% end for Method 1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Enter directory to put results of analysis
outputDir = '/Exps/Analysis/WB1/GiftAnalysis/Unlearnable';

%% Enter Name (Prefix) Of Output Files
prefix = 'Unlearnable';

%% Enter location (full file path) of the image file to use as mask
% or use Default mask which is []
maskFile = [];

%% Group PCA Type. Used for analysis on multiple subjects and sessions.
% Options are 'subject specific' and 'grand mean'. 
%   a. Subject specific - Individual PCA is done on each data-set before group
%   PCA is done.
%   b. Grand Mean - PCA is done on the mean over all data-sets. Each data-set is
%   projected on to the eigen space of the mean before doing group PCA.
%
% NOTE: Grand mean implemented is from FSL Melodic. Make sure that there are
% equal no. of timepoints between data-sets.
%
group_pca_type = 'subject specific';

%% Back reconstruction type. Options are str and gica
% 1 - Regular
% 2 - Spatial-temporal regression 
% 3 - GICA3
% 4 - GICA

backReconType = 3;

%% Data Pre-processing options
% 1 - Remove mean per time point
% 2 - Remove mean per voxel
% 3 - Intensity normalization
% 4 - Variance normalization
preproc_type = 1;


%% PCA Type. Also see options associated with the selected pca option. EM
% PCA options and SVD PCA are commented.
% Options are 1, 2 and 3
% 1 - Standard 
% 2 - Expectation Maximization
% 3 - SVD
pcaType = 2;

%% PCA options (Standard)

% a. Options are yes or no
% 1a. yes - Datasets are stacked. This option uses lot of memory depending
% on datasets, voxels and components.
% 2a. no - A pair of datasets are loaded at a time. This option uses least
% amount of memory and can run very slower if you have very large datasets.
% pca_opts.stack_data = 'yes';

% b. Options are full or packed.
% 1b. full - Full storage of covariance matrix is stored in memory.
% 2b. packed - Lower triangular portion of covariance matrix is only stored in memory.
% pca_opts.storage = 'full';

% c. Options are double or single.
% 1c. double - Double precision is used
% 2c. single - Floating point precision is used.
% pca_opts.precision = 'double';

% d. Type of eigen solver. Options are selective or all
% 1d. selective - Selective eigen solver is used. If there are convergence
% issues, use option all.
% 2d. all - All eigen values are computed. This might run very slow if you
% are using packed storage. Use this only when selective option doesn't
% converge.

% pca_opts.eig_solver = 'selective';


% %% PCA Options (Expectation Maximization)
% % a. Options are yes or no
% % 1a. yes - Datasets are stacked. This option uses lot of memory depending
% % on datasets, voxels and components.
% % 2a. no - A pair of datasets are loaded at a time. This option uses least
% % amount of memory and can run very slower if you have very large datasets.
pca_opts.stack_data = 'yes';
% 
% % b. Options are double or single.
% % 1b. double - Double precision is used
% % 2b. single - Floating point precision is used.
pca_opts.precision = 'double';
% 
% % c. Stopping tolerance 
pca_opts.tolerance = 1e-4;
% 
% % d. Maximum no. of iterations
pca_opts.max_iter = 1000;


% %% PCA Options (SVD)
% % a. Options are double or single.
% % 1a. double - Double precision is used
% % 2a. single - Floating point precision is used.
% pca_opts.precision = 'single';

% % b. Type of eigen solver. Options are selective or all
% % 1b. selective - svds function is used.
% % 2b. all - Economy size decomposition is used.
% pca_opts.solver = 'selective';


%% Maximum reduction steps you can select is 3
% Note: This number will be changed depending upon the number of subjects
% and sessions
numReductionSteps = 2;

%% Batch Estimation. If 1 is specified then estimation of  
% the components takes place and the corresponding PC numbers are associated 
% Options are 1 or 0
doEstimation = 1;  

% In batch script estimation is done on all data-sets if
% doEstimation variable is set to 1. You could use the following command to run
% the estimation on a specified subject and set the principal components to the
% estimated number.

% num_components = icatb_estimate_dimesion(file_name); 


%% MDL Estimation options. This variable will be used only if doEstimation is set to 1.
% Options are 'mean', 'median' and 'max' for each reduction step. The length of cell is equal to
% the no. of data reductions used.
estimation_opts.PC1 = 'mean';
estimation_opts.PC2 = 'mean';
estimation_opts.PC3 = 'mean';

%% Number of pc to reduce each subject down to at each reduction step
% The number of independent components the will be extracted is the same as 
% the number of principal components after the final data reduction step.  
numOfPC1 = 20;
numOfPC2 = 16;
numOfPC3 = 16;


%% Scale the Results. Options are 0, 1, 2, 3 and 4
% 0 - Don't scale
% 1 - Scale to Percent signal change
% 2 - Scale to Z scores
% 3 - Normalize spatial maps using the maximum intensity value and multiply timecourses using the maximum intensity value
% 4 - Scale timecourses using the maximum intensity value and spatial maps using the standard deviation of timecourses
scaleType = 1;


%% 'Which ICA Algorithm Do You Want To Use';
% see icatb_icaAlgorithm for details or type icatb_icaAlgorithm at the
% command prompt.
% Note: Use only one subject and one session for Semi-blind ICA. Also specify atmost two reference function names

% 1 means infomax, 2 means fastICA, etc.
algoType = 1;


%% Specify at most two reference function names if you select Semi-blind ICA algorithm.
% Reference function names can be acessed by loading SPM.mat in MATLAB and accessing 
% structure SPM.xX.name.
% refFunNames = {'Sn(1) right*bf(1)', 'Sn(1) left*bf(1)'};


%% Specify spatial reference files for multi-fixed ICA
%refFiles = {which('ref_default_mode.nii'), which('ref_left_visuomotor.nii'), which('ref_right_visuomotor.nii')};

%% ICA Options - Name by value pairs in a cell array. Options will vary depending on the algorithm. See icatb_icaOptions for more details. Some options are shown below.
% Infomax -  {'posact', 'off', 'sphering', 'on', 'bias', 'on', 'extended', 0}
% FastICA - {'approach', 'symm', 'g', 'tanh', 'stabilization', 'on'}

icaOptions = {'posact', 'off', 'sphering', 'on', 'bias', 'on', 'extended', 1};

