#!/bin/bash

: <<COMMENTBLOCK
find and copy dir raw.  Used to traverse directory structure in ARP_Subjects looking for "raw" directories
and then rename those dirs and save them all together.
COMMENTBLOCK

# Exit if number of arguments is too small
if [ $# -lt 1 ]
    then
        echo "Please enter the name of a subdirectory you wish to find in the hierarchy and extract to a specified directory, which will be created if it does not exist."
        echo "e.g., $0 raw dicoms"
        echo "would extract all directories called raw into a directory called dicoms"
        exit 1
fi

indir=$1
outdir=$2

if [ ! -d ${outdir} ]; then
  mkdir ${outdir}
fi

echo "running"
# the find command runs for a while.
# -type d means only look for directories
# -name tells
#dir=`find . -type d -name "raw"`
dir=`find . -type d -name ${indir}`

for mydir in ${dir}; do
# The in and out variables seem necessary to capture a single item at a time
# otherwise cp grabs a whole list and explodes.
in=`echo $mydir`
# First we echo the in file, then sed replaces / with _.  Finally sed replaces the initial ._
out=`echo ${in} | sed 's/\//_/g' | sed 's/\._//g'`
# Just keeping track
echo "in is ${in}; out is ${out}"
# copy the folders to a dicom area, from here we can import anything unique into horos
cp -fr ${in} ./${outdir}/${out}
done
