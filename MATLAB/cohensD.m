function [d] = cohensD(vec1, vec2)
%This function generates cohens d for 2 vectors
% Handles NaNs and vectors of different lengths
m1=nanmean(vec1);
m2=nanmean(vec2);
sd1=nanstd(vec1);
sd2=nanstd(vec2);

d=(m1-m2)/sqrt((sd1*sd1 + sd2*sd2)/2);
end

