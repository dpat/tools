#!/bin/bash

#==============================================================================
: <<COMMENTBLOCK

Function:   InDir
Purpose:    Iteratively run command in the bids_dir for each subject
Input:      List participants, or apply to all participants
            Main directory, command (interactive)
Output:     Command execution

COMMENTBLOCK

InDir ()
{
# Do something in each bids directory for the named subjects
for s in ${subjlist[@]}; do
  echo ${s}
  cd ${s}
  eval ${command}
done
}

#==============================================================================
: <<COMMENTBLOCK

Function:   OutDir
Purpose:    Iteratively run command in the bids_dir for each subject
Input:      List participants, or apply to all participants
            Main directory, command (interactive)
Output:     Command execution

COMMENTBLOCK

OutDir ()
{
# Do something in each bids directory for the named subjects
for s in ${subjlist[@]}; do
  echo ${out_dir}/${s}
  cd out_dir/${s}
  eval ${command}
done
}


#==============================================================================

: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Display help
Calls:      None

COMMENTBLOCK

HelpMessage ()
{
echo "You must always be in your bids directory"
echo "Run for selected subjects (e.g., 327 is sub-327 etc.):"
echo "Example: bids_cmd.sh InDir 327 328"
echo "Run for all available subjects:"
echo "example: bids_cmd.sh InDir"
echo "============================"
echo
exit 1
}
#==============================================================================


bids_dir=${PWD}
out_dir=${PWD}/derivatives
subj=sub-${1}
echo "bidsdir=${bids_dir}/"
echo "outdir=${out_dir}/"

if [ $# -lt 1 ]; then
  HelpMessage
else
  func=$1
  echo ${func}
fi

if [ $# -eq 1 ] && [ ${func} = InDir ]; then
  func=$1
  all_subj=$(ls -d sub-*)
  declare -a subjlist=${all_subj}
elif [ $# -gt 1 ]; then
  func=$1
  shift
  # Here we must specify sub-075 sub-100 etc. I don't like this
  all_subj=$@
  declare -a subjlist=${all_subj}
fi

# echo "all_subj is ${all_subj}"
# echo "subjlist is ${subjlist}"
# len=${#subjlist[@]}
# echo "array length = ${len}"
# # for s in ${subjlist[@]}; do
# #   echo ${s}
# # done
# #create an array to hold the subject list (works perfectly for all subjects)
# #declare -a subjlist=${all_subj}
#
echo "Enter a command for all list members:" # Ask user to type a command
read -e command
#
echo "command is ${command}"
echo "subjlist is ${subjlist}"
echo "function is ${func}"
#
if [ ${func} = InDir ]; then
   InDir
fi
