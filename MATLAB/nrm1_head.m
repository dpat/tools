% This is the script that accompanies the nrm1_head_job.m batch script.
% See detailed description in that file.
% clear any existing variables
clc; clear all
% insure spm is ready to go
spm('defaults', 'FMRI');
% define the data directory path containing your subject folders
dataDir = '/Volumes/Main/Working/Beeson_Data_Review/patients47';
% define a list of subject directories
%subjList={'sub-001', 'sub-003', 'sub-006', 'sub-007', 'sub-008', 'sub-009', 'sub-010', 'sub-011', 'sub-012', 'sub-013', 'sub-014', 'sub-016', 'sub-017', 'sub-018', 'sub-019', 'sub-020', 'sub-021', 'sub-022', 'sub-023', 'sub-024', 'sub-025', 'sub-027', 'sub-028', 'sub-029', 'sub-030', 'sub-031', 'sub-033'}
subjList={'sub-0120', 'sub-0182', 'sub-0187', 'sub-0190', 'sub-0191', 'sub-0194', 'sub-0196', 'sub-0199', 'sub-0301', }
% for each subject in subjList
for subj = subjList
    % display the subject
    disp(strcat('Subject:   ',subj));
    % clear matlabbatch variables
    clear matlabbatch;
    % cd into the subject directory
    cd(char(subj));
    % call the job nrm1_head_job.m
    spm_jobman('run','nrm1_head_job.m')
    % when finished, go back up to the dataDir
    cd(dataDir)
end;
