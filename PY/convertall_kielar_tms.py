import os


def create_key(template, outtype=('nii.gz',), annotation_classes=None):
    if template is None or not template:
        raise ValueError('Template must be a valid format string')
    return template, outtype, annotation_classes


def infotodict(seqinfo):
    # define keys for each sequence
    t1w = create_key('sub-{subject}/{session}/anat/sub-{subject}_T1w')
    tse = create_key('sub-{subject}/{session}/anat/sub-{subject}_acq-tse_T2w')
    dwi = create_key('sub-{subject}/{session}/dwi/sub-{subject}_acq-AP_dwi')
    fmap_rev_phase =  create_key('sub-{subject}/{session}/fmap/sub-{subject}_dir-PA_epi')
    fmap_mag =  create_key('sub-{subject}/{session}/fmap/sub-{subject}_magnitude')
    fmap_phase = create_key('sub-{subject}/{session}/fmap/sub-{subject}_phasediff')
    func_rest = create_key('sub-{subject}/{session}/func/sub-{subject}_{session}_rest_run-01')
    func_rest_post = create_key('sub-{subject}/{session}/func/sub-{subject}_{session}_rest_run-02')
    asl = create_key('sub-{subject}/{session}/func/sub-{subject}_{session}_asl_run-01')
    asl_post = create_key('sub-{subject}/{session}/func/sub-{subject}_{session}_asl_run-02')

    info = {t1w: [], tse: [], dwi: [], fmap_rev_phase: [], fmap_mag: [], fmap_phase: [], func_rest: [], func_rest_post: [], asl: [], asl_post: []}
    last_run = len(seqinfo)

    # define test criteria to check that each sequence is correct"""

    for idx, s in enumerate(seqinfo):
        if (s.dim3 == 176) and ('mprage' in s.protocol_name):
            info[t1w].append(s.series_id)
        if ('TSE' in s.protocol_name):
            info[tse].append(s.series_id)
        if (s.dim3 == 74) and (s.dim4 == 32) and ('DTI' in s.protocol_name):
            info[dwi].append(s.series_id)
        if ('verify_P-A' in s.protocol_name):
            info[fmap_rev_phase] = [s.series_id]
        if (s.dim3 == 64) and ('field_mapping' in s.protocol_name):
            info[fmap_mag] = [s.series_id]
        if (s.dim3 == 32) and ('field_mapping' in s.protocol_name):
            info[fmap_phase] = [s.series_id]
        if ('restingstate' == s.protocol_name):
            info[func_rest].append(s.series_id)
        if ('Post_TMS_restingstate' == s.protocol_name):
            info[func_rest_post].append(s.series_id)
        if ('ASL_3D_tra_iso' == s.protocol_name):
            info[asl].append(s.series_id)
        if ('Post_TMS_ASL_3D_tra_iso' == s.protocol_name):
            info[asl_post].append(s.series_id)
    return info
