#!/usr/bin/env python

"""
From a bash (or zsh) shell, start the csv anaconda environment
source activate csv
This will make pandas, matplotlib and csvkit available
However, the path may not be available until you source ~/.bashrc
Once the path is available:
example:
>join.py slo2_median_split.csv volreg_median_split.csv outme.csv SubjectNum -k2 Run
explanation of this example:
join.py expects 2 files to join (slo2_median_split.csv volreg_median_split.csv),
1-4 columns necessary for the key, 2 in this example (SubjectNum Run)
and an output filename (outme.csv)
"""
import argparse
parser= argparse.ArgumentParser(
    description='inner joins 2 csvs with at least one shared key --and as many as 4-- into a 3rd output csv. Must be run in an environment with pandas and argparse installed, like my csv environment under anaconda.',
    epilog="example with 3 keys (One key column is required, but with flags -k2, -k3, -k4, you can specify up to 3 more): join.py slo2_median_split.csv volreg_median_split.csv out.csv SubjectNum -k2 Run -k3 IC")
parser.add_argument('file1', help='1st csv file')
parser.add_argument('file2', help='2nd csv file')
parser.add_argument('outfile', help='output csv file')
parser.add_argument('colname1', help='1st key column header')
parser.add_argument('-k2','--colname2', help='-k2 flag and 2nd key column header')
# Use the - to designate the arg as optional
## Not yet sure how this interacts with sys.argv...but it
## works when this is not considered optional.
parser.add_argument('-k3','--colname3', help='-k3 flag and 3rd key column header')
parser.add_argument('-k4','--colname4', help='-k4 flag and 4th key column header')

args=parser.parse_args()
# Get pandas library and read in csvs
import pandas as pd
# handle white spaces with skipinitialspace=True
data1 = pd.read_csv(args.file1, skipinitialspace=True)
data2 = pd.read_csv(args.file2, skipinitialspace=True)

# Joins are inner joins by default (intersection of the sets)
# Four keys
if args.colname4 is not None:
    merged = pd.merge(data1, data2, on=[args.colname1, args.colname2, args.colname3, args.colname4], indicator='join')

# Three keys
if args.colname3 is not None:
    merged = pd.merge(data1, data2, on=[args.colname1, args.colname2, args.colname3], indicator='join')

# Two keys
if args.colname2 is not None:
    merged = pd.merge(data1, data2, on=[args.colname1, args.colname2], indicator='join')
else:
# One key
   merged = pd.merge(data1, data2, on=[args.colname1], indicator='join')

# Always do this
merged.to_csv(args.outfile, index=False)
