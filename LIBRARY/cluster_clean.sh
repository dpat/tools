#!/bin/bash

# Author: Dianne Patterson, Ph.D.

if [ $# -lt 2 ]
then
    echo "Usage: $0 <input file> <threshold>"
    echo "Example: $0 combo_mask 1000"
    echo "This cleans a mask to retain only clusters that meet a certain threshold"
    echo "An optional 3rd argument defaults to 26 and represents the number of neighbours"
    echo "to include in clustering.  Other options are 6 or 18 (6 is the harshest)"
    echo "e.g., Example: $0 combo_mask 1000 6"
    echo ""
    exit 1
fi

if [ $# -eq 2 ]
then
  number=26
  else
  number=$3
fi

input=$(remove_ext $1)
thresh=$2

# The threshold is 1 because I use this on binary masks
if [ -e ${input}_cluster_size ]; then
  "imrm ${input}_cluster_size"
fi

#echo "Clusters before thresholding with a connectivity of ${number}"
fsl-cluster -i $input -t 1 --connectivity=${number}  --no_table --osize=${input}_cluster_size
# This step thresholds and names the output=input image
fslmaths ${input}_cluster_size -thr ${thresh} -bin ${input} -odt char
echo
echo "Clusters after thresholding, with a connectivity of ${number}"
fsl-cluster -i $input -t 1 --connectivity=${number}  --oindex=${input}_labels
echo ------------------------------------------------------------------------------
echo
imrm ${input}_cluster_size ${input}_labels.nii.gz
