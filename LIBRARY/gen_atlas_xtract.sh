#!/bin/bash

 #==============================================================================
: <<COMMENTBLOCK

   Function:   HelpMessage
   Purpose:    function_purpose
   Input:      input_files
   Output:     output

COMMENTBLOCK

function HelpMessage ()
{
cat <<- "EOF"
Purpose: 	Create either an FSL atlas XML and directory structure and/or masks for roixtractor and the accompanying region list

Input: 	- Requires FSL MNI-space label file (atlas) in 1mm.nii.gz.
        - Requires label file: a csv file of this form with the name ${atlas}.csv 
          e.g. If the input image is ARTERIAL2_1mm.nii.gz, 
          then the label file should be called: ARTERIAL2.csv
          - The comma is the field separator.
            Columns are: 
              1) Label value The numerical value of the label in the atlas
              2) Short region name (no spaces in names, used to label the roixtractor masks) 
                 The short region label must be unique and have either _R or _L appended 
              3) Long region name with either _R or _L appended (Use underscores instead of spaces)
              4) Hemisphere (L or R)
              5) Lobe (or encompassing region) with a final comma at the end of the line

              Example: Insert a blank line at the beginning and commas at the end of every line.
                 
            
              1,ACA_L,anterior_cerebral_artery_L,L,anterior,
              2,ACA_R,anterior_cerebral_artery_R,R,anterior,
              3,MCA_L,middle_cerebral_artery_L,L,anterior,
              4,MCA_R,middle_cerebral_artery_R,R,anterior,
              5,PCA_L,posterior_cerebral_artery_L,L,posterior,
              6,PCA_R,posterior_cerebral_artery_R,R,posterior,
              7,B_L,basilar_L,L,posterior,
              8,B_R,basilar_R,R,posterior,
              9,LV_L,lateral_ventricle_L,L,ventricle,
              10,LV_R,lateral_ventricle_R,R,ventricle,

=======================
          
Usage fslatlas: 
        The fslatlas function produces the atlas directories and XML files needed for adding 
        1mm MNI label image to fsl as an FSLeyes atlas will generate just the fsl atlas files in an fsl_atlas subdirectory

        $0 fslatlas 1mm_atlas_label_1mm.nii.gz

Output:
        fsl_atlas directory:
        - The XML file for an FSLeyes atlas: ${atlas_name}.xml
        - The 1mm and 2mm versions of the label image in the correct subdirectory. 

        These are suitable for adding to your fsl/data/atlases folder where they will be found by
        fsleyes as an available atlas.

====================== 

Usage xtractor:
        The xtractor function extracts and binarizes all individual rois in a label file for use with roixtractor.
        These will be placed in the xtractor subdirectory with the required region list

        $0 xtractor 1mm_atlas_label_file

Output: 
        xtractor directory:
        - The subdirectory of named 2mm standard space masks for each region in this atlas
        - A *.csv file containing the list of regions and their properties.

=====================
If neither function is specified, both will be run:

$0 1mm_atlas_label_file

===================
EOF
}

#==============================

: <<COMMENTBLOCK

   Function:   fslatlas
   Purpose:    create fsl atlas files
   Input:      1mm FSL-MNI space label image and existing ${atlas_name}.csv
   Output:     XML file and subdirectory for FSL atlas

COMMENTBLOCK

function fslatlas ()
{
# atlas is passed in as argument 2 
atlas_base=$(remove_ext ${atlas})
atlas_name=$(basename -s _1mm.nii.gz ${atlas})

# Check that the expected atlas is present
if [ ! -f ${atlas} ]; then
  echo "${atlas} is not here, cannot proceed without it."
  exit 1
fi

# Make fsl_atlas directories
if [ ! -d fsl_atlas/${atlas_name} ]; then
  echo "making fsl_atlas directory"
  mkdir -p fsl_atlas/${atlas_name}
fi

# Create 2mm version of file if it does not exist
if [ ! -e ${atlas_name}_2mm.nii.gz ]; then
  echo "creating 2mm atlas"
  flirt -in ${atlas} -ref ${atlas} -applyisoxfm 2 -interp nearestneighbour -out ${atlas_name}_2mm.nii.gz
fi

# Copy 1mm and 2mm image to fslatlas subdir: 
cp ${atlas} fsl_atlas/${atlas_name}/
cp ${atlas_name}_2mm.nii.gz fsl_atlas/${atlas_name}/

# Get max label value and clean it up so it is an integer for bash
max=$(fslstats ${atlas} -R | awk '{print $2}' | cut -d'.' -f1)

# Create XML file skeleton
echo "creating fsl_atlas/${atlas_name}.xml"
touch fsl_atlas/${atlas_name}.xml

# Use heredoc to generate skeleton of XML file required for FSL atlases
# The 0 label index is special, leave it as is.
cat << EOF > fsl_atlas/${atlas_name}.xml
<?xml version="1.0" encoding="ISO-8859-1"?>
<atlas version="1.0">
<header>
  <name>${atlas_name}</name>
  <type>Label</type>
  <images>
    <imagefile>/${atlas_name}/${atlas_name}_1mm.nii.gz</imagefile>
    <summaryimagefile>/${atlas_name}/${atlas_name}_1mm</summaryimagefile>
  </images>
    <images>
      <imagefile>/${atlas_name}/${atlas_name}_2mm.nii.gz</imagefile>
      <summaryimagefile>/${atlas_name}/${atlas_name}_1mm</summaryimagefile>
    </images>
  </header>
<data>
  <label index="0" x="0" y="0" z="0">*.*.*.*.*</label>
EOF

# Extract label column corresponding to long region name from text file ${atlas_dir}.csv
roi_label_col=$(cut -d"," -f3 < ${atlas_name}.csv)
# generate mean and center of gravity for each mask in the label file $atlas
fslstats -K ${atlas} ${atlas} -M -C >> temp1
thresh_col=$(cut -d "." -f1 < temp1)
cogx_col=$(cut -d " " -f2 < temp1)
cogy_col=$(cut -d " " -f3 < temp1)
cogz_col=$(cut -d " " -f4 < temp1)
# Build up a file from columns (bash but not sh compatible).  
# Use double quotes else variables become rows instead of columns
# Set the delimiter to a space
paste -d " " <(echo "$thresh_col") <(echo "$cogx_col") <(echo "$cogy_col") <(echo "$cogz_col") <(echo "$roi_label_col") >>temp2
rm temp1

# Iterate over the rows in temp2 creating the XML syntax
thresh=1
while [  ${thresh} -le ${max} ]; do
  # get values from the text file: Use a conditional statement in awk to check that the first field is equivalent to the
  # bash variable thresh.  If it is, print the relevant value from that line
  roi_label=$(cat ${atlas_name}.csv | awk -F',' '{if ($1=="'"$thresh"'") print $2}') 
  # echo "label_num is ${thresh}, roi_label is ${roi_label}"
  # In case values are not consecutive, ensure that roi_label is not an empty string before continuing
  if [[ ! -z $roi_label ]]; then 
    # grep for the first field followed by a space
    cogx=$(grep "^$thresh " temp2 | awk '{print $2}')
    cogy=$(grep "^$thresh " temp2 | awk '{print $3}')
    cogz=$(grep "^$thresh " temp2 | awk '{print $4}')
    # When using the double-quote as the delimiter for awk, the field of interest is the second one (string with spaces)
    roi_name=$(cat ${atlas_name}.csv | awk -F',' '{if ($1=="'"$thresh"'") print $3}')
    echo "  <label index="\"${thresh}\"" x="\"${cogx}\"" y="\"${cogy}\"" z="\"${cogz}\"">${roi_name}</label>"
    echo "  <label index="\"${thresh}\"" x="\"${cogx}\"" y="\"${cogy}\"" z="\"${cogz}\"">${roi_name}</label>" >> fsl_atlas/${atlas_name}.xml
  fi
 ((thresh+=1))
done 

echo "  </data>" >> fsl_atlas/${atlas_name}.xml
echo "</atlas>" >> fsl_atlas/${atlas_name}.xml
echo ""
echo "==========================="
echo "See the directory fsl_anat"
echo "See ${atlas_base}_info.csv"
echo "See also fsl_atlas/${atlas_name}.xml"
echo "==========================="
rm temp2
}

#==============================

: <<COMMENTBLOCK

   Function:   xtractor
   Purpose:    Create region list and 2mm standard space masks for each region: for use with roixtractor
   Input:      1mm FSL-MNI space label image and existing ${atlas_name}.csv
   Output:     xtractor directory containing ${atlas_name}.csv containing volume and center of gravity information
               and a subdirectory of labelled 2mm FSL MNI space masks for each region in the label atlas. 


COMMENTBLOCK

function xtractor ()
{
# atlas is passed in as argument 2 
atlas_base=$(remove_ext ${atlas})
atlas_name=$(basename -s _1mm.nii.gz ${atlas})

# Check that the expected atlas is present
if [ ! -f ${atlas} ]; then
  echo "${atlas} is not here, cannot proceed without it."
  exit 1
fi

# Make xtractor directories
if [ ! -d xtractor${atlas_name} ]; then
  mkdir -p xtractor/${atlas_name}
fi

# Create 2mm version of file if it does not exist
if [ ! -e ${atlas_name}_2mm.nii.gz ]; then
  flirt -in ${atlas} -ref ${atlas} -applyisoxfm 2 -interp nearestneighbour -out ${atlas_name}_2mm.nii.gz
fi

# Create the masks
# Get max label value and clean it up so it is an integer for bash
max=$(fslstats ${atlas} -R | awk '{print $2}' | cut -d'.' -f1)

# Mask values start at 1, The thresh value is the label index value in the atlas
thresh=1
while [  ${thresh} -le ${max} ]; do
  # get values from the text file: Use a conditional statement in awk to check that the first field is equivalent to the
  # bash variable thresh.  If it is, print the relevant value from that line
  roi_label=$(cat ${atlas_name}.csv | awk -F',' '{if ($1=="'"$thresh"'") print $2}') 
  # In case values are not consecutive, ensure that roi_label is not an empty string before continuing
  if [[ ! -z $roi_label ]]; then 
    roi_label_nolat=$(cat ${atlas_name}.csv | awk -F',' '{if ($1=="'"$thresh"'") print $2}' | sed 's/_R//' | sed 's/_L//')
    roi_name=$(cat ${atlas_name}.csv | awk -F',' '{if ($1=="'"$thresh"'") print $3}')
    roi_name_nolat=$(cat ${atlas_name}.csv | awk -F',' '{if ($1=="'"$thresh"'") print $3}' | sed 's/_R//' | sed 's/_L//')
    hemisphere=$(cat ${atlas_name}.csv | awk -F',' '{if ($1=="'"$thresh"'") print $4}')  
    lobe=$(cat ${atlas_name}.csv | awk -F',' '{if ($1=="'"$thresh"'") print $5}') 

    echo "roi index: ${thresh}, roi_label: ${roi_label},roi: ${roi_label_nolat}, roi_name: ${roi_name}, Lobe: ${lobe}, hemisphere: ${hemisphere}"
    # create the mask
    fslmaths ${atlas_name}_2mm -thr ${thresh} -uthr ${thresh} -bin xtractor/${atlas_name}/${roi_label}
    # # extract values from the mask
    cogx=$(fslstats xtractor/${atlas_name}/${roi_label} -C | awk '{print $1}')
    cogy=$(fslstats xtractor/${atlas_name}/${roi_label} -C | awk '{print $2}')
    cogz=$(fslstats xtractor/${atlas_name}/${roi_label} -C | awk '{print $3}') 
    volmm=$(fslstats xtractor/${atlas_name}/${roi_label} -V | awk '{print $2}')
    echo "${roi_label},${hemisphere},${lobe},${cogx},${cogy},${cogz},${roi_name},${volmm}" >> xtractor/temp_List.csv
    echo "${roi_label_nolat},${lobe},${roi_name_nolat}" >> xtractor/temp_Lobes_List.csv
    echo "${roi_label},${roi_name},${thresh}_${hemisphere},${hemisphere},${roi_label_nolat},${lobe},${roi_name_nolat},${volmm}" >> xtractor/temp_UniqueRegionList.csv 
  fi
((thresh+=1))
done

## Sort lists so that join will work correctly (which will now be by roi_label instead of thresh)
## For the Lobes_List it is especially important to select unique rows (or we'll get duplicates for left and right for laterality measures).
sort -u -o xtractor/${atlas_name}_List.csv xtractor/temp_List.csv && rm xtractor/temp_List.csv
sort -u -o xtractor/${atlas_name}_Lobes_List.csv xtractor/temp_Lobes_List.csv && rm xtractor/temp_Lobes_List.csv
sort -u -o xtractor/${atlas_name}_UniqueRegionList.csv xtractor/temp_UniqueRegionList.csv && rm xtractor/temp_UniqueRegionList.csv
}

#==============================================================================
: <<COMMENTBLOCK

Function:  Main
Purpose:   Create fslatlas 2mm image and XML file in correct directory structure under fsl_atlas
           Create masks in 2mm space for roixtractor
Input:     Label altas file in 1mm FSL MNI space
           csv file with colon separators specifying the label index, short name (no spaces), and long name (spaces allowed).  
           Put each in its own row, e.g.,
          1: ACA_L : Left Anterior Cerebral artery territory
          2: ACA_R : Right Anterior cerebral artery territory
           
Output:   Two directories: 
          1) fsl_atlas containing the atlas files to place in fsl/data/atlases
          2) xtractor containing the regional masks in 2mm space and the corresponding csv file for roixtractor to use 

COMMENTBLOCK

function Main ()
{
# Run fslatlas code
fslatlas

# Run xtractor code
xtractor
}

#########################  END FUNCTION DEFINITIONS  ##########################
###############################################################################

while getopts h options 2> /dev/null; do        # Setup -h flag. redirect errors
    case $options in                            # In case someone invokes -h
    h) HelpMessage;;                            # Run the help message function
    \?) echo "only h is a valid flag" 1>&2      # If bad options are passed, print message
    esac
done

shift $(( $OPTIND -1))                          # Index option count, so h isn't treated as arg
optnum=$(( $OPTIND -1))                        	# optnums hold # of options passed in

if [ ${optnum} -lt 1 ] && [ $# -lt 1 ]; then        # If no options or arg
      HelpMessage                               # then display HelpMessage
fi

if [ ${optnum} -lt 1 ] && [ $# -eq 1 ]; then        # If no options or args
        dothis=Main                             # Default to running Main
        atlas=$1
    else
        dothis=$1                               # Run fslatlas or xtractor separately
        atlas=$2
fi

$dothis $atlas
