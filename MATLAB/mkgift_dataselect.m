% A for loop to create the data select statements for a dataset.
% define the root path
root_path ='/Volumes/Main/Users/dpat/Downloads/nifti_subjects_2runs/derivatives/';
% create the subject list
subjList = {'sub-001', 'sub-002', 'sub-003'};
% create the list of file patterns that identify each run (gift calls these sessions)
sessList = {'*run-01*bold.nii.gz', '*run-02*bold.nii.gz'}; 

% get the number of runs (sessions) in the session list
maxSess = numel(sessList);
% get the number of subjects in the subject list
maxsub=numel(subjList);

for sub = 1 :maxsub for sess = 1 :maxSess 
  subnum = sprintf('%s/func', char(subjList(sub)));
  runnum = sprintf('*run-0%d*bold.nii.gz', sess);
  dselect = sprintf('s%d_s%d = {strcat(root_path, ''%s''), ''%s'';}', sub,sess,subnum,runnum);
  disp(dselect)
  end
end