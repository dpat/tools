
ext='.nii';
img=load_nii(['cluster1_all_learn_sent_group_t.nii' ext]);
% clear clust X vals
X=double(img.img); %extract the raw image from the structure, make it a double to be more compatible with other matlab functions
%clear img
%[lin]=find(X(:,:,:,1));  %identify the linear index of any nonzero value in the first of the 4d volumes.
%Xsize=size(X); % Get the matrix size
%[row,col,page]=ind2sub(Xsize,lin);% extract the row, column and page from the linear index
% Rlen=length(row);% Get the matrix size
% vals=zeros(size(X,4),Rlen); % create an empty matrix of the correct size
% for i=1:Rlen  % sadly, use a for loop
%      vals(:,i)=squeeze(X(row(i),col(i),page(i),:)); % get each timeseries, squeeze it and put it in the matrix
%     i=i+1; % increment i
% end; % end the for loop
% outname=[output_mat]
% clust=vals; % set the output arg
% save (outname, 'clust');
%clear 


