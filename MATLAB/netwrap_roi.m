
%% Random Synthetic Networks 2
% For the atlas roi analysis, I want to generate synthetic networks with
% Nodes (All): 137
% edges, S1: 2273
% edges, S2: 1666
% edges, S3: 2308


% Session 1
for count= 1:100
    outmat=sprintf('adjrand_s1_%d', count);
    adjMat=makerandCIJ_und(137,2273);
    eval([sprintf(outmat),'=adjMat;']);
end 

clear count outmat adjMat

% Session 2
for count= 1:100
    outmat=sprintf('adjrand_s2_%d', count);
    adjMat=makerandCIJ_und(137,1666);
    eval([sprintf(outmat),'=adjMat;']);
end 

clear count outmat adjMat

% Session 3
for count= 1:100
    outmat=sprintf('adjrand_s3_%d', count);
    adjMat=makerandCIJ_und(137,2308);
    eval([sprintf(outmat),'=adjMat;']);
end 

clear count outmat adjMat
%% Random Rewired Networks (take 3)

for count= 1:100
    count
    outmat=sprintf('adjrand_s1_%d', count);
    adjMat=randmio_und(s1_adj, 20);
    eval([sprintf(outmat),'=adjMat;']);
end 

for count= 1:100
    count
    outmat=sprintf('adjrand_s2_%d', count);
    adjMat=randmio_und(s2_adj, 20);
    eval([sprintf(outmat),'=adjMat;']);
end 

for count= 1:100
    count
    outmat=sprintf('adjrand_s3_%d', count);
    adjMat=randmio_und(s3_adj, 20);
    eval([sprintf(outmat),'=adjMat;']);
end 
%% Degrees Learnable

% S1
degrees_s1=degrees_und(s1_adj)
[unique_s1,count_s1]=count_unique(degrees_s1)
     
% S2
degrees_s2=degrees_und(s2_adj)
[unique_s2,count_s2]=count_unique(degrees_s2)

% S3
degrees_s3=degrees_und(s3_adj)
[unique_s3,count_s3]=count_unique(degrees_s3)

s1_degree_dist=[count_s1,unique_s1]
s2_degree_dist=[count_s2,unique_s2]
s3_degree_dist=[count_s3,unique_s3]

% Re-imported AllBrainNodes.txt as table this time (which is a dataset).
% Made values nominal: 
Nodes.Nodes=nominal(Nodes.Nodes)

% Apparently table and datasets are not exactly the same,
% and tables are meant to replace datasets. So:

tbl_degrees_s1=array2table(degrees_s1')
tbl_degrees_s2=array2table(degrees_s2')
tbl_degrees_s3=array2table(degrees_s3')
tbl_degrees_s1.Properties.VariableNames{1} = 'degrees_s1';
tbl_degrees_s2.Properties.VariableNames{1} = 'degrees_s2';
tbl_degrees_s3.Properties.VariableNames{1} = 'degrees_s3';
CompareDegrees123=[Nodes,tbl_degrees_s1, tbl_degrees_s2, tbl_degrees_s3]

%% strength learnable

% S1
strengths_s1=strengths_und(s1_adj);
[unique_s1,count_s1]=count_unique(strengths_s1);
     
% S2
strengths_s2=strengths_und(s2_adj);
[unique_s2,count_s2]=count_unique(strengths_s2);

% S3
strengths_s3=strengths_und(s3_adj);
[unique_s3,count_s3]=count_unique(strengths_s3);

s1_strength_dist=[count_s1,unique_s1]
s2_strength_dist=[count_s2,unique_s2]
s3_strength_dist=[count_s3,unique_s3]

% Apparently table and datasets are not exactly the same,
% and tables are meant to replace datasets. So:

tbl_strengths_s1=array2table(strengths_s1');
tbl_strengths_s2=array2table(strengths_s2');
tbl_strengths_s3=array2table(strengths_s3');
tbl_strengths_s1.Properties.VariableNames{1} = 'strengths_s1';
tbl_strengths_s2.Properties.VariableNames{1} = 'strengths_s2';
tbl_strengths_s3.Properties.VariableNames{1} = 'strengths_s3';
CompareStrengths123=[Nodes,tbl_strengths_s1, tbl_strengths_s2, tbl_strengths_s3]

%% degrees rand

randDeg_s1=zeros(100,137);

for count= 1:100
    inmat=sprintf('adjrand_s3_%d', count);
    randDeg_s1(count,:)=degrees_und(inmat);
    %adjMat=makerandCIJ_und(137,2308);
    %eval([sprintf(inmat),'=adjMat;']);   
end 

clear count inmat
%% edge betweenness
len_mat_s1=weight_conversion(s1_adj, 'lengths');
len_mat_s2=weight_conversion(s2_adj, 'lengths');
len_mat_s3=weight_conversion(s3_adj, 'lengths');

edg_btw_s1=edge_betweenness_wei(len_mat_s1);
edg_btw_s2=edge_betweenness_wei(len_mat_s2);
edg_btw_s3=edge_betweenness_wei(len_mat_s3);

% combining edge betweenness tables 
% create table with variable names
s1_btw=array2table(edg_btw_s1, 'VariableNames', NodeRows);
% add row names
s1_btw=[Nodes,s1_btw];
s2_btw=array2table(edg_btw_s2, 'VariableNames', NodeRows);
s2_btw=[Nodes,s2_btw];
s3_btw=array2table(edg_btw_s3, 'VariableNames', NodeRows);
s3_btw=[Nodes,s3_btw];

%% efficiency

>> efficiency_wei(s1_adj) % ans = 0.4197

>> efficiency_wei(s2_adj) % ans = 0.3179

>> efficiency_wei(s3_adj) % ans =0.4587

local_efficiency_s1=efficiency_wei(s1_adj,1);
local_efficiency_s2=efficiency_wei(s2_adj,1);
local_efficiency_s3=efficiency_wei(s3_adj,1);

loc_eff_s1=array2table(local_efficiency_s1);
loc_eff_s2=array2table(local_efficiency_s2);
loc_eff_s3=array2table(local_efficiency_s3);
local_efficiency123=[Nodes,loc_eff_s1, loc_eff_s2, loc_eff_s3];
%% clustering coefficient

% create tables from the get go
clust1=array2table(clustering_coef_wu(s1_adj));
clust2=array2table(clustering_coef_wu(s2_adj));
clust3=array2table(clustering_coef_wu(s3_adj));

% if you create tables as above, each column is named var1
% so then you cannot combine them...you can edit them in 
% the variable editor which runs the following commands:
clust1.Properties.VariableNames{1} = 's1_clust';
clust2.Properties.VariableNames{1} = 's2_clust';
clust3.Properties.VariableNames{1} = 's3_clust';
% Now load up nodes and make a nice table:
clust123=[Nodes, clust1, clust2, clust3]

%% Distance Matrices
dist_mat_s1=distance_wei(len_mat_s1);
dist_mat_s2=distance_wei(len_mat_s2);
dist_mat_s3=distance_wei(len_mat_s3);

%% characteristic path length

[charpath1, ecc1, radius1, diam1]=charpath(dist_mat_s1)
[charpath2, ecc2, radius2, diam2]=charpath(dist_mat_s2)
[charpath3, ecc3, radius3, diam3]=charpath(dist_mat_s3)

[charpath1]=charpath(dist_mat_s1) % charpath1 = 1.2491
[charpath2]=charpath(dist_mat_s2) % charpath2 = 1.3203
[charpath3]=charpath(dist_mat_s3) % charpath3 = 1.2340

% diameter (diam1 diam2 diam3) always seems to come out 0, 
% so I deleted it. Saved the rest as charpath123.mat

%% density (cost) learnable

density_und(s1_adj) % ans = 0.2440
density_und(s2_adj) % ans = 0.1788
density_und(s3_adj) % ans = 0.2477

%% Unlearnable Degrees

nnz(s1_adjU)
nnz(s2_adjU)
nnz(s3_adjU)
degrees_s1U=degrees_und(s1_adjU)
[unique_s1U,count_s1U]=count_unique(degrees_s1U)
degrees_s2U=degrees_und(s2_adjU);
[unique_s2U,count_s2U]=count_unique(degrees_s2U)
degrees_s3U=degrees_und(s3_adjU);
[unique_s3U,count_s3U]=count_unique(degrees_s3U)
s1U_degree_dist=[count_s1U, unique_s1U]
s2U_degree_dist=[count_s2U, unique_s2U]
s3U_degree_dist=[count_s3U, unique_s3U]
tbl_degrees_s1U=array2table(degrees_s1U')
tbl_degrees_s2U=array2table(degrees_s2U')
tbl_degrees_s3U=array2table(degrees_s3U')
tbl_degrees_s1U.Properties.VariableNames{1} = 'degreesU_s1';
tbl_degrees_s2U.Properties.VariableNames{1} = 'degreesU_s2';
tbl_degrees_s3U.Properties.VariableNames{1} = 'degreesU_s3';
CompareDegreesU123=[Nodes, tbl_degrees_s1U, tbl_degrees_s2U, tbl_degrees_s3U]

%% strength unlearnable

% S1
strengths_s1U=strengths_und(s1_adjU);
[unique_s1U,count_s1U]=count_unique(strengths_s1U);
     
% S2
strengths_s2U=strengths_und(s2_adjU);
[unique_s2U,count_s2U]=count_unique(strengths_s2U);

% S3
strengths_s3U=strengths_und(s3_adjU);
[unique_s3U,count_s3U]=count_unique(strengths_s3U);

s1U_strength_dist=[count_s1U,unique_s1U]
s2U_strength_dist=[count_s2U,unique_s2U]
s3U_strength_dist=[count_s3U,unique_s3U]

% Apparently table and datasets are not exactly the same,
% and tables are meant to replace datasets. So:

tbl_strengths_s1U=array2table(strengths_s1U');
tbl_strengths_s2U=array2table(strengths_s2U');
tbl_strengths_s3U=array2table(strengths_s3U');
tbl_strengths_s1U.Properties.VariableNames{1} = 'strengths_s1U';
tbl_strengths_s2U.Properties.VariableNames{1} = 'strengths_s2U';
tbl_strengths_s3U.Properties.VariableNames{1} = 'strengths_s3U';
CompareStrengths123U=[Nodes,tbl_strengths_s1U, tbl_strengths_s2U, tbl_strengths_s3U]

%% Unlearnable Edge Betweenness

lenU_mat_s1=weight_conversion(s1_adjU, 'lengths');
lenU_mat_s2=weight_conversion(s2_adjU, 'lengths');
lenU_mat_s3=weight_conversion(s3_adjU, 'lengths');

edgU_btw_s1=edge_betweenness_wei(lenU_mat_s1);
edgU_btw_s2=edge_betweenness_wei(lenU_mat_s2);
edgU_btw_s3=edge_betweenness_wei(lenU_mat_s3);

% combining tables 
% create table with variable names
s1_btwU=array2table(edgU_btw_s1, 'VariableNames', NodeRows);
% add row names
s1_btwU=[Nodes,s1_btwU];
% create table with variable names
s2_btwU=array2table(edgU_btw_s2, 'VariableNames', NodeRows);
% add row names
s2_btwU=[Nodes,s2_btwU];
% create table with variable names
s3_btwU=array2table(edgU_btw_s3, 'VariableNames', NodeRows);
% add row names
s3_btwU=[Nodes,s3_btwU];

%% Unlearnable efficiency

>> efficiency_wei(s1_adjU) % ans =  0.0668

>> efficiency_wei(s2_adjU) % ans = 0.0402

>> efficiency_wei(s3_adjU) % ans = 0.0513

local_efficiency_s1U=efficiency_wei(s1_adjU,1);
local_efficiency_s2U=efficiency_wei(s2_adjU,1);
local_efficiency_s3U=efficiency_wei(s3_adjU,1);

loc_eff_s1U=array2table(local_efficiency_s1U);
loc_eff_s2U=array2table(local_efficiency_s2U);
loc_eff_s3U=array2table(local_efficiency_s3U);
local_efficiency123U=[Nodes,loc_eff_s1U, loc_eff_s2U, loc_eff_s3U];

%% UNLEARNABLE clustering coefficient

% create tables from the get go
clust1U=array2table(clustering_coef_wu(s1_adjU));
clust2U=array2table(clustering_coef_wu(s2_adjU));
clust3U=array2table(clustering_coef_wu(s3_adjU));

% if you create tables as above, each column is named var1
% so then you cannot combine them...you can edit them in 
% the variable editor which runs the following commands:
clust1U.Properties.VariableNames{1} = 's1_clustU';
clust2U.Properties.VariableNames{1} = 's2_clustU';
clust3U.Properties.VariableNames{1} = 's3_clustU';
% Now load up nodes and make a nice table:
clust123U=[Nodes, clust1U, clust2U, clust3U]

%% Density (Cost) Unlearnable
density_und(s1_adjU) % ans = 0.0492
density_und(s2_adjU) % ans = 0.0298
density_und(s3_adjU) % ans = 0.0350

%% Ice Degrees

nnz(s1_ice_adj)
nnz(s2_ice_adj)
nnz(s3_ice_adj)
nnz(s4_ice_adj)
degrees_s1_ice=degrees_und(s1_ice_adj);
[unique_s1_ice,count_s1_ice]=count_unique(degrees_s1_ice);
degrees_s2_ice=degrees_und(s2_ice_adj);
[unique_s2_ice,count_s2_ice]=count_unique(degrees_s2_ice);
degrees_s3_ice=degrees_und(s3_ice_adj);
[unique_s3_ice,count_s3_ice]=count_unique(degrees_s3_ice);
degrees_s4_ice=degrees_und(s4_ice_adj);
[unique_s4_ice,count_s4_ice]=count_unique(degrees_s4_ice);
s1_ice_degree_dist=[count_s1_ice, unique_s1_ice];
s2_ice_degree_dist=[count_s2_ice, unique_s2_ice];
s3_ice_degree_dist=[count_s3_ice, unique_s3_ice];
s4_ice_degree_dist=[count_s4_ice, unique_s4_ice];
tbl_degrees_s1_ice=array2table(degrees_s1_ice');
tbl_degrees_s2_ice=array2table(degrees_s2_ice');
tbl_degrees_s3_ice=array2table(degrees_s3_ice');
tbl_degrees_s4_ice=array2table(degrees_s4_ice');
tbl_degrees_s1_ice.Properties.VariableNames{1} = 'degrees_ice_s1';
tbl_degrees_s2_ice.Properties.VariableNames{1} = 'degrees_ice_s2';
tbl_degrees_s3_ice.Properties.VariableNames{1} = 'degrees_ice_s3';
tbl_degrees_s4_ice.Properties.VariableNames{1} = 'degrees_ice_s4';
CompareDegrees_ice_1234=[Nodes, tbl_degrees_s1_ice, tbl_degrees_s2_ice, tbl_degrees_s3_ice, tbl_degrees_s4_ice]

%% strength ice

% S1
strengths_s1_ice=strengths_und(s1_ice_adj);
[unique_s1_ice,count_s1_ice]=count_unique(strengths_s1_ice);
     
% S2
strengths_s2_ice=strengths_und(s2_ice_adj);
[unique_s2_ice,count_s2_ice]=count_unique(strengths_s2_ice);

% S3
strengths_s3_ice=strengths_und(s3_ice_adj);
[unique_s3_ice,count_s3_ice]=count_unique(strengths_s3_ice);

s1_ice_strength_dist=[count_s1_ice,unique_s1_ice]
s2_ice_strength_dist=[count_s2_ice,unique_s2_ice]
s3_ice_strength_dist=[count_s3_ice,unique_s3_ice]

% Apparently table and datasets are not exactly the same,
% and tables are meant to replace datasets. So:

tbl_strengths_s1_ice=array2table(strengths_s1_ice');
tbl_strengths_s2_ice=array2table(strengths_s2_ice');
tbl_strengths_s3_ice=array2table(strengths_s3_ice');
tbl_strengths_s1_ice.Properties.VariableNames{1} = 'strengths_s1_ice';
tbl_strengths_s2_ice.Properties.VariableNames{1} = 'strengths_s2_ice';
tbl_strengths_s3_ice.Properties.VariableNames{1} = 'strengths_s3_ice';
CompareStrengths123_ice=[Nodes,tbl_strengths_s1_ice, tbl_strengths_s2_ice, tbl_strengths_s3_ice]

%% Ice Edge Betweenness

len_ice_mat_s1=weight_conversion(s1_ice_adj, 'lengths');
len_ice_mat_s2=weight_conversion(s2_ice_adj, 'lengths');
len_ice_mat_s3=weight_conversion(s3_ice_adj, 'lengths');
len_ice_mat_s4=weight_conversion(s4_ice_adj, 'lengths');

edg_ice_btw_s1=edge_betweenness_wei(len_ice_mat_s1);
edg_ice_btw_s2=edge_betweenness_wei(len_ice_mat_s2);
edg_ice_btw_s3=edge_betweenness_wei(len_ice_mat_s3);
edg_ice_btw_s4=edge_betweenness_wei(len_ice_mat_s4);

% combining tables 
% create table with variable names
s1_btw_ice=array2table(edg_ice_btw_s1, 'VariableNames', NodeRows);
% add row names
s1_btw_ice=[Nodes,s1_btw_ice];
% create table with variable names
s2_btw_ice=array2table(edg_ice_btw_s2, 'VariableNames', NodeRows);
% add row names
s2_btw_ice=[Nodes,s2_btw_ice];
% create table with variable names
s3_btw_ice=array2table(edg_ice_btw_s3, 'VariableNames', NodeRows);
% add row names
s3_btw_ice=[Nodes,s3_btw_ice];
% create table with variable names
s4_btw_ice=array2table(edg_ice_btw_s4, 'VariableNames', NodeRows);
% add row names
s4_btw_ice=[Nodes,s4_btw_ice];

%% Ice efficiency

efficiency_wei(s1_ice_adj) % ans = 0.2170
efficiency_wei(s2_ice_adj) % ans = 0.5444
efficiency_wei(s3_ice_adj) % ans = 0.3281
efficiency_wei(s4_ice_adj) % ans = 0.6277

local_efficiency_s1_ice=efficiency_wei(s1_ice_adj,1);
local_efficiency_s2_ice=efficiency_wei(s2_ice_adj,1);
local_efficiency_s3_ice=efficiency_wei(s3_ice_adj,1);
local_efficiency_s4_ice=efficiency_wei(s4_ice_adj,1);

loc_eff_s1_ice=array2table(local_efficiency_s1_ice);
loc_eff_s2_ice=array2table(local_efficiency_s2_ice);
loc_eff_s3_ice=array2table(local_efficiency_s3_ice);
loc_eff_s4_ice=array2table(local_efficiency_s4_ice);
local_efficiency1234=[Nodes,loc_eff_s1_ice, loc_eff_s2_ice, loc_eff_s3_ice, loc_eff_s4_ice];

%% Ice clustering coefficient

% create tables from the get go
clust1_ice=array2table(clustering_coef_wu(s1_ice_adj));
clust2_ice=array2table(clustering_coef_wu(s2_ice_adj));
clust3_ice=array2table(clustering_coef_wu(s3_ice_adj));
clust4_ice=array2table(clustering_coef_wu(s4_ice_adj));

% if you create tables as above, each column is named var1
% so then you cannot combine them...you can edit them in 
% the variable editor which runs the following commands:
clust1_ice.Properties.VariableNames{1} = 's1_clust_ice';
clust2_ice.Properties.VariableNames{1} = 's2_clust_ice';
clust3_ice.Properties.VariableNames{1} = 's3_clust_ice';
clust4_ice.Properties.VariableNames{1} = 's4_clust_ice';
% Now load up nodes and make a nice table:
clust1234_ice=[Nodes, clust1_ice, clust2_ice, clust3_ice, clust4_ice]

%% Density (Cost) Ice
density_und(s1_ice_adj) % ans =  0.1263
density_und(s2_ice_adj) % ans =  0.3345
density_und(s3_ice_adj) % ans =  0.2071
density_und(s3_ice_adj) % ans =  0.3865
