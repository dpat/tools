#!/bin/bash

: <<COMMENTBLOCK
This code calls the docker bidsonym tool to deface structural data in the BIDS data structure. 
This is set up to use pydeface.
This script requires that you are in the directory where your subject subdirectories are stored in the BIDS data structure.
COMMENTBLOCK


# Exit and spit out help message if number of arguments is too small
if [ $# -lt 1 ]
  then
    echo "======================================================"
    echo "This assumes you are running docker"
    echo "and have downloaded bidsonym: docker pull peerherholz/bidsonym"
    echo "e.g. Run subject sub-304: $0 304"
    echo "e.g. Run multiple subjects: $0 304 305 306"
    echo "run this from the BIDS directory"
    echo "======================================================"
    exit 1
fi

docker run -i --rm -v ${PWD}:/data peerherholz/bidsonym /data participant --deid pydeface --participant_label ${*} --brainextraction bet --bet_frac 0.5 --skip_bids_validation
