#!/bin/bash

: <<COMMENTBLOCK
sqdiff_wrap.sh >> sqdiff.txt will dump results into sqdiff.txt
COMMENTBLOCK

if [ ! -e sqdiff.txt ]; then
  touch sqdiff.txt
fi

for subj in sub*; do
  cd ${subj}
  for img in *.nii *.nii.gz; do
    if [ -e  ${img} ]; then
    sqdiff.sh ${img}
    fi
  done
  cd ..
  echo "++++++++++++++++++++++++++++++++++++++++++++++++"
done
