#!/bin/bash

# Author: Dianne Patterson, Ph.D.

if [ $# -lt 4 ]
then
    echo "Usage: $0 <input file> <stats_threshold_pos> <stats_thresh_neg> <cluster_size>"
    echo "Example: $0 stats1 2.1 -3 20"
    echo "This cleans a stats image to retain only clusters that meet three criteria:"
    echo "The minimum positive statistical value, e.g., 2.1 in the example (retain values like 2.5, 3 etc) "
    echo "The maximum negative statistical value, e.g., -3 in the example (retain all values like -3.5, -4 etc)"
    echo "the minimum cluster size, e.g., 20 in the example"
    echo ""
    echo ""
    exit 1
fi

input=$(remove_ext $1)
stats_thresh_pos=$2
stats_thresh_neg=$3
cluster_size=$4

connectivity=6

# test this
stats_thresh_neg_abs=$(echo ${stats_thresh_neg} | tr -d -)
echo "absolute negative threshold is ${stats_thresh_neg_abs}"

echo "========================="

# invert input for negative threshold calculations
fslmaths ${input} -mul -1 temp_neg

echo "create positive threshold mask"
fslmaths ${input} -thr ${stats_thresh_pos} -bin temp_pos${stats_thresh_pos}_mask
echo "create negative threshold mask"
fslmaths temp_neg -thr ${stats_thresh_neg_abs} -bin temp_neg${stats_thresh_neg}_mask

echo "Clusters before thresholding positive"
# This step thresholds the pos statistical image and produces an osize image which contains the cluster size
# in each voxel instead of the original voxel value 
# Connectivity of 6 means faces must be touching...so it should clean up voxels that appear to be alone in a particular slice
cluster -i temp_pos${stats_thresh_pos}_mask -t 1  --connectivity=${connectivity} --osize=temp_pos_cluster_size
# This step thresholds the neg statistical image and produces an osize image which contains the cluster size
# in each voxel instead of the original voxel value.  Clustering looks for values above a threshold,
# so I choose an arbitrarily low value here, -1000
echo ""
echo "========================================================="
echo "" 
echo "Clusters before thresholding negative"
fsl-cluster -i temp_neg${stats_thresh_neg}_mask -t 1 --connectivity=${connectivity} --osize=temp_neg_cluster_size

# Combine positive and negative masks into combo mask
fslmaths temp_pos_cluster_size -add temp_neg_cluster_size temp_cluster_size

# This step thresholds the combo mask image by the given cluster size
fslmaths temp_cluster_size -thr ${cluster_size} -bin temp_mask 

# This step multiplies the original image by the thresholded combo input mask image we just created
# Only clusters that meet threshold are retained in the image
fslmaths ${input} -mul temp_mask ${input}_statgt${stats_thresh_pos}_statlt${stats_thresh_neg}_clustsize${cluster_size}


# Remove intermediate images
imrm temp*

echo ==============================================================================
echo ""
echo "Try the following command to view the results:"
echo ------------------------------------------------------------------------------
echo "fsleyes $input ${input}_statgt${stats_thresh_pos}_statlt${stats_thresh_neg}_clustsize${cluster_size} --cmap red-yellow --negativeCmap blue-lightblue --useNegativeCmap"
echo ------------------------------------------------------------------------------
echo ""