function dfnc_states(filename, numwin)
% example: dfnc_states('LearnSlo2ERBM_dfnc_post_process.mat', 9)
% state data in contained in the DFNC subdir in *_dfnc_post_process.mat
load(filename);
% permute the dimensions into a more approachable format.
% this creates a column of timepoints for each subject and each of the
% sessions.
states=permute(clusterInfo.states,[3,1,2]);
numsess=size(states, 3);
numsub=size(states,2);
winvals=[1:numwin];

for sess=1:numsess
    for sub=1:numsub
        vec=(states(:,sub,sess));
        nstate=nnz(diff(vec)); % number of state transitions
        state_cat=categorical(vec, winvals); % count of each state
        cnt=countcats(state_cat)';
        cnt1=table(sub, sess, nstate, cnt);
        if ~exist ('t', 'var')
                    t=cnt1;
                % if t exists, then put the new data in another table and
                % concatenate it onto t
                elseif exist ('t', 'var')
                    cnt2=table(sub, sess, nstate, cnt);
                    t=[t;cnt2];
         end   
    end  
end    
% write the concatenated table for subjects and sessions.
writetable(t,'state_stats.csv');
