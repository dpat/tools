#!/bin/bash

#!/bin/bash

if [ $# -lt 1 ]  #  $# is the number of arguments
then
    echo "Usage: $0 <NIfTI-1 image>"
    echo "Example: $0 lesion.nii.gz"
    echo "Example: $0 *"
    echo =======================================
    echo "This provides descriptive statistics for each argument:"
    echo "Stats include voxel and datatype info from fslhd,"
    echo "also voxel count, mean, and sd for non-zero voxels."
    echo "It also provides the robust range of values."
    echo ""
    exit 1
fi

for img in $@; do
  if [[ $img =~ .*nii.* ]]; then
    echo "image is $img"
    ${FSLDIR}/bin/fslhd $img | cat -v | egrep 'data_type|dim[1-4]'
    vol=$(fslstats $img -V | awk '{print $1}')
    mean=$(fslstats $img -M)
    sd=$(fslstats $img -S)
    robrng=$(fslstats $img -r)
    echo "vox_cnt         $vol"
    echo "mean            $mean"
    echo "sd              $sd"
    echo "robust_rng      $robrng"
    echo =============================================
  fi
done