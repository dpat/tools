for idataset = (datasetArray);
    idataset=char(idataset); % converts cells to strings, so sprintf can use them
    for fieldValue=(fieldValueArray);
        fieldValue=char(fieldValue);
        field=char(field);
       sprintf('%s(ismember(%s.%s, (fieldValue)),:);', idataset, idataset, field);
       newdataset=char(strcat((idataset), '_', (fieldValue)));
       eval(ans);
      assignin('base',(newdataset), ans);
  end
end    

clear ans idataset fieldValue field newdataset ans
% Requires cell arrays like these:
% datasetArray= {'dprime_tracts','dprime_endpoints'};
% fieldValueArray = { 'r','l' }
% field={'R_L'};
