#!/bin/bash

: <<COMMENTBLOCK
Reslice an image from SPM12 space to FSL space.
Many thanks to Mark Jenkinson in the FSL group.
COMMENTBLOCK

# Exit if number of arguments is too small
if [ $# -lt 1 ]
    then
        echo "======================================================"
        echo "One arguments is required."
        echo "argument 1: name of standard space image to reslice"
        echo "This can be an anatomical or mask image"
        echo "The image must be in SPM12 1mm space:"
        echo "181x217x181"
        echo "The output will be an image the slightly different"
        echo "FSL 1mm space: 182x218x182"
        echo "e.g., spm2fsl.sh myimage"
        echo "Output will be myimage_fsl.nii.gz"
        echo "Useful for tractotron, for example."
        echo "======================================================"
        exit 1
fi

# Get the input image to be resliced
input=`remove_ext ${1}`
# -noresampleblur ensures the mask does not get bigger.
flirt -in ${input} -ref /usr/local/fsl/data/standard/MNI152_T1_1mm -applyxfm -usesqform -noresampblur -interp nearestneighbour -out ${input}_fsl
