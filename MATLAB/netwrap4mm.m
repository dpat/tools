% life after netwrap

s1_2x7=adj_s1_IC2 + adj_s1_IC7;
% Find count of nonzero elements
a=nnz(adj_s1_IC2) 
b=nnz(adj_s1_IC7)
a+b
c=nnz(s1_2x7)

% Get the difference in count of nonzero elements between 2 matrices
diff=(a+b)-c
% There are ~14 million cells in each adjacency matrix.  Matlab handles
% this gracefully and quickly.
% Add adjacency matrices
adj_s1_all=adj_s1_IC2 + adj_s1_IC25 + adj_s1_IC31 + adj_s1_IC39 + adj_s1_IC7;
adj_s2_all=adj_s2_IC2 + adj_s2_IC25 + adj_s2_IC31 + adj_s2_IC39 + adj_s2_IC7;
adj_s3_all=adj_s3_IC2 + adj_s3_IC25 + adj_s3_IC31 + adj_s3_IC39 + adj_s3_IC7;
adj123=adj_s1_all + adj_s1_all + adj_s1_all;
% Make a picture of the adjacency matrix using color to represent value
imagesc(adj123)
% Find the linear index of coordinate values=37 in row 1 of coords (this
% corresponds to x)
find(coords(1,:)==37)

% Find the linear index of the following x,y,z values: 37,24,14 
find(coords(1,:)==37 & coords(2,:)==24 & coords(3,:)==14)
% Find all nonzero elements in the vector. If there are 120
 find(vec_s1_IC2)
% As in this example, then the adjacency matrix will contain 
% 120 connections for each connected voxel.
 find(degrees_und(adj_s1_IC2)==120)
% Find the max in each column
 max(adj_s1_all)
% Find the max in the whole matrix 
 max(max(adj_s1_all))
% Find the linear indices of all values=3 in the matrix 
% These liear indices are crazy big and not useful to humans
 find(adj_s1_all(:,:)==3);
% Get the row and column indices of all cells containing 3 
 [i,j]=ind2sub(size(adj_s1_all),find(adj_s1_all==3))
 
 %==================================================
 %% DEGREES
 % calculate degrees and put the resulting vector in a variable named
 % s1_degrees
 s1_degrees=degrees_und(adj_s1_all);
 % group the values in the adjacency matrix and tell me what each unique
 % value is. The following results in 0,1,2,3 indicating that at most we 
 % have overlaps of 3 components in session 1. 
 unique(adj_s1_all);
 % what are the numbers of degrees we have?
 unique(s1_degrees);
 % unique s1_degrees=[0,120,260,370,419,532,539,643,764,913,934,1029,1144,1252,1275,1399,1611]
 % 17 bins
 % A fully connected graph of one component will have (n*(n-1)) values in the adjacency matrix.
 % Where n=the number of nonzero elements in the vector.
 % Number of nonzero elements in each vector
 s1_IC2:  121
 s1_IC25: 261
 s1_IC31: 914
 s1_IC39: 420
 s1_IC7:  533
 
 % unique s2_degrees=[0,93,422,453,457,511,545,661,747,829]
 % 10 bins
 % Number of nonzero elements in each vector
 s2_IC2:  454
 s2_IC25: 423
 s2_IC31: 662
 s2_IC39: 458
 s2_IC7:  94
 
 % unique s3_degrees=[0,371,472,581,742,790,835,938,1085,1110,1155,1191,1203,1320,1449,1471,1532,1748,1794,1860]
 % 20 bins
 % Number of nonzero elements in each vector
 s3_IC2: 473
 s3_IC25:372 
 s3_IC31:743
 s3_IC39: 582
 s3_IC7: 791
 
 % Downloaded function from matlab site to count the number in each unique
 % group. These values are saved in degrees123.mat
 [uniques, counts]=count_unique(s1_degrees)
 uniques_s1_degrees=[uniques, counts];
 [uniques, counts]=count_unique(s2_degrees)
 uniques_s2_degrees=[uniques, counts];
 [uniques, counts]=count_unique(s3_degrees)
 uniques_s3_degrees=[uniques, counts];
 
 %% DEGREE DISTRIBUTION
 % roughly looks like degree distribution drops slightly from s1 to s2,
 % then rises in s3
 hist(s1_degrees)
 figure
 hist(s2_degrees)
 figure
 hist(s3_degrees)
 %==============
 %% Strengths (weighted degrees)
 s1_strengths=strengths_und(adj_s1_all);
 s2_strengths=strengths_und(adj_s2_all);
 s3_strengths=strengths_und(adj_s3_all);
 
 [uniques, counts]=count_unique(s1_strengths);
 uniques_s1_strengths=[uniques, counts];
 
 [uniques, counts]=count_unique(s2_strengths);
 uniques_s2_strengths=[uniques, counts];
 
 [uniques, counts]=count_unique(s3_strengths);
 uniques_s3_strengths=[uniques, counts];
 %=================
 % Angular Gyrus S1
 big_s1s=find(s1_strengths==1705);
 1772	1775	1783	1945	1946	1952	1953	1954	1955	2110

>> coords(:,big_s1s)

ans =

    33    36    33    35    36    34    35    36    37    36
    16    16    17    16    16    17    17    17    17    18
    21    21    21    22    22    22    22    22    22    23
 
 -------------------------
% S2: Middle Temporal gyrus left    
s2_all
>> big_s2s=find(s2_strengths==1536)

big_s2s =

   783   784

>> coords(:,big_s2s)

ans =

    35    36
    25    25
    16    16   
-------------------------
% S3: Middle Temporal gyrus left
>> big_s3s=find(s3_strengths==2113)

big_s3s =

   558   755

>> coords(:,big_s3s)

ans =

    37    37
    21    20
    15    16   
=========================
%% DENSITY
% N=nodes, K=edges
[density,N,K]=density_und(adj_s1_all)

density = 0.1137
N =  3470
K =  684409

[density,N,K]=density_und(adj_s2_all)

density = 0.0855
N =   3470
K =   514891

[density,N,K]=density_und(adj_s3_all)

density = 0.1525
N =    3470
K =   917849

%% Betweenness Centrality (wrong)

betweennness_wei(adj_s1_all)
betweennness_wei(adj_s2_all)
betweennness_wei(adj_s3_all)

[uniques, counts]=count_unique(s1_betweenness);
uniques_s1_betweenness=[uniques, counts];

[uniques, counts]=count_unique(s2_betweenness);
uniques_s2_betweenness=[uniques, counts];

[uniques, counts]=count_unique(s3_betweenness);
uniques_s3_betweenness=[uniques, counts];

% Retrieve coordinates of values over 30,000
coords(:,find(s2_betweenness>30000))

(except this may all be wrong, because I needed to convert to a length matrix first)

%% Betweenness take 2

% moved betweenness123.mat to betweenness123_wrong.mat (just in case I need
% it)

len_s1_all=weight_conversion(adj_s1_all, 'lengths');
unique(len_s1_all)

ans =

         0
    0.3333
    0.5000
    1.0000

s1_betweeness=betweenness_wei(len_s1_all);

len_s2_all=weight_conversion(adj_s2_all, 'lengths');
s2_betweeness=betweenness_wei(len_s2_all);

len_s3_all=weight_conversion(adj_s3_all, 'lengths');
s3_betweeness=betweenness_wei(len_s3_all);

[uniques, counts]=count_unique(s1_betweenness);
uniques_s1_betweenness=[uniques, counts];

[uniques, counts]=count_unique(s2_betweenness);
uniques_s2_betweenness=[uniques, counts];

[uniques, counts]=count_unique(s3_betweenness);
uniques_s3_betweenness=[uniques, counts];

% Retrieve coordinates of values over 30,000
coords(:,find(s2_betweenness>30000))

%% Random Networks

vec_s1_all=vec_s1_IC2 + vec_s1_IC25 + vec_s1_IC31 + vec_s1_IC39 + vec_s1_IC39
% count of nodes
nnz(vec_s1_all)
ans = 1614

% count of edges
nnz(adj_s1_all)
ans =1368818

vec_s2_all=vec_s2_IC2 + vec_s2_IC25 + vec_s2_IC31 + vec_s2_IC39 + vec_s2_IC39
% count of nodes
nnz(vec_s2_all)
ans =  1812

% count of edges
nnz(adj_s2_all)
ans = 1029782

vec_s3_all=vec_s3_IC2 + vec_s3_IC25 + vec_s3_IC31 + vec_s3_IC39 + vec_s3_IC39

% count of nodes
nnz(vec_s3_all)
ans=1973

% count of edges
nnz(adj_s3_all)
ans = 1835698



