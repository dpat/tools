#!/bin/bash

#============================================================================
: <<COMMENTBLOCK

Function: MkROIS
Purpose: 	Create a set of appropriately named binary ROIs under the subdir ROIs
Input: 		Assumes a subdir ROISxId containing roi indexed binary masks for each region
Output:   Binary ROIs with label names under ROIS

COMMENTBLOCK

MkROIS ()
{

if [ ! -d ROIS ]; then
  mkdir ROIS
 else
  echo "directory ROIS already exists, exiting"
  exit 1
fi

r_indx_l=1
for name in `echo ${area_list_L}`; do
   echo "The name is ${name} and r_indx_l is ${r_indx_l}"
   imcp ROISxID/roi_${r_indx_l} ROIS/${name}
   let "r_indx_l=r_indx_l+1"
done

r_indx_r=201
for name in `echo ${area_list_R}`; do
 	echo "The name is ${name} and r_indx_r is ${r_indx_r}"
     imcp ROISxID/roi_${r_indx_r} ROIS/${name}
  let "r_indx_r=r_indx_r+1"
done

}
#============================================================================
: <<COMMENTBLOCK

Function: MkROISxID
Purpose: 	Create a set of rois labeled by their index
Input: 		Requires HCP-MMP_1mm.nii.gz locally, which it checks for
Output: 	Creates ROISxID dir and populates it with indexed ROIs.  Also creates
          HCP-MMP1_info.csv containing volume and center of gravity information
          for each roi
COMMENTBLOCK

MkROISxID ()
{

atlas=HCP-MMP_1mm.nii.gz

# Check that the expected atlas is present
if [ ! -f HCP-MMP_1mm.nii.gz ]; then
  "HCP-MMP_1mm.nii.gz is not here, cannot proceed without it."
  exit 1
fi

if [ ! -d ROISxID ]; then
  mkdir ROISxID
 else
  echo "directory ROISxID already exists, exiting"
  exit 1
fi

# Get max and clean it up so it is an integer for bash
max=`fslstats ${atlas} -R | awk '{printf $2 "\n"}'`
# Identify the extension .*
ext=`echo ${max} | cut -d'.' -f2`
# Chop the extension off the end of the number, to make sure it is an integer
max=`basename -s .${ext} ${max}`
echo "Max value in atlas is ${max}"
echo "index, x-cog, y-cog, z-cog, volmm" >> HCP-MMP1_info.csv
# Mask values start at 1: The thresh value is the label index value in the atlas
thresh=1
# Iterate through atlas indices creating individual named volumes
# The atlas I'm using goes from 1-180 on the left and 201-280 on the right.
# So, I do some work to make sure I check that there really is a volume for
# a given index under max, then I only generate cogs and entries in csvs
# if the volume is good.
while [  ${thresh} -le ${max} ]; do
 fslmaths ${atlas} -thr ${thresh} -uthr ${thresh} -bin ROISxID/roi_${thresh}
 volmm=`fslstats ROISxID/roi_${thresh} -V | awk '{print $2}'`
 # Check that the roi volume is at least 1mm cubed
 good=`echo "${volmm}  >= 1" | bc`
 # If the roi has some volume, then proceed
 if [[ ${good}  -eq 1  ]]; then
   cogx=`fslstats ROISxID/roi_${thresh} -C | awk '{print $1}'`
   cogy=`fslstats ROISxID/roi_${thresh} -C | awk '{print $2}'`
   cogz=`fslstats ROISxID/roi_${thresh} -C | awk '{print $3}'`
   echo "${thresh}, ${cogx}, ${cogy}, ${cogz}, ${volmm}"
   echo "${thresh}, ${cogx}, ${cogy}, ${cogz}, ${volmm}" >> HCP-MMP1_info.csv
 fi
 let thresh+=1
done

}

#============================================================================
: <<COMMENTBLOCK

Function: MkXMLlabels
Purpose: 	Create a text file containing the list of values to put into an XML file
Input: 		Assumes a subdir ROISxId containing roi indexed binary masks for each region
Output:   Creates HCP-MMP1_labels.txt

COMMENTBLOCK

MkXMLlabels ()
{

r_indx_l=1
for name in `echo ${area_list_L}`; do
   echo "The name is ${name} and r_indx_l is ${r_indx_l}"
   cogx=`fslstats ROISxID/roi_${r_indx_l} -C | awk '{print $1}'`
   cogy=`fslstats ROISxID/roi_${r_indx_l} -C | awk '{print $2}'`
   cogz=`fslstats ROISxID/roi_${r_indx_l} -C | awk '{print $3}'`
   echo "<label index=\"${r_indx_l}\" x=\"${cogx}\" y=\"${cogy}\" z=\"${cogz}\">${name}</label>" >> HCP-MMP1_labels.csv
   let "r_indx_l=r_indx_l+1"
done

r_indx_r=201
for name in `echo ${area_list_R}`; do
 	echo "The name is ${name} and r_indx_r is ${r_indx_r}"
  cogx=`fslstats ROISxID/roi_${r_indx_r} -C | awk '{print $1}'`
  cogy=`fslstats ROISxID/roi_${r_indx_r} -C | awk '{print $2}'`
  cogz=`fslstats ROISxID/roi_${r_indx_r} -C | awk '{print $3}'`
  echo "<label index=\"${r_indx_r}\" x=\"${cogx}\" y=\"${cogy}\" z=\"${cogz}\">${name}</label>" >> HCP-MMP1_labels.csv
  let "r_indx_r=r_indx_r+1"
done

}



#============================================================================
: <<COMMENTBLOCK
Three functions to help create and name rois from the HCPMMP1 atlas.
MkROISxID makes binary images of each roi with the index number of the label
MkXMLlabels creates the list of values for the XML file required by FST_L
MkROIS creates the labeled binary rois (not indexed). These are used by roixtractor
COMMENTBLOCK

area_list_L="V1_L MST_L V6_L V2_L V3_L V4_L V8_L 4_L 3b_L FEF_L PEF_L 55b_L V3A_L RSC_L POS2_L V7_L IPS1_L FFC_L V3B_L LO1_L LO2_L PIT_L MT_L A1_L PSL_L SFL_L PCV_L STV_L 7Pm_L 7m_L POS1_L 23d_L v23ab_L d23ab_L 31pv_L 5m_L 5mv_L 23c_L 5L_L 24dd_L 24dv_L 7AL_L SCEF_L 6ma_L 7Am_L 7Pl_L 7PC_L LIPv_L VIP_L MIP_L 1_L 2_L 3a_L 6d_L 6mp_L 6v_L p24pr_L 33pr_L a24pr_L p32pr_L a24_L d32_L 8BM_L p32_L 10r_L 47m_L 8Av_L 8Ad_L 9m_L 8BL_L 9p_L 10d_L 8C_L 44_L 45_L 47l_L a47r_L 6r_L IFJa_L IFJp_L IFSp_L IFSa_L p9-46v_L 46_L a9-46v_L 9-46d_L 9a_L 10v_L a10p_L 10pp_L 11l_L 13l_L OFC_L 47s_L LIPd_L 6a_L i6-8_L s6-8_L 43_L OP4_L OP1_L OP2-3_L 52_L RI_L PFcm_L PoI2_L TA2_L FOP4_L MI_L Pir_L AVI_L AAIC_L FOP1_L FOP3_L FOP2_L PFt_L AIP_L EC_L PreS_L H_L ProS_L PeEc_L STGa_L PBelt_L A5_L PHA1_L PHA3_L STSda_L STSdp_L STSvp_L TGd_L TE1a_L TE1p_L TE2a_L TF_L TE2p_L PHT_L PH_L TPOJ1_L TPOJ2_L TPOJ3_L DVT_L PGp_L IP2_L IP1_L IP0_L PFop_L PF_L PFm_L PGi_L PGs_L V6A_L VMV1_L VMV3_L PHA2_L V4t_L FST_L V3CD_L LO3_L VMV2_L 31pd_L 31a_L VVC_L 25_L s32_L pOFC_L PoI1_L Ig_L FOP5_L p10p_L p47r_L TGv_L MBelt_L LBelt_L A4_L STSva_L TE1m_L PI_L a32pr_L p24_L "

area_list_R="V1_R MST_R V6_R V2_R V3_R V4_R V8_R 4_R 3b_R FEF_R PEF_R 55b_R V3A_R RSC_R POS2_R V7_R IPS1_R FFC_R V3B_R LO1_R LO2_R PIT_R MT_R A1_R PSL_R SFL_R PCV_R STV_R 7Pm_R 7m_R POS1_R 23d_R v23ab_R d23ab_R 31pv_R 5m_R 5mv_R 23c_R 5L_R 24dd_R 24dv_R 7AL_R SCEF_R 6ma_R 7Am_R 7Pl_R 7PC_R LIPv_R VIP_R MIP_R 1_R 2_R 3a_R 6d_R 6mp_R 6v_R p24pr_R 33pr_R a24pr_R p32pr_R a24_R d32_R 8BM_R p32_R 10r_R 47m_R 8Av_R 8Ad_R 9m_R 8BL_R 9p_R 10d_R 8C_R 44_R 45_R 47l_R a47r_R 6r_R IFJa_R IFJp_R IFSp_R IFSa_R p9-46v_R 46_R a9-46v_R 9-46d_R 9a_R 10v_R a10p_R 10pp_R 11l_R 13l_R OFC_R 47s_R LIPd_R 6a_R i6-8_R s6-8_R 43_R OP4_R OP1_R OP2-3_R 52_R RI_R PFcm_R PoI2_R TA2_R FOP4_R MI_R Pir_R AVI_R AAIC_R FOP1_R FOP3_R FOP2_R PFt_R AIP_R EC_R PreS_R H_R ProS_R PeEc_R STGa_R PBelt_R A5_R PHA1_R PHA3_R STSda_R STSdp_R STSvp_R TGd_R TE1a_R TE1p_R TE2a_R TF_R TE2p_R PHT_R PH_R TPOJ1_R TPOJ2_R TPOJ3_R DVT_R PGp_R IP2_R IP1_R IP0_R PFop_R PF_R PFm_R PGi_R PGs_R V6A_R VMV1_R VMV3_R PHA2_R V4t_R FST_R V3CD_R LO3_R VMV2_R 31pd_R 31a_R VVC_R 25_R s32_R pOFC_R PoI1_R Ig_R FOP5_R p10p_R p47r_R TGv_R MBelt_R LBelt_R A4_R STSva_R TE1m_R PI_R a32pr_R p24_R"

MkROISxID
MkXMLlabels
MkROIS
