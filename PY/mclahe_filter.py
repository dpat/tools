#!/usr/bin/env python3
import os
import sys
import numpy as np
import nibabel as nib
import mclahe as mc


def filename_core (apath):
    """ Return the core filename string without the path prefix or extension. """
    if (apath is None):                     # sanity check
        return ''
 
    if in_nifti.endswith('.gz'):
        return os.path.basename(os.path.splitext(os.path.splitext(apath)[0])[0])
    else:
        return os.path.basename(os.path.splitext(apath)[0])    


def clahe_filter(in_nifti):
    """
    Apply the mclahe filter to a nifti image and save the result to disk.
    
    Parameters:
    in_nifti (str): The path to the input nifti image file.
    """
    # load the image with nibael
    img_nifti = nib.load(in_nifti)
    # Get the affine matrix from the nifti file
    affine = img_nifti.affine
    # make the image into a numpy array
    img = img_nifti.get_fdata()
    # apply the mclahe filter
    img_mclahe = mc.mclahe(img[:, :, :])
    # Use nibabel to recombine the image with its affine matrix
    imgN = nib.Nifti1Image(img_mclahe, affine)
    # Create output filename by concatenating pieces
    output_filename = f'{filename_core(in_nifti)}_mclahe.nii.gz'
    # Export nifti image to disk
    nib.save(imgN, output_filename)


if __name__ == '__main__':
    if len(sys.argv[1:]) < 1:
        print(f'Usage: {sys.argv[0]} nifti_image')
        sys.exit(1)
    in_nifti = sys.argv[1]
    clahe_filter(in_nifti)