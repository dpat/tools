#!/bin/bash

: <<COMMENTBLOCK
This script is intended to get the first roi for a particular bip result, truncate it so it only includes grey matter, 
and put it in standard space in a directory derivatives/roi_analysis.  
The goal is to compare the initial roi to the final roi to see how much tissue was 
removed from the parcelltion.  Run this from the main bip directory.

Created: 2020_07_14
Author: Dianne Patterson, Ph.D.
COMMENTBLOCK

# Set up some variable names
subj=$1
tract=$2
roi_num=$3
tract_dir=derivatives/bip/${subj}/dwi/${tract}_bip
roi=${tract_dir}/roi_${roi_num}.nii.gz
gm_mask=derivatives/bip/${subj}/subjectrois/${subj}_B0_gmseg_bin.nii.gz
warp=derivatives/fsl_dwi_proc/${subj}/reg/${subj}_b0_mni_warp.nii.gz
out=derivatives/roi_analysis/${tract}/roi_${roi_num}

# Provide help if the user has not provided the correct number of arguments
if [ $# -lt 3 ]; then
  echo "Run this script from the main BIDs directory."
  echo "It assumes that you have run bip and generated iterative parcellations for the tract in question." 
  echo "This script needs 3 arguments:"
  echo "1) a subject number, e.g, sub-100"
  echo "2) a tract, e.g. arc_l"
  echo "3) the roi number you want to work on, e.g., 1 or 2" 
  echo ""
  echo "example for one subject:" 
  echo "bip_roi_analysis.sh sub-100 arc_l 1" 
  echo ""
  echo "example for all subjects in the BIDS dir:"
  echo "for subj in sub-*; do bip_roi_analysis.sh ${subj} arc_l 1; done"
  echo ""
  exit 1
fi

if [ ! -d ${out} ]; then
  mkdir -p ${out}
fi

# If the roi exists for the given subject
if [ -e ${roi} ]; then
  # if the masks.txt has not already been copied
  if [ ! -e derivatives/roi_analysis/${tract}/masks.txt ]; then
    # Copy masks.txt from the original tract directory to the output tract directory 
    # so there is a record of which roi number corresponded to which region.
    cp ${tract_dir}/masks.txt derivatives/roi_analysis/${tract}/
    echo ""
    echo "See derivatives/roi_analysis/${tract}/masks.txt" 
    roi1=`head -n 1 derivatives/roi_analysis/${tract}/masks.txt`
    roi2=`tail -n 1 derivatives/roi_analysis/${tract}/masks.txt`
    echo "roi_1 is ${roi1}" 
    echo "roi_2 is ${roi2}"
    echo ""
  fi
  echo "============================="
  echo "${subj}, ${tract}, roi_${roi_num}"
  # echo "============================="
  # truncate the native space mask so that it only includes grey matter
  fslmaths ${roi} -mul ${gm_mask} ${out}/${subj}_roi_${roi_num}_gm -odt char
  # warp the truncated mask into 2x2x2 MNI space
  applywarp -r ${FSLDIR}/data/standard/MNI152_T1_2mm -i ${out}/${subj}_roi_${roi_num}_gm -o ${out}/${subj}_roi_${roi_num}_gm_mni -w ${warp} 
  # # binarize the mni space mask and add the tract name to the output mask
  fslmaths ${out}/${subj}_roi_${roi_num}_gm_mni -thr 0.5 -bin ${out}/${subj}_roi_${roi_num}_${tract}_gm_mni_bin -odt char

  # # On the theory that we want to run fsladd in this directory, we will remove intermediate rois.
  rm ${out}/${subj}_roi_${roi_num}_gm.nii.gz
  rm ${out}/${subj}_roi_${roi_num}_gm_mni.nii.gz
fi